package com.highgo.cyyd.service;

import java.util.List;

import com.highgo.cyyd.entity.MyAttention;

public interface MyAttentionService {
	void save(MyAttention myAttention);
	List<MyAttention> getMyAttentionList();
}
