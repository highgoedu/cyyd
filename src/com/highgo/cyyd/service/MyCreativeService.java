package com.highgo.cyyd.service;

import java.util.List;

import com.highgo.cyyd.model.MyCreativeTO;

public interface MyCreativeService {
	
	List<MyCreativeTO> getMyCreativeAll(long userId);
	
	List<MyCreativeTO> getMyCreativeAll(long userId,int status);
	
	long save(MyCreativeTO to);

	List<MyCreativeTO> getMyCreativeAll(long userId,Integer page, Integer pageSize);

	void updateCreative(MyCreativeTO to);
	
	MyCreativeTO getMyCreativeById(long id);
}
