package com.highgo.cyyd.service;

import java.util.List;

import com.highgo.cyyd.model.CreativeCommentTO;

public interface CreativeCommentService {
	
	List<CreativeCommentTO> getMyCreativeListByCreativeId(long id);
	
	void save(CreativeCommentTO creativeComment);
}
