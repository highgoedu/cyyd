package com.highgo.cyyd.service.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.highgo.cyyd.Util.DateUtils;
import com.highgo.cyyd.dao.SdeAwsQuestionDao;
import com.highgo.cyyd.entity.SdeAwsPostsIndex;
import com.highgo.cyyd.entity.SdeAwsQuestion;
import com.highgo.cyyd.entity.SdeSolution;
import com.highgo.cyyd.model.AnswerUserTO;
import com.highgo.cyyd.model.SdeAwsPostsIndexTO;
import com.highgo.cyyd.model.SdeAwsQuestionTO;
import com.highgo.cyyd.model.SdeSolutionTO;
import com.highgo.cyyd.model.convertor.SdeAwsPostsIndexConvertor;
import com.highgo.cyyd.model.convertor.SdeAwsQuestionConvertor;
import com.highgo.cyyd.model.convertor.SdeSolutionConvertor;
import com.highgo.cyyd.service.SdeAwsQuestionService;

@Service
@Transactional
public class SdeAwsQuestionServiceImpl implements SdeAwsQuestionService {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private SdeAwsQuestionDao sdeAwsQuestionDao;

	@Override
	public long saveSdeAwsQuestion(SdeAwsQuestionTO to) {
		logger.debug("Enter,");
		
		SdeAwsQuestion entity = SdeAwsQuestionConvertor.convert(to);
		sdeAwsQuestionDao.save(entity);
		
		logger.debug("return id = " + entity.getQuestionId());
		
		return entity.getQuestionId();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public long updateSdeAwsQuestion(long id) {
		logger.debug("Enter,");
		
		String questionHql = "from SdeAwsQuestion where cyydId=" + id;
		List<SdeAwsQuestion> questionList = (List<SdeAwsQuestion>) sdeAwsQuestionDao.executeHql(questionHql);
		if(questionList == null || questionList.isEmpty()){
			//没有发布，直接归档
			return -1;
		}
		SdeAwsQuestion entity = questionList.get(0);
		entity.setLock(1);//0:未锁 ,1:锁
		sdeAwsQuestionDao.updateObject(entity);
		
		String hql = "from SdeAwsPostsIndex where postId=" + questionList.get(0).getQuestionId();
		List<SdeAwsPostsIndex> list = (List<SdeAwsPostsIndex>) sdeAwsQuestionDao.executeHql(hql);
		for (int i = 0; i < list.size(); i++) {
			list.get(i).setLock(1);
		}
		
		return entity.getQuestionId();
	}
	
	@Override
	public long saveSdeAwsPostsIndex(SdeAwsPostsIndexTO to) {
		logger.debug("Enter,");
		
		SdeAwsPostsIndex entity = SdeAwsPostsIndexConvertor.convert(to);
		sdeAwsQuestionDao.save(entity);
		
		logger.debug("return id = " + entity.getId());
		
		return entity.getId();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getQuestionScore(long questionId) {
		logger.debug("Enter,");
		
		List<String> list = new ArrayList<>();
		
		//creativity 创新性,practicability实用性,feasibility可行性,economy经济性
		try {
			String countHql = "select avg(creativity),avg(economy),avg(feasibility),avg(practicability) from SdeAwsAnswer where questionId=" + questionId;
			logger.debug("countHql  = " + countHql);
			List<Object> count = (List<Object>) sdeAwsQuestionDao.executeHql(countHql);
			logger.debug("List<Object> count  = " + count.size());
			
			Object[] object = (Object[]) count.get(0);
			
			DecimalFormat df = new DecimalFormat("#.00");   
			for (int i = 0; i < object.length; i++) {
				if(null == object[i]){
					list.add("0");
				}else{
					double a = Double.parseDouble(object[i].toString());
					list.add(df.format(a));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public SdeAwsQuestionTO getQuestionIdByCyydId(long id) {
		logger.debug("Enter,");
		
		String questionHql = "from SdeAwsQuestion where cyydId=" + id;
		List<SdeAwsQuestion> questionList = (List<SdeAwsQuestion>) sdeAwsQuestionDao.executeHql(questionHql);
		logger.debug("questionList size = " + questionList.size());
		
		if(questionList.isEmpty()){
			return null;
		}else{
			return  SdeAwsQuestionConvertor.convert(questionList.get(0));
		}
	}

	@Override
	public List<AnswerUserTO> getCommentsAndUser(long questionId) {
		logger.debug("Enter,");
		
		List<AnswerUserTO> result = new ArrayList<>();
		
		String sql = "select u.user_name,u.avatar_file,a.answer_content,a.add_time from sde_aws_users u "
				+ " join sde_aws_answer a on u.uid=a.uid where a.question_id="+questionId
				+ " ORDER BY a.add_time desc";
		logger.debug("sql  = " + sql);
		try {
			List<Object[]> list = sdeAwsQuestionDao.executeSql(sql);
			logger.debug("return sql list size = " + list);
			
			AnswerUserTO to;
			for(Object[] obj :list){
				to = new AnswerUserTO();
				if(obj[0] != null){
					to.setUserName(obj[0].toString());
				}else{
					to.setUserName("");
				}
				if(obj[1] != null){
					to.setAvatarFile(obj[1].toString());
				}else{
					to.setAvatarFile("");
				}
				if(obj[2] != null){
					to.setAnswerContent(obj[2].toString());
				}else{
					to.setAnswerContent("");
				}
				if(obj[3] != null){
					to.setAddTime(DateUtils.format(new Date(Long.parseLong(obj[3].toString())*1000), DateUtils.formatStr_yyyyMMddHHmmss));
				}else{
					to.setAddTime("");
				}
				result.add(to);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		logger.debug("return result size = " + result.size());
		return result;
	}

	@Override
	public void saveSdeSolution(SdeSolutionTO to) {
		logger.debug("Enter,");
		
		SdeSolution entity = SdeSolutionConvertor.convert(to);
		sdeAwsQuestionDao.save(entity);
		
	}
}
