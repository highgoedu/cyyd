package com.highgo.cyyd.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.highgo.cyyd.Util.DateUtils;
import com.highgo.cyyd.dao.CreativeCommentDao;
import com.highgo.cyyd.entity.CreativeComment;
import com.highgo.cyyd.model.CreativeCommentTO;
import com.highgo.cyyd.model.convertor.CreativeCommentConvertor;
import com.highgo.cyyd.service.CreativeCommentService;

@Service
@Transactional
public class CreativeCommentServiceImpl implements CreativeCommentService {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	public CreativeCommentDao creativeCommentDao;

	@SuppressWarnings("unchecked")
	@Override
	public List<CreativeCommentTO> getMyCreativeListByCreativeId(long id) {
		logger.debug("Enter,");
		
		List<CreativeCommentTO> result = new ArrayList<CreativeCommentTO>();
		
		String hql = "from CreativeComment where creativeId = " + id;
		logger.debug("hql = " + hql);
		
		try {
			List<CreativeComment> list = (List<CreativeComment>) creativeCommentDao.executeHql(hql);
			logger.debug("hql result size = " + list.size());
			
			CreativeCommentTO to = null;
			for (CreativeComment entity : list) {
				to = new CreativeCommentTO();
				BeanUtils.copyProperties(entity, to);
				result.add(to);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		logger.debug("return result size = " + result.size());
		return result;
	}

	@Override
	public void save(CreativeCommentTO to) {
		logger.debug("Enter,");
		
		CreativeComment entity = CreativeCommentConvertor.convert(to);
		entity.setCreateTime(DateUtils.format(new Date(), DateUtils.formatStr_yyyyMMddHHmmss));
		creativeCommentDao.save(entity);
	}
	
	public CreativeCommentDao getCreativeCommentDao() {
		return creativeCommentDao;
	}

	public void setCreativeCommentDao(CreativeCommentDao creativeCommentDao) {
		this.creativeCommentDao = creativeCommentDao;
	}
	
}
