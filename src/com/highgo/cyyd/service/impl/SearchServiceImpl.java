package com.highgo.cyyd.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.highgo.cyyd.service.SearchService;
import com.highgo.cyyd.vec.Word2VEC;
import com.highgo.cyyd.vec.term.D3Force;
import com.highgo.cyyd.vec.term.D3Pack;
import com.highgo.cyyd.vec.term.D3Tree;
import com.highgo.cyyd.vec.term.Node;
import com.highgo.cyyd.vec.term.Term;
import com.highgo.cyyd.vec.term.TokenizeWord;

@Service
public class SearchServiceImpl implements SearchService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public String createJSONFile(String path,String keyword,String layerNum,String layerCount,String showType) throws IOException {
		logger.debug("Enter,");
		logger.debug("keyword:" + keyword + ",layerNum:" + layerNum + ",layerCount:" + layerCount + ",showType:" + showType);
		String keyWordBianma = "";
		byte[] jiema = keyword.getBytes("UTF-8");
		for (int i = 0; i < jiema.length; i++) {
			keyWordBianma += jiema[i];
		}
		Word2VEC w = new Word2VEC();
		String jsonName = "";
		if(showType.equals("tree")) {
			jsonName = "tree" + "_" + keyWordBianma + "_" + layerNum + "_" + layerCount + ".json";
		} else if(showType.equals("force")) {
			jsonName = "force" + "_" + keyWordBianma + "_" + layerNum + "_" + layerCount + ".json";
		} else if(showType.equals("pack")) {
			jsonName = "pack" + "_" + keyWordBianma + "_" + layerNum + "_" + layerCount + ".json";
		}
		String jsonStorePath = path + jsonName;
		File file = new File(jsonStorePath);
		if (file.exists()) {
			logger.debug("存在：" + file.getAbsolutePath());
		} else {
			TokenizeWord tw = new TokenizeWord();
			tw.add(keyword);
			Node node2 = new Node(new Term(keyword, 0.1));
			Node node = tw.tokenizedToTree(w, node2,
					Integer.parseInt(layerNum) - 1,
					Integer.parseInt(layerCount));
			if(showType.equals("tree")) {
				new D3Tree(node, jsonStorePath);
			} else if(showType.equals("force")) {
				new D3Force(node, jsonStorePath);
			} else if(showType.equals("pack")) {
				new D3Pack(node, jsonStorePath);
			}
		}
		logger.debug(jsonStorePath);
		
		String jsonReadPath = "JSON/" + jsonName;
		
		return jsonReadPath;
		
	}
	
	public List<String> loadInfoByKeyword(String keyword) {
		return null;
	}
}
