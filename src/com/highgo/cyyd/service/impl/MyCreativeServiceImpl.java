package com.highgo.cyyd.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.highgo.cyyd.Util.DateUtils;
import com.highgo.cyyd.dao.MyCreativeDao;
import com.highgo.cyyd.entity.MyCreative;
import com.highgo.cyyd.model.MyCreativeTO;
import com.highgo.cyyd.model.convertor.MyCreativeConvertor;
import com.highgo.cyyd.service.MyCreativeService;

@Service
@Transactional
public class MyCreativeServiceImpl implements MyCreativeService {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	public MyCreativeDao myCreativeDao;

	@SuppressWarnings("unchecked")
	@Override
	public List<MyCreativeTO> getMyCreativeAll(long userId) {
		List<MyCreativeTO> result = new ArrayList<MyCreativeTO>();
		
		String hql = "";
		if(userId == 0){
			//超级管理员
			hql = "from MyCreative where isVisible=0 ";
		}else{
			hql = "from MyCreative where isVisible=0 and userId=" + userId;
		}

		List<MyCreative> list = (List<MyCreative>) myCreativeDao.executeHql(hql);
		
		MyCreativeTO to = null;
		for (MyCreative entity : list) {
			to = MyCreativeConvertor.convert(entity);
			result.add(to);
		}
		logger.debug("return result size = " + result.size());
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MyCreativeTO> getMyCreativeAll(long userId,int status) {
		List<MyCreativeTO> result = new ArrayList<MyCreativeTO>();
		
		String hql = "";
		if(userId == 0){
			hql = "from MyCreative where isVisible=0 and status = " + status;
		}else{
			hql = "from MyCreative where isVisible=0 and userId="+userId + " and status = " + status;
		}
		
		List<MyCreative> list = (List<MyCreative>) myCreativeDao.executeHql(hql);
		
		MyCreativeTO to = null;
		for (MyCreative entity : list) {
			to = MyCreativeConvertor.convert(entity);
			result.add(to);
		}
		logger.debug("return result size = " + result.size());
		return result;
	}
	
	@Override
	public List<MyCreativeTO> getMyCreativeAll(long userId,Integer page, Integer pageSize) {
		logger.debug("Enter, userId = "+ userId + ",page = "+ page +" , pageSize =" + pageSize);
		
		List<MyCreativeTO> result = new ArrayList<MyCreativeTO>();
		MyCreativeTO to ;
//		select c.id,c.startCreativeTitle,c.startCreativeContent,c.`status`,c.createTime,u.user_name from t_mycreative c 
//		JOIN sde_aws_users u ON c.uid = u.uid 
//		where c.userId=13
//		ORDER BY c.createTime desc
		StringBuilder sql = new StringBuilder();
		sql.append("select c.id,c.startCreativeTitle,c.startCreativeContent,c.`status`,c.createTime,u.user_name from t_mycreative c ");
		sql.append("JOIN sde_aws_users u ON c.uid = u.uid where isVisible=0 ");
		if(userId != 0){
			sql.append(" and c.userId=" + userId);
		}
		sql.append(" ORDER BY c.createTime desc");
		logger.debug("sql = " + sql);
		
		List<Object[]> list = myCreativeDao.SqlPageQuery(sql.toString(), pageSize, page);
		for (int i = 0; i < list.size(); i++) {
			//[45, 发发发, <p>发发发<br/></p>, 0, 2016-11-11 10:24:50, sde]
			Object[] obj = list.get(i);
			to = new MyCreativeTO();
			to.setId(Long.parseLong(String.valueOf(obj[0])));
			if(obj[1] != null){
				to.setStartCreativeTitle(obj[1].toString());
			}
			if(obj[2] != null){
				to.setStartCreativeContent(obj[2].toString());
			}
			to.setStatus(Integer.parseInt(obj[3].toString()));
			to.setCreateTime(obj[4].toString());
			if(obj[5] != null){
				to.setUserName(obj[5].toString());
			}
			result.add(to);
		}
		logger.debug("return result size = " + result.size());
		return result;
	}

	@Override
	public long save(MyCreativeTO to) {
		logger.debug("Enter,");
		
		MyCreative entity = MyCreativeConvertor.convert(to);
		entity.setCreateTime(DateUtils.format(new Date(), DateUtils.formatStr_yyyyMMddHHmmss));
		myCreativeDao.save(entity);
		to.setId(entity.getId());
		logger.debug("save succ, id = " +entity.getId());
		
		return entity.getId();
	}
	
	@Override
	public void updateCreative(MyCreativeTO to) {
		logger.debug("Enter,");
		
		MyCreative myCreative = myCreativeDao.getObjectById(MyCreative.class, to.getId());
		myCreative.setStartCreativeTitle(to.getStartCreativeTitle());
		myCreative.setStartCreativeContent(to.getStartCreativeContent());
		myCreative.setEndCreativeTitle(to.getEndCreativeTitle());
		myCreative.setEndCreativeContent(to.getEndCreativeContent());
		myCreative.setKeyword(to.getKeyword());
		myCreative.setStatus(to.getStatus());
		myCreative.setSearchKey(to.getSearchKey());
		myCreative.setSourcetype(to.getSourcetype());
		myCreative.setNotepad(to.getNotepad());
	}
	
	public MyCreativeDao getMyCreativeDao() {
		return myCreativeDao;
	}

	public void setMyCreativeDao(MyCreativeDao myCreativeDao) {
		this.myCreativeDao = myCreativeDao;
	}

	@Override
	public MyCreativeTO getMyCreativeById(long id) {
		MyCreative myCreative = myCreativeDao.getObjectById(MyCreative.class, id);
		MyCreativeTO to = MyCreativeConvertor.convert(myCreative);
		return to;
	}

}
