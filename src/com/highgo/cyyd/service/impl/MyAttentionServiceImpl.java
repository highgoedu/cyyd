package com.highgo.cyyd.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.highgo.cyyd.dao.MyAttentionDao;
import com.highgo.cyyd.entity.MyAttention;
import com.highgo.cyyd.service.MyAttentionService;

@Service
@Transactional
public class MyAttentionServiceImpl implements MyAttentionService {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private MyAttentionDao myAttentionDao;
	
	public void save(MyAttention myAttention) {
		this.myAttentionDao.save(myAttention);
	}

	public MyAttentionDao getMyAttentionDao() {
		return myAttentionDao;
	}
	
	public List<MyAttention> getMyAttentionList() {
		logger.debug("Enter,");
		List<?> list = this.myAttentionDao.getAllObject(MyAttention.class);
		List<MyAttention> attentionList = new ArrayList<>();
		if(list != null) {
			for (int i = 0; i < list.size(); i++) {
				attentionList.add((MyAttention)list.get(i));
			}
		}
		logger.debug("My attention list size is " + attentionList.size());
		return attentionList;
	}

	public void setMyAttentionDao(MyAttentionDao myAttentionDao) {
		this.myAttentionDao = myAttentionDao;
	}
	
	
	
}
