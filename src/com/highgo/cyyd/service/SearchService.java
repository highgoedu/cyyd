/**
 * 
 */
package com.highgo.cyyd.service;

import java.io.IOException;
import java.util.List;

/**
 * @author zhangyc
 *
 */

public interface SearchService {
	String createJSONFile(String path,String keyword,String layerNum,String layerCount,String showType) throws IOException;
	List<String> loadInfoByKeyword(String keyword);
}
