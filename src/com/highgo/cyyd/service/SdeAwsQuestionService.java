/**
 * 
 */
package com.highgo.cyyd.service;

import java.util.List;

import com.highgo.cyyd.model.AnswerUserTO;
import com.highgo.cyyd.model.SdeAwsPostsIndexTO;
import com.highgo.cyyd.model.SdeAwsQuestionTO;
import com.highgo.cyyd.model.SdeSolutionTO;

public interface SdeAwsQuestionService {
	
	long saveSdeAwsQuestion(SdeAwsQuestionTO to);
	
	long saveSdeAwsPostsIndex(SdeAwsPostsIndexTO to);
	
	void saveSdeSolution(SdeSolutionTO to);
	
	long updateSdeAwsQuestion(long id);
	
	List<String> getQuestionScore(long questionId);
	
	SdeAwsQuestionTO getQuestionIdByCyydId(long id);
	
	//根据sql获取评论和用户表的集合
	List<AnswerUserTO> getCommentsAndUser(long questionId);
}
