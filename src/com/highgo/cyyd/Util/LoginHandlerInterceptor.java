package com.highgo.cyyd.Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import net.sf.json.JSONObject;

/**
 * 请求拦截器
 */
public class LoginHandlerInterceptor extends HandlerInterceptorAdapter {
	
	private Logger logger = LoggerFactory.getLogger(LoginHandlerInterceptor.class);

	//验证登陆接口
	private String remoteURL = "http://www.bigdatainnovation.cn/User/login/checkLogin";
	
	//去登陆
	public static String path = "/login.jsp";
	
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		logger.debug("Enter,");
		
		Object userId = request.getSession().getAttribute("userId");
		logger.debug("userId = " + userId);
		
		Object uid = request.getSession().getAttribute("uid");
		logger.debug("uid = " + uid);
		
		try {
			if(userId == null){
				String result = HttpRequest.sendGet(remoteURL);
				logger.debug("HttpRequest sendGet = " + result);
				
				result = result.substring(1, result.length()-1);
				logger.debug("HttpRequest sendGet = " + result);
				
				JSONObject json = JSONObject.fromObject(result);
				Object code =  json.get("code");
				if("0".equals(code.toString())){
					response.sendRedirect(request.getContextPath() + path);
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect(request.getContextPath() + path);
			return false;
		}
		return super.preHandle(request, response, handler);
	}

	
}
