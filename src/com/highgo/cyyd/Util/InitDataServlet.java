package com.highgo.cyyd.Util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.highgo.cyyd.vec.Word2VEC;

public class InitDataServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	@Override
	public void init() throws ServletException {
		
		try {
			InputStream in = getClass().getClassLoader().getResourceAsStream("/settings.properties");
			Properties properties = new Properties();
			properties.load(in);
			Constants.BIN_PATH = properties.getProperty("binpath");
			Constants.INDEX_PATH = properties.getProperty("indexpath");
			Word2VEC w = new Word2VEC();
			w.loadGoogleModel(Constants.BIN_PATH);
			w.setTopNSize(20);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
