package com.highgo.cyyd.Util;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class HttpRequest {

    /**
     * Main
     * @param args
     * @throws Exception 
     */
    public static void main(String[] args) throws Exception {
//    	String url1 = "http://sde.yangzhe.net/User/login/checkLogin";
    	String url1 = "http://www.sml163.com/cors/api/testGet?text=1";
        System.out.println(sendGet(url1));
        
//        String url2 = "http://localhost:8080/test/api/echo";
//        System.out.println(sendPost(url2, "msg=方法"));
//        
//        ResultTO to = new ResultTO();
//        to.setMessageType(1);
//        to.setMessage("fff");
//        JSONObject json = JSONObject.fromObject(to);
//        String url3 = "http://localhost:8080/test/api/testPost";
//        System.out.println(sendPost(url3, json.toString()));
    }
    
    /**
     * Get Request
     * @return
     * @throws Exception
     */
    public static String sendGet(String url) throws Exception {
        String result = "";
        BufferedReader in = null;
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            HttpURLConnection connection = (HttpURLConnection) realUrl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            connection.setRequestMethod("GET");
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result;
    }
    /**
     * 向指定 URL 发送POST方法的请求
     * 
     * @param url
     *            发送请求的 URL
     * @param param
     *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     */
    public static String sendPost(String url, String param) {
    	OutputStream out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            connection.setRequestProperty("Content-Type", "application/json");
            
            // 发送POST请求必须设置如下两行
            connection.setDoOutput(true);  
            connection.setUseCaches(false);  
            connection.setDoInput(true);  
            connection.setConnectTimeout(20000);  
            connection.setReadTimeout(300000);  
            connection.setRequestProperty("Charset", "UTF-8");  
            // 获取URLConnection对象对应的输出流
            out = connection.getOutputStream();  
            out.write(param.toString().getBytes());  
            out.close();  
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！"+e);
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally{
            try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }
        return result;
    }    
//    
//    /*
//     * params 填写的URL的参数 encode 字节编码
//     */
//    public static String sendPostMessage(Map<String, String> params,
//        String encode) {
//
//      StringBuffer stringBuffer = new StringBuffer();
//
//      if (params != null && !params.isEmpty()) {
//        for (Map.Entry<String, String> entry : params.entrySet()) {
//          try {
//            stringBuffer
//                .append(entry.getKey())
//                .append("=")
//                .append(URLEncoder.encode(entry.getValue(), encode))
//                .append("&");
//
//          } catch (UnsupportedEncodingException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//          }
//        }
//        // 删掉最后一个 & 字符
//        stringBuffer.deleteCharAt(stringBuffer.length() - 1);
//        System.out.println("-->>" + stringBuffer.toString());
//
//        try {
//          HttpURLConnection httpURLConnection = (HttpURLConnection) url
//              .openConnection();
//          httpURLConnection.setConnectTimeout(3000);
//          httpURLConnection.setDoInput(true);// 从服务器获取数据
//          httpURLConnection.setDoOutput(true);// 向服务器写入数据
//
//          // 获得上传信息的字节大小及长度
//          byte[] mydata = stringBuffer.toString().getBytes();
//          // 设置请求体的类型
//          httpURLConnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
//          httpURLConnection.setRequestProperty("Content-Lenth",String.valueOf(mydata.length));
//
//          // 获得输出流，向服务器输出数据
//          OutputStream outputStream = (OutputStream) httpURLConnection
//              .getOutputStream();
//          outputStream.write(mydata);
//
//          // 获得服务器响应的结果和状态码
//          int responseCode = httpURLConnection.getResponseCode();
//          if (responseCode == 200) {
//
//            // 获得输入流，从服务器端获得数据
//            InputStream inputStream = (InputStream) httpURLConnection
//                .getInputStream();
//            return (changeInputStream(inputStream, encode));
//
//          }
//
//        } catch (IOException e) {
//          // TODO Auto-generated catch block
//          e.printStackTrace();
//        }
//      }
//
//      return "";
//    }
}

