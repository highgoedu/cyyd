﻿package com.highgo.cyyd.Util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * <p>
 * 类名
 * </p>
 * 日期工具类，快速获得日期，对日期格式进行转换
 * @author Administrator
 * @version 1.0
 */
public class DateUtils {
	public final static String YYYY = "yyyy";
	public final static String MM = "MM";
	public final static String DD = "dd";
	public final static String YYYY_MM_DD = "yyyy-MM-dd";
	public final static String YYYY_MM = "yyyy-MM";
	public final static String HH_MM_SS = "HH:mm:ss";
	public final static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
	public final static String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
	public static String formatStr_yyyyMMddHHmmssS = "yyyy-MM-dd HH:mm:ss.S";
	public static String formatStr_yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss";
	public static String formatStr_yyyyMMddHHmm = "yyyy-MM-dd HH:mm";
	public static String formatStr_yyyyMMddHH = "yyyy-MM-dd HH";
	public static String formatStr_yyyyMMdd = "yyyy-MM-dd";
	public static String[] formatStr = { formatStr_yyyyMMddHHmmss, formatStr_yyyyMMddHHmm, formatStr_yyyyMMddHH,
			formatStr_yyyyMMdd };

	/**
	 * 构造函数
	 */
	public DateUtils() {
	}
	/**
	 * 日期格式化－将<code>Date</code>类型的日期格式化为<code>String</code>型
	 * @param date 待格式化的日期
	 * @param pattern 时间样式
	 * @return 一个被格式化了的<code>String</code>日期
	 */
	public static String format(Date date, String pattern) {
		if (date == null) {
			return "";
		} else {
			return getFormatter(pattern).format(date);
		}
	}
	/**
	 * 默认把日期格式化成yyyy-mm-dd格式
	 * @param date
	 * @return
	 */
	public static String format(Date date) {
		if (date == null) {
			return "";
		} else {
			return getFormatter(YYYY_MM_DD).format(date);
		}
	}
	/**
	 * 把字符串日期默认转换为yyyy-mm-dd格式的Data对象
	 * @param strDate
	 * @return
	 */
	public static Date format(String strDate) {
		Date d = null;
		if (StringUtils.isEmpty(strDate)) {
			return null;
		} else {
			try {
				d = getFormatter(YYYY_MM_DD).parse(strDate);
			} catch (ParseException pex) {
				return null;
			}
		}
		return d;
	}
	/**
	 * 把字符串日期转换为f指定格式的Data对象
	 * @param strDate ,f
	 * @return
	 */
	public static Date format(String strDate, String f) {
		Date d = null;
		if (StringUtils.isEmpty(strDate)) {
			return null;
		} else {
			try {
				d = getFormatter(f).parse(strDate);
			} catch (ParseException pex) {
				return null;
			}
		}
		return d;
	}
	/**
	 * 日期解析－将<code>String</code>类型的日期解析为<code>Date</code>型
	 * @param date 待格式化的日期
	 * @param pattern 日期样式
	 * @exception ParseException 如果所给的字符串不能被解析成一个日期
	 * @return 一个被格式化了的<code>Date</code>日期
	 */
	public static Date parse(String strDate, String pattern) throws ParseException {
		try {
			return getFormatter(pattern).parse(strDate);
		} catch (ParseException pe) {
			throw new ParseException("Method parse in Class DateUtils  err: parse strDate fail.", pe.getErrorOffset());
		}
	}
	/**
	 * 获取当前日期
	 * @return 一个包含年月日的<code>Date</code>型日期
	 */
	public static synchronized Date getCurrDate() {
		Calendar calendar = Calendar.getInstance();
		return calendar.getTime();
	}
	/**
	 * 获取当前日期
	 * @return 一个包含年月日的<code>String</code>型日期， 指定格式pattern
	 */
	public static String getCurrDateStr(String pattern) {
		return format(getCurrDate(), pattern);
	}
	/**
	 * 获取当前日期
	 * @return 一个包含年月日的<code>String</code>型日期，但不包含时分秒。yyyy-mm-dd
	 */
	public static String getCurrDateStr() {
		return format(getCurrDate(), YYYY_MM_DD);
	}
	/**
	 * 获取当前日期
	 * @return 一个包含年月日的<code>String</code>型日期，但不包含时分秒。yyyyMMdd
	 */
	public static String getCurrDateStrYYYYMMDD() {
		return format(getCurrDate(), "yyyyMMdd");
	}
	/**
	 * 获得当前日期
	 * @return 一个包含年月日时分秒的<code>String</code>型日期。yyyymmddhhmmss
	 */
	public static String getCurrDateStr_() {
		StringBuffer now_ = new StringBuffer();
		now_.append(getYear());
		now_.append(getMonth());
		now_.append(getDay());
		now_.append(format(getCurrDate(), "HH"));
		now_.append(format(getCurrDate(), "MM"));
		now_.append(format(getCurrDate(), "SS"));
		return now_.toString();
	}
	/**
	 * 获得当前日期
	 * @return 一个包含年月日的<code>String</code>型日期。yyyymmdd
	 */
	public static String getCurrDateS() {
		StringBuffer now_ = new StringBuffer();
		now_.append(getYear());
		now_.append(getMonth());
		now_.append(getDay());
		return now_.toString();
	}
	/**
	 * 获取当前日期的后三个月日期
	 * @return 三个月后的时间
	 */
	public static String getLastMonth(int num) {
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, num);
		Date otherDate = cal.getTime();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		// System.out.println( "today:   "+dateFormat.format(date)+
		// "   3   months   after:   "+dateFormat.format(otherDate));
		return dateFormat.format(otherDate);
	}
	/**
	 * 获取当前时间
	 * @return 一个包含年月日时分秒的<code>String</code>型日期。hh:mm:ss
	 */
	public static String getCurrTimeStr() {
		return format(getCurrDate(), HH_MM_SS);
	}
	/**
	 * 获取当前完整时间,样式: yyyy－MM－dd hh:mm:ss
	 * @return 一个包含年月日时分秒的<code>String</code>型日期。yyyy-MM-dd hh:mm:ss
	 */
	public static String getCurrDateTimeStr() {
		return format(getCurrDate(), YYYY_MM_DD_HH_MM_SS);
	}
	/**
	 * 获取当前年分 样式：yyyy
	 * @return 当前年分
	 */
	public static String getYear() {
		return format(getCurrDate(), YYYY);
	}
	/**
	 * 获取当前月分 样式：MM
	 * @return 当前月分
	 */
	public static String getMonth() {
		return format(getCurrDate(), MM);
	}
	/**
	 * 获取当前日期号 样式：dd
	 * @return 当前日期号
	 */
	public static String getDay() {
		return format(getCurrDate(), DD);
	}
	/**
	 * 按给定日期样式判断给定字符串是否为合法日期数据
	 * @param strDate 要判断的日期
	 * @param pattern 日期样式
	 * @return true 如果是，否则返回false
	 */
	public static boolean isDate(String strDate, String pattern) {
		try {
			parse(strDate, pattern);
			return true;
		} catch (ParseException pe) {
			return false;
		}
	}
	/**
	 * 判断给定字符串是否为特定格式日期（包括：年月日yyyy-MM-dd）数据
	 * @param strDate 要判断的日期
	 * @return true 如果是，否则返回false
	 */
	// public static boolean isDate(String strDate) {
	// try {
	// parse(strDate, YYYY_MM_DD);
	// return true;
	// }
	// catch (ParseException pe) {
	// return false;
	// }
	// }
	/**
	 * 判断给定字符串是否为特定格式年份（格式：yyyy）数据
	 * @param strDate 要判断的日期
	 * @return true 如果是，否则返回false
	 */
	public static boolean isYYYY(String strDate) {
		try {
			parse(strDate, YYYY);
			return true;
		} catch (ParseException pe) {
			return false;
		}
	}
	public static boolean isYYYY_MM(String strDate) {
		try {
			parse(strDate, YYYY_MM);
			return true;
		} catch (ParseException pe) {
			return false;
		}
	}
	/**
	 * 判断给定字符串是否为特定格式的年月日（格式：yyyy-MM-dd）数据
	 * @param strDate 要判断的日期
	 * @return true 如果是，否则返回false
	 */
	public static boolean isYYYY_MM_DD(String strDate) {
		try {
			parse(strDate, YYYY_MM_DD);
			return true;
		} catch (ParseException pe) {
			return false;
		}
	}
	/**
	 * 判断给定字符串是否为特定格式年月日时分秒（格式：yyyy-MM-dd HH:mm:ss）数据
	 * @param strDate 要判断的日期
	 * @return true 如果是，否则返回false
	 */
	public static boolean isYYYY_MM_DD_HH_MM_SS(String strDate) {
		try {
			parse(strDate, YYYY_MM_DD_HH_MM_SS);
			return true;
		} catch (ParseException pe) {
			return false;
		}
	}
	/**
	 * 判断给定字符串是否为特定格式时分秒（格式：HH:mm:ss）数据
	 * @param strDate 要判断的日期
	 * @return true 如果是，否则返回false
	 */
	public static boolean isHH_MM_SS(String strDate) {
		try {
			parse(strDate, HH_MM_SS);
			return true;
		} catch (ParseException pe) {
			return false;
		}
	}
	/**
	 * 判断给定字符串是否为特定格式时间（包括：时分秒hh:mm:ss）数据
	 * @param strTime 要判断的时间
	 * @return true 如果是，否则返回false
	 */
	// public static boolean isTime(String strTime) {
	// try {
	// parse(strTime, HH_MM_SS);
	// return true;
	// }
	// catch (ParseException pe) {
	// return false;
	// }
	// }
	/**
	 * 判断给定字符串是否为特定格式日期时间（包括：年月日时分秒 yyyy-MM-dd hh:mm:ss）数据
	 * @param strDateTime 要判断的日期时间
	 * @return true 如果是，否则返回false
	 */
	// public static boolean isDateTime(String strDateTime) {
	// try {
	// parse(strDateTime, YYYY_MM_DD_HH_MM_SS);
	// return true;
	// }
	// catch (ParseException pe) {
	// return false;
	// }
	// }
	/**
	 * 获取一个简单的日期格式化对象
	 * @return 一个简单的日期格式化对象
	 */
	private static SimpleDateFormat getFormatter(String parttern) {
		return new SimpleDateFormat(parttern);
	}
	/**
	 * 获取给定日前的后intevalDay天的日期
	 * @param refenceDate 给定日期（格式为：yyyy-MM-dd）
	 * @param intevalDays 间隔天数
	 * @return 计算后的日期
	 */
	public static String getNextDate(String refenceDate, int intevalDays) {
		try {
			return getNextDate(parse(refenceDate, YYYY_MM_DD), intevalDays);
		} catch (Exception ee) {
			return "";
		}
	}
	/**
	 * 获取给定日前的后intevalDay天的日期
	 * @param refenceDate Date 给定日期
	 * @param intevalDays int 间隔天数
	 * @return String 计算后的日期
	 */
	public static String getNextDate(Date refenceDate, int intevalDays) {
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(refenceDate);
			calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + intevalDays);
			return format(calendar.getTime(), YYYY_MM_DD);
		} catch (Exception ee) {
			return "";
		}
	}
	public static long getIntevalDays(String startDate, String endDate) {
		try {
			return getIntevalDays(parse(startDate, YYYY_MM_DD), parse(endDate, YYYY_MM_DD));
		} catch (Exception ee) {
			return 0l;
		}
	}
	public static long getIntevalDays(Date startDate, Date endDate) {
		try {
			java.util.Calendar startCalendar = java.util.Calendar.getInstance();
			java.util.Calendar endCalendar = java.util.Calendar.getInstance();
			startCalendar.setTime(startDate);
			endCalendar.setTime(endDate);
			long diff = endCalendar.getTimeInMillis() - startCalendar.getTimeInMillis();
			return diff;
		} catch (Exception ee) {
			return 0l;
		}
	}
	/**
	 * 求当前日期和指定字符串日期的相差天数
	 * @param startDate
	 * @return
	 */
	public static long getTodayIntevalDays(String startDate) {
		try {
			// 当前时间
			Date currentDate = new Date();
			// 指定日期
			SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date theDate = myFormatter.parse(startDate);
			// 两个时间之间的天数
			long days = (currentDate.getTime() - theDate.getTime()) / (24 * 60 * 60 * 1000);
			return days;
		} catch (Exception ee) {
			return 0l;
		}
	}
	/**
	 * 获取订单计时秒数
	 * @param startDate
	 * @return
	 */
	public static long getTodayIntevalSecond(String startDate) {
		try {
			// 当前时间
			Date currentDate = new Date();
			// 指定日期
			SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			java.util.Date theDate = myFormatter.parse(startDate);
			long endtime = theDate.getTime() + (30 * 60 * 1000);
			// 两个时间之间的天数
			long days = ((endtime - currentDate.getTime()) / 1000);
			return days;
		} catch (Exception ee) {
			return 0l;
		}
	}
	public static Date parseToDate(String dateTimeStr) {
		if (dateTimeStr == null) {
			return null;
		}
		Date d = null;
		int formatStrLength = formatStr.length;
		for (int i = 0; i < formatStrLength; i++) {
			d = parseToDate2(dateTimeStr, formatStr[i]);
			if (d != null) {
				break;
			}
		}
		return d;
	}
	private static Date parseToDate2(String dateTimeStr, String formatString) {
		Date d = null;
		SimpleDateFormat sdf = new SimpleDateFormat(formatString);
		try {
			d = sdf.parse(dateTimeStr);
		} catch (ParseException pe) {
		}
		return d;
	}
	public static String dateTimeToString(Date datetime) {
		java.util.GregorianCalendar calendar = new java.util.GregorianCalendar();
		calendar.setTime(datetime);
		String dateTime = calendar.get(Calendar.YEAR) + "" + ((calendar.get(Calendar.MONTH) + 1) > 9 ? "" : "0")
				+ (calendar.get(Calendar.MONTH) + 1) + "" + (calendar.get(Calendar.DATE) > 9 ? "" : "0")
				+ calendar.get(Calendar.DATE) + "" + (calendar.get(Calendar.HOUR_OF_DAY) > 9 ? "" : "0")
				+ calendar.get(Calendar.HOUR_OF_DAY) + "" + (calendar.get(Calendar.MINUTE) > 9 ? "" : "0")
				+ calendar.get(Calendar.MINUTE) + "" + (calendar.get(Calendar.SECOND) > 9 ? "" : "0")
				+ calendar.get(Calendar.SECOND);
		return dateTime;
	}
	/**
	 * <p>
	 * 获取指定时长定时器的计时数（离当前时间还有多长时间）
	 * </p>
	 * 方法描述
	 * @param startTime 开始时间点 2015-07-07 14:14:00
	 * @param time 计时分钟数
	 * @return
	 */
	public static long getTimer(String startTime, int time) {
		// 当前时间
		Date currentDate = new Date();
		// 指定日期
		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		long days = 0;
		try {
			java.util.Date theDate = myFormatter.parse(startTime);
			long endtime = theDate.getTime() + (5 * 60 * 1000);
			// 两个时间之间的秒数
			days = ((endtime - currentDate.getTime()) / 1000);
			return days;
		} catch (ParseException e) {
			return -1;
		}
	}
	/**
	 * debug
	 */
	// public static void main(String[] args) {
	// try {
	// System.out.println(DateUtils.format(DateUtils.format(DateUtils
	// .getNextDate(new Date(), 4), "yyyy-MM-dd"), "MM月dd日"));
	//
	// Calendar calendar = Calendar.getInstance();
	// // 当前时间
	// Date currentDate = new Date();
	// // 指定日期
	// SimpleDateFormat myFormatter = new SimpleDateFormat(
	// "yyyy-MM-dd HH:mm:ss");
	// java.util.Date theDate = myFormatter.parse("2015-07-06 14:14:00");
	// long endtime = theDate.getTime() + (5 * 60 * 1000);
	// // 两个时间之间的天数
	// long days = ((endtime - currentDate.getTime()) / 1000);
	// System.out.println(days);
	// // getTodayIntevalSecond("2015")
	//
	// // System.out.println(getCurrentMonthEndTime());
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	/**
	 * 根据当前日期获取星期几
	 * @param args
	 */
	public static String getDay(String time) {
		String y = time.substring(0, 4);
		String m = time.substring(5, 7);
		String d = time.substring(8, 10);
		int year = Integer.parseInt(y);
		int month = 0;
		if (m.substring(0, 1).equals("0")) {
			month = Integer.parseInt(m.substring(1, 2));
		} else {
			month = Integer.parseInt(m);
		}
		int day = Integer.parseInt(d);
		Calendar calendar = Calendar.getInstance();// 获得一个日历
		calendar.set(year, month - 1, day);// 设置当前时间,月份是从0月开始计算
		int number = calendar.get(Calendar.DAY_OF_WEEK);// 星期表示1-7，是从星期日开始，
		String[] str = { "", "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六", };
		return str[number];
	}
	/**
	 * 根据当前日期获取星期几
	 * @param args
	 */
	public static int weekOfDay(String time) {
		String y = time.substring(0, 4);
		String m = time.substring(5, 7);
		String d = time.substring(8, 10);
		int year = Integer.parseInt(y);
		int month = 0;
		if (m.substring(0, 1).equals("0")) {
			month = Integer.parseInt(m.substring(1, 2));
		} else {
			month = Integer.parseInt(m);
		}
		int day = Integer.parseInt(d);
		Calendar calendar = Calendar.getInstance();// 获得一个日历
		calendar.set(year, month - 1, day);// 设置当前时间,月份是从0月开始计算
		int number = calendar.get(Calendar.DAY_OF_WEEK);// 星期表示1-7，是从星期日开始，
		return number;
	}
	/**
	 * 判断当前是否在时间1、时间2之间
	 * @param seeddate
	 * @return isDateBefore("2015-1-26 11:48:26","2015-1-31 11:48:32");
	 */
	public static boolean isDateIn(String date1, String date2) {
		try {
			Date date = new Date();
			DateFormat df = DateFormat.getDateTimeInstance();
			return !date.before(df.parse(date1)) && date.before(df.parse(date2));
		} catch (ParseException e) {
			System.out.print("当前时间：是否在时间1与2之间。" + e.getMessage());
			return false;
		}
	}
	/**
	 * 判断一个日期是否在两个日期点之内（字符串格式比较）
	 * @param date1 起始日期
	 * @param date2 结束日期
	 * @return
	 */
	public static boolean isDateStrIn(String date1, String date2) {
		boolean flag = false;
		try {
			String currentDateStr = getCurrDateStr();
			if ((currentDateStr.compareTo(date1) >= 0) && (currentDateStr.compareTo(date2) <= 0)) {
				flag = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("判断字符串当前日期是否在两个时间点范围内" + e.getMessage());
		}
		return flag;
	}
	/**
	 * 获取 当前年、半年、季度、月、日、小时 开始结束时间
	 */
	// private final static SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
	// private final static SimpleDateFormat longHourSdf = new SimpleDateFormat("yyyy-MM-dd HH");;
	// private final static SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");;
	/**
	 * 获得本周的第一天，周一
	 * @return
	 */
	public static String getCurrentWeekDayStartTime() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			int weekday = c.get(Calendar.DAY_OF_WEEK) - 2;
			c.add(Calendar.DATE, -weekday);
			c.setTime(longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return format(c.getTime(), YYYY_MM_DD_HH_MM_SS);
	}
	/**
	 * 获得本周的最后一天，周日
	 * @return
	 */
	public static String getCurrentWeekDayEndTime() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			int weekday = c.get(Calendar.DAY_OF_WEEK);
			c.add(Calendar.DATE, 8 - weekday);
			c.setTime(longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return format(c.getTime(), YYYY_MM_DD_HH_MM_SS);
	}
	/**
	 * 获得本天的开始时间，即2012-01-01 00:00:00
	 * @return
	 */
	public static String getCurrentDayStartTime() {
		Date now = new Date();
		SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			now = shortSdf.parse(shortSdf.format(now));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return format(now, YYYY_MM_DD_HH_MM_SS);
	}
	/**
	 * 获得本天的结束时间，即2012-01-01 23:59:59
	 * @return
	 */
	public static String getCurrentDayEndTime() {
		Date now = new Date();
		SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			now = longSdf.parse(shortSdf.format(now) + " 23:59:59");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return format(now, YYYY_MM_DD_HH_MM_SS);
	}
	/**
	 * 获得本小时的开始时间，即2012-01-01 23:59:59
	 * @return
	 */
	public static Date getCurrentHourStartTime() {
		Date now = new Date();
		SimpleDateFormat longHourSdf = new SimpleDateFormat("yyyy-MM-dd HH");
		try {
			now = longHourSdf.parse(longHourSdf.format(now));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return now;
	}
	/**
	 * 获得本小时的结束时间，即2012-01-01 23:59:59
	 * @return
	 */
	public static Date getCurrentHourEndTime() {
		Date now = new Date();
		SimpleDateFormat longHourSdf = new SimpleDateFormat("yyyy-MM-dd HH");
		SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			now = longSdf.parse(longHourSdf.format(now) + ":59:59");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return now;
	}
	/**
	 * 获得本月的开始时间，即2012-01-01 00:00:00
	 * @return
	 */
	public static String getCurrentMonthStartTime() {
		Calendar c = Calendar.getInstance();
		Date now = null;
		SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			c.set(Calendar.DATE, 1);
			now = shortSdf.parse(shortSdf.format(c.getTime()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return format(now, YYYY_MM_DD_HH_MM_SS);
	}
	/**
	 * 当前月的结束时间，即2012-01-31 23:59:59
	 * @return
	 */
	public static String getCurrentMonthEndTime() {
		Calendar c = Calendar.getInstance();
		Date now = null;
		SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			c.set(Calendar.DATE, 1);
			c.add(Calendar.MONTH, 1);
			c.add(Calendar.DATE, -1);
			now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return format(now, YYYY_MM_DD_HH_MM_SS);
	}
	/**
	 * 当前年的开始时间，即2012-01-01 00:00:00
	 * @return
	 */
	public static Date getCurrentYearStartTime() {
		Calendar c = Calendar.getInstance();
		Date now = null;
		SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			c.set(Calendar.MONTH, 0);
			c.set(Calendar.DATE, 1);
			now = shortSdf.parse(shortSdf.format(c.getTime()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return now;
	}
	/**
	 * 当前年的结束时间，即2012-12-31 23:59:59
	 * @return
	 */
	public static Date getCurrentYearEndTime() {
		Calendar c = Calendar.getInstance();
		Date now = null;
		SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			c.set(Calendar.MONTH, 11);
			c.set(Calendar.DATE, 31);
			now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return now;
	}
	/**
	 * 当前季度的开始时间，即2012-01-1 00:00:00
	 * @return
	 */
	public static Date getCurrentQuarterStartTime() {
		Calendar c = Calendar.getInstance();
		int currentMonth = c.get(Calendar.MONTH) + 1;
		Date now = null;
		SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			if ((currentMonth >= 1) && (currentMonth <= 3)) {
				c.set(Calendar.MONTH, 0);
			} else if ((currentMonth >= 4) && (currentMonth <= 6)) {
				c.set(Calendar.MONTH, 3);
			} else if ((currentMonth >= 7) && (currentMonth <= 9)) {
				c.set(Calendar.MONTH, 4);
			} else if ((currentMonth >= 10) && (currentMonth <= 12)) {
				c.set(Calendar.MONTH, 9);
			}
			c.set(Calendar.DATE, 1);
			now = longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return now;
	}
	/**
	 * 当前季度的结束时间，即2012-03-31 23:59:59
	 * @return
	 */
	public static Date getCurrentQuarterEndTime() {
		Calendar c = Calendar.getInstance();
		int currentMonth = c.get(Calendar.MONTH) + 1;
		Date now = null;
		SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			if ((currentMonth >= 1) && (currentMonth <= 3)) {
				c.set(Calendar.MONTH, 2);
				c.set(Calendar.DATE, 31);
			} else if ((currentMonth >= 4) && (currentMonth <= 6)) {
				c.set(Calendar.MONTH, 5);
				c.set(Calendar.DATE, 30);
			} else if ((currentMonth >= 7) && (currentMonth <= 9)) {
				c.set(Calendar.MONTH, 8);
				c.set(Calendar.DATE, 30);
			} else if ((currentMonth >= 10) && (currentMonth <= 12)) {
				c.set(Calendar.MONTH, 11);
				c.set(Calendar.DATE, 31);
			}
			now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return now;
	}
	/**
	 * 获取前/后半年的开始时间
	 * @return
	 */
	public static Date getHalfYearStartTime() {
		Calendar c = Calendar.getInstance();
		int currentMonth = c.get(Calendar.MONTH) + 1;
		SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date now = null;
		try {
			if ((currentMonth >= 1) && (currentMonth <= 6)) {
				c.set(Calendar.MONTH, 0);
			} else if ((currentMonth >= 7) && (currentMonth <= 12)) {
				c.set(Calendar.MONTH, 6);
			}
			c.set(Calendar.DATE, 1);
			now = longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return now;
	}
	/**
	 * 获取前/后半年的结束时间
	 * @return
	 */
	public static Date getHalfYearEndTime() {
		Calendar c = Calendar.getInstance();
		int currentMonth = c.get(Calendar.MONTH) + 1;
		SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date now = null;
		try {
			if ((currentMonth >= 1) && (currentMonth <= 6)) {
				c.set(Calendar.MONTH, 5);
				c.set(Calendar.DATE, 30);
			} else if ((currentMonth >= 7) && (currentMonth <= 12)) {
				c.set(Calendar.MONTH, 11);
				c.set(Calendar.DATE, 31);
			}
			now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return now;
	}
	/**
	 * <p>
	 * 方法名
	 * </p>
	 * 指定格式的日期字符串
	 * @param datetime 日期，如2015-03-21
	 * @param format 时间格式，如 00:00:00
	 * @return
	 */
	public static String formatDate(String datetime, String format) {
		String formatdate = "";
		// 判断注册开始时间是否为空
		if ((datetime == null) || "".equals(datetime.trim())) {
			formatdate = null;
		} else {
			formatdate = datetime + " " + format;
		}
		return formatdate;
	}
	/**
	 * 根据当前日期获取本周日的日期
	 * @param time
	 * @return
	 */
	public static String getSunDay(String time) {
		String y = time.substring(0, 4);
		String m = time.substring(5, 7);
		String d = time.substring(8, 10);
		int year = Integer.parseInt(y);
		int month = 0;
		if (m.substring(0, 1).equals("0")) {
			month = Integer.parseInt(m.substring(1, 2));
		} else {
			month = Integer.parseInt(m);
		}
		int day = Integer.parseInt(d);
		Calendar calendar = Calendar.getInstance();// 获得一个日历
		calendar.set(year, month - 1, day);// 设置当前时间,月份是从0月开始计算
		int number = calendar.get(Calendar.DAY_OF_WEEK);// 星期表示1-7，是从星期日开始，
		int addday = 0;
		if (number == 1) {
			addday = 1;
		} else if (number == 2) {
			addday = 0;
		} else {
			addday = 9 - number;
		}
		return getNextDate(time, addday);
	}
	/**
	 * 获取N天后的日期列表
	 * @param days
	 * @return
	 */
	public static List getNextDaysList(int days) {
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		for (int i = 0; i <= days; i++) {
			calendar.setTime(new Date());
			calendar.add(calendar.DATE, i);//
			Date date = calendar.getTime(); // 这个时间就是日期往后推一天的结果
			String dateStr = sdf.format(date); // 增加一天后的日期
			Map<String, String> map = new HashMap<String, String>();
			map.put("requireDate", dateStr);
			list.add(map);
		}
		return list;
	}
	/**
	 * 计算两个日期字符串之间相差的天数
	 * @param smdate
	 * @param bdate
	 * @return
	 * @throws ParseException
	 */
	public static int daysBetween(String smdate, String bdate) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(smdate));
		long time1 = cal.getTimeInMillis();
		cal.setTime(sdf.parse(bdate));
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);
		return Integer.parseInt(String.valueOf(between_days));
	}
	/**
	 * 计算两个日期字符串之间相差的天数
	 * @param smdate
	 * @param bdate
	 * @return
	 * @throws ParseException
	 */
	public static int daysTimeBetween(String smdate, String bdate) throws ParseException {
		int between_days = 0;
		java.text.DateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		java.util.Calendar c1 = java.util.Calendar.getInstance();
		java.util.Calendar c2 = java.util.Calendar.getInstance();
		try {
			c1.setTime(df.parse(smdate));
			c2.setTime(df.parse(bdate));
		} catch (java.text.ParseException e) {
			System.err.println("格式不正确");
		}
		int result = c1.compareTo(c2);
		if (result == 0) {
			between_days = 0;
			System.out.println("c1相等c2");
		} else if (result < 0) {
			between_days = 1;
			System.out.println("c1小于c2");
		} else {
			between_days = 0;
			System.out.println("c1大于c2");
		}
		return between_days;
	}
	public static int calcDaysFromNow(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String curDate = sdf.format(new Date());
		int days = daysBetween(curDate, date);
		return days;
	}
	/**
	 * 获取当前日期days天后的日期
	 * @param days int类型，表示几天后
	 * @return String days后的日期字符串
	 */
	public static String getDaysAfter(int days) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(calendar.DATE, days);
		Date date = calendar.getTime();
		String dateStr = sdf.format(date);
		return dateStr;
	}
	public static void main(String[] args) throws ParseException {
		// System.out.println(isDateStrIn("2015-08-10", "2015-08-10"));
		// String date1 = "2015-08-12 21:23:34";
		// String date2 = "2015-08-17 00:23:34";
		// // System.out.println(daysBetween(date1, date2));
		// System.out.println(calcDaysFromNow(date2));
		// System.out.println(calcDaysFromNow(date1));
		String str = getCurrDateStr("yyyy-MM-dd");
		System.out.println(getCurrDateStr("yyyy-MM-dd"));
		System.out.println(getCurrTimeStr());
		// System.out.println(str.compareTo("2015-09-07"));
		// System.out.println(str.compareTo("2015-09-08") );
		// System.out.println(str.compareTo("2015-09-09") );
	}
}
