package com.highgo.cyyd.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.highgo.cyyd.model.MyCreativeTO;
import com.highgo.cyyd.service.MyCreativeService;

@Controller
@RequestMapping("/main")
public class MainController {

	private Logger logger = LoggerFactory.getLogger(MainController.class);

	public final static int IS_FINISH = 0;
	public final static int IS_PUBLIC = 1;
	public final static int IS_ARCHIVED = 2;
	
	@Autowired
	private MyCreativeService myCreativeService;
	
	@RequestMapping(value = "/index")
	public ModelAndView index(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Enter,");
		
		ModelAndView mv = new ModelAndView("main");
		
		long userId = Long.parseLong(session.getAttribute("userId").toString());
		logger.debug("userId = " + userId);
		
		String name = (String) session.getAttribute("name");
		logger.debug("name = " + name);
		mv.addObject("name",name);
		
		int total = 0;//总数
		int pageCount = 0;//总页数
		int pageIndex = 1;//当前页
		int pageSize = 10;//当前页数量
		
		if(request.getParameter("pageIndex") != null){
			try {
				pageIndex = Integer.parseInt(request.getParameter("pageIndex")) ;
			} catch (Exception e) {
				pageIndex = 1;
			}
		}
		logger.debug("pageIndex = " + pageIndex);
		
		List<MyCreativeTO> count = myCreativeService.getMyCreativeAll(userId);
		pageCount = (count.size() + pageSize-1) / pageSize;
		
		//if exception error.
		if(pageIndex > pageCount){
			pageIndex = 1;
		}
		
		List<MyCreativeTO> list = myCreativeService.getMyCreativeAll(userId,pageIndex,pageSize);
		List<MyCreativeTO> isFinish = myCreativeService.getMyCreativeAll(userId,MainController.IS_FINISH);
		List<MyCreativeTO> isPublic = myCreativeService.getMyCreativeAll(userId,MainController.IS_PUBLIC);
		List<MyCreativeTO> isArchived = myCreativeService.getMyCreativeAll(userId,MainController.IS_ARCHIVED);

		
		total = count.size();
		logger.debug("return total = " + total + ",pageCount = " + pageCount + ",pageIndex = " + pageIndex); 
		
		mv.addObject("finishCount",isFinish.size());
		mv.addObject("publicCount",isPublic.size());
		mv.addObject("archivedCount",isArchived.size());
		
		mv.addObject("total",total);
		mv.addObject("pageCount",pageCount);
		mv.addObject("pageIndex",pageIndex);
		mv.addObject("pageSize",pageSize);
		mv.addObject("list",list);
		
		return mv;
	}


}
