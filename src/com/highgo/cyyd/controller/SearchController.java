package com.highgo.cyyd.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.highgo.cyyd.Util.Constants;
import com.highgo.cyyd.entity.MyAttention;
import com.highgo.cyyd.maker.query.Data;
import com.highgo.cyyd.maker.query.KeywordSearch;
import com.highgo.cyyd.service.MyAttentionService;
import com.highgo.cyyd.service.SearchService;

@Controller
@RequestMapping("/search")

public class SearchController {
	
	private Logger logger = LoggerFactory.getLogger(SearchController.class);
	
	/**展示3层**/
	private String layoutNum = "3";
	
	/**每次5个**/
	private String layoutCount = "5";
	
	/*Show Type*/
	private String showType = "tree";
	
	private ArrayList<HashMap<String, String>> infoList;
	
	@Autowired
	public SearchService searchService;
	
	@Autowired
	public MyAttentionService myAttentionService;

	@RequestMapping(value = "/index")
	public ModelAndView index(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Enter,");
		ModelAndView mv = new ModelAndView("index");

		List<String> historyList = new ArrayList<>();
		
		List<MyAttention> attentionList = this.myAttentionService.getMyAttentionList();
		logger.debug("Size of history is " + historyList.size());
		
		for(MyAttention attention : attentionList) {
			historyList.add(attention.getAttentiveWord());
		}
		mv.addObject("historyList", historyList);
		return mv;
	}
	
	/**
	 * 搜索关键字--文本框搜索引擎
	 * @param value
	 * @param session
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/getSearchContent")
	@ResponseBody
	public Map<String, Object> getSearchContent(@RequestParam("value") String value,HttpSession session,
			HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		
//		List<String> list = null;
//		String lt = "汽车轮胎,车胎,车胎,内胎,车轮";//轮胎
//		String fdj = "内燃机,柴油发动机,柴油机,二冲程,四冲程";//发动机
//		String cfy = "二件套,男款,女款,衣袖,衣服";//冲锋衣
//		if(value.equals("")){
//			String recommend = "防水智能手机,投射虚拟键盘手机壳,便携自拍干手机壳,指纹是表自行车锁,自动系鞋带运动鞋";
//			list = Arrays.asList(recommend.split(","));
//		}
//		else if(value.equals("轮胎")){
//			list = Arrays.asList(lt.split(","));
//		}else if(value.equals("发动机")){
//			list = Arrays.asList(fdj.split(","));
//		}else if(value.equals("冲锋衣")){
//			list = Arrays.asList(cfy.split(","));
//		}
		
//		map.put("list", list);
		return map;
	}
	
	/**
	 * 搜索关键字--推荐信息展示
	 * @param session
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/getRecommend")
	@ResponseBody
	public Map<String, Object> getRecommend(HttpSession session,HttpServletRequest request,HttpServletResponse response){
		Map<String, Object> map = new HashMap<String, Object>();
		String[] str = new String[10];
		str[0] = "【IT168 资讯】虽然现在还不清楚小米Note 2会在何时发布，但该机或有曲屏版的传闻却似乎得到了证实。日前，有网友在微博上首次曝光了疑似小米Note 2的双曲屏前面板谍照，据称采用的是LG的曲面显示屏，传闻会采用金属机身和搭载骁龙821处理器，而售价则可能冲击四千元档次，预计有可能在今年9月份正式与我们见面。";
		str[1] = "什么是冲锋衣？先从名字说起。阿尔卑斯式登山，特别是需要多天行程的山峰，大多数时间，需要自己背着全部的给养和装备，包括羽绒服、睡袋、帐篷等，当接近顶峰不远，大约几个小时之内就可以往返的时候，会扔掉负重，充分利用好天气窗口，快速登顶并返回，这个过程在登山中就叫冲锋，冲锋过程中，为了提高行进速度，只穿最简的服装，带最少的装备，这时穿的衣服就是冲锋衣。";
		str[2] = "发动机（Engine）是一种能够把其它形式的能转化为机械能的机器，包括如内燃机（汽油发动机等）、外燃机（斯特林发动机、蒸汽机等）、电动机等。如内燃机通常是把化学能转化为机械能。发动机既适用于动力发生装置，也可指包括动力装置的整个机器（如：汽油发动机、航空发动机）。";
		str[3] = "轮胎是在各种车辆或机械上装配的接地滚动的圆环形弹性橡胶制品。通常安装在金属轮辋上，能支承车身，缓冲外界冲击，实现与路面的接触并保证车辆的行驶性能。轮胎常在复杂和苛刻的条件下使用，它在行驶时承受着各种变形、负荷、力以及高低温作用，因此必须具有较高的承载性能、牵引性能、缓冲性能。";
		str[4] = "三星公司具有成型的手机屏幕生产链和研究院，因此成就了三星手机在屏幕竞争中的领先水平。三星的Galaxy S6 edge+成为曲面时代绝对领先的创新产品，其屏幕产业上的优势也得到充分发挥。2013年，三星发布曲面屏手机GalaxyNote3，此后一直没有停止创新进程，此后又发布了NoteEdge,S6Edge以及S6Edge+。市场销售情况显示，市场对曲面屏的需求大大超出了预期，三星原本预期Galaxy S6和S6 Edge的销量比例是4：1，市场实际情况却接近1：1，可见曲面屏幕非常受欢迎。";
		str[5] = "尴尬的“四轮电动车”：销售很火爆 上路无牌照。<br>无论是在城市的林荫大道，还是在乡间的羊肠小道，我们经常会遇见这样一种“车”：外形酷似迷你汽车，但却没有汽车跑得快，有时在机动车道上和汽车“竞技赛跑”，有时又在非机动车道上和电动自行车“谁与争锋”。然而至今，这类“车”还没有一个确切的称谓：低速电动车、四轮电动车、老年代步车、迷你观光车……";
		str[6] = "购车补贴退坡、充电设施不足 新能源汽车发展遭掣肘。<br>今年上半年，我国新能源汽车产销翻番。随着人们环保意识提高和产品多元化趋势，分析人士认为，我国新能源车从政策刺激开始向政策和个人需求双重驱动转变，但购车补贴退坡、电动汽车电池续航能力较差、充电设施建设不足等新老问题依然制约行业大踏步发展。";
		str[7] = "现在就流行 这种“不正经”的牛仔裤。<br>比如去年大热门的破洞牛仔裤，一开始只是小破洞，后来就非得破得露出半条腿才时髦。<br>后来又流行缝缝补补又一年的补丁牛仔裤。<br>像B.sides这样的潮牌，专门买来古着Levis牛仔裤剪一剪，再拼成一条新裤子，每条卖到200-300美刀。";
		int number = new Random().nextInt(8);
		map.put("content", str[number]);
		return map;
	}
	
	/**
	 * 关键字描述 -- 展示图形信息
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/searchkey")
	@ResponseBody
	public Map<String, Object> search(HttpServletRequest request,HttpServletResponse response, ModelMap modelMap) throws Exception {
		logger.debug("Enter,");
		
		String keyword = request.getParameter("keyword");
		logger.debug("keyword = " + keyword);
		
		if(request.getParameter("showType") != null) {
			showType = request.getParameter("showType");
		}
	
		String path = request.getSession().getServletContext().getRealPath("") + File.separator + "JSON" + File.separator;
//		String path = "E:/JSON" + File.separator;
		String readJsonPath = this.searchService.createJSONFile(path, keyword, layoutNum, layoutCount,showType);
		logger.debug("JSON path is " + readJsonPath);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("jsonpath", readJsonPath);
		
		//save keyword
		try {
			MyAttention myAttention = new MyAttention();
			myAttention.setAttentiveWord(keyword);
			this.myAttentionService.save(myAttention);
		} catch(Exception e) {
			logger.error(e.getMessage());
//			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 第一步--点击创意“搜索”按钮进入第二步
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/gosearch")
	public String goSearch(HttpServletRequest request) throws Exception {
		logger.debug("Enter,");
		
		if(request.getParameter("id") != null && !request.getParameter("id").isEmpty()){
			String id = request.getParameter("id");
			request.setAttribute("id", id);
			logger.debug("id = " + id);
		}
		
		String keyword = request.getParameter("keyword");
		String sourcetype = request.getParameter("sourcetype");
		request.setAttribute("keyword", keyword);
		request.setAttribute("sourcetype", sourcetype);
		
		//2016.11.04新增
		String selectedKeyword = request.getParameter("selectedKeyword")==null?"":request.getParameter("selectedKeyword");
		request.setAttribute("selectedKeyword", selectedKeyword);
		
		logger.debug("keyword = " + keyword);
		logger.debug("sourcetype = " + sourcetype);
		logger.debug("selectedKeyword = "+ selectedKeyword);
		return "makecreative";
	}
	
	@RequestMapping(value="/loadinfo")
	@ResponseBody
	public Map<String, Object> loadInfo(HttpServletRequest request,HttpServletResponse response, ModelMap modelMa) {
		Map<String, Object> map = new HashMap<String, Object>();
//		String keyword = request.getParameter("keyword");
		List<String> infoList = this.searchService.loadInfoByKeyword("");
		map.put("infolist", infoList);
		
		return map;
	}
	
	/**
	 * 关键字描述--ajax获取关键字的右侧相关信息
	 * @param request
	 * @param response
	 * @param modelMa
	 * @return
	 */
	@RequestMapping(value="/showinfo")
	@ResponseBody
	public Map<String, Object> showRelatedInfo(HttpServletRequest request,HttpServletResponse response, ModelMap modelMa) {
		logger.debug("Enter,");
		
		Map<String, Object> map = new HashMap<String, Object>();
		String keyword = request.getParameter("keyword");
		logger.debug("keyword = " + keyword);
		
		KeywordSearch keySearch = new KeywordSearch();
		String type = Data.PATENT;
		String indexPath = Constants.INDEX_PATH;
		ArrayList<HashMap<String, String>> resList = keySearch.execute(keyword, type, indexPath,60);
		List<String> titleList = new ArrayList<>();
		for (int i = 0; i < resList.size(); i++) {
			if(!resList.get(i).get("title").isEmpty()){
				titleList.add(resList.get(i).get("title") + "$$" + resList.get(i).get("id"));
			}
		}
		this.infoList = resList;
		logger.debug("Related info size is " + titleList.size());
		
		List<String> keyList = new ArrayList<>();
		try {
			keyList = keySearch.recommendWords(10);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		logger.debug("Keyword info size is " + keyList.size());
		map.put("titlelist", titleList);
		map.put("keylist", keyList);
		
		return map;
	}
	
	@RequestMapping(value="/showdetail")
	@ResponseBody
	public Map<String, String> showDetailInfo(HttpServletRequest request,HttpServletResponse response, ModelMap modelMa) {
		logger.debug("Enter,");
		
		Map<String, String> map = new HashMap<String, String>();
		String id = request.getParameter("id");
		logger.debug("Info id " + id);
		
		for(Map<String,String> infoMap : infoList) {
			if(infoMap.get("id").equals(id)) {
				map = infoMap;
//				map.put("infocontent", infoMap.get("abstract"));
//				map.put("infotitle", infoMap.get("title"));
				break;
			}
		}
		logger.debug(" map = " + map.get("id") + ",title = " + map.get("title"));
		return map;
	}
	
	public SearchService getSearchService() {
		return searchService;
	}

	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}

	public MyAttentionService getMyAttentionService() {
		return myAttentionService;
	}

	public void setMyAttentionService(MyAttentionService myAttentionService) {
		this.myAttentionService = myAttentionService;
	}
}
