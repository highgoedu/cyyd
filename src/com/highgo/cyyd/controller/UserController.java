package com.highgo.cyyd.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
public class UserController {
	
	private Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@RequestMapping(value = "/login")
	@ResponseBody
	public Map<String, Object> login(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Enter,");
		
		String userId = request.getParameter("userId");
		logger.debug("userId = " + userId);
		
		//知乎平台id
		String uid = request.getParameter("uid");
		logger.debug("uid = " + uid);
		
		//知乎平台id
		String name = request.getParameter("name");
		logger.debug("name = " + name);
		
		if(userId != null && !"".equals(userId)) {
			session.setAttribute("userId", userId);
			session.setAttribute("uid", uid);
			session.setAttribute("name", name);
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		return map;
	}
	
	@RequestMapping(value = "/exit")
	@ResponseBody
	public Map<String, Object> exit(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Enter,");
		
		session.invalidate();
		
		Map<String, Object> map = new HashMap<String, Object>();
		return map;
	}
}
