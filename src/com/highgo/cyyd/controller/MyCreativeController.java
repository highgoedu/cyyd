package com.highgo.cyyd.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.highgo.cyyd.maker.query.Recommend;
import com.highgo.cyyd.model.AnswerUserTO;
import com.highgo.cyyd.model.MyCreativeTO;
import com.highgo.cyyd.model.SdeAwsPostsIndexTO;
import com.highgo.cyyd.model.SdeAwsQuestionTO;
import com.highgo.cyyd.model.SdeSolutionTO;
import com.highgo.cyyd.service.MyCreativeService;
import com.highgo.cyyd.service.SdeAwsQuestionService;

/**
 * 创意操作类
 * @author admin
 *
 */
@Controller
@RequestMapping("/myCreative")
public class MyCreativeController {
	
	private Logger logger = LoggerFactory.getLogger(MyCreativeController.class);
	
	@Autowired
	private MyCreativeService myCreativeService;
	
	@Autowired
	private SdeAwsQuestionService sdeAwsQuestionService;
	
	/**
	 * 创意保存
	 * @param session
	 * @param request
	 * @param respon
	 * @return
	 */
	@RequestMapping("/creativeSave")
	@ResponseBody
	public Map<String, Object> creativeSave(HttpSession session,HttpServletRequest request,HttpServletResponse respon){
		logger.debug("Enter,");
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		try {
			//有可能是修改返回
			String backId = request.getParameter("id");
			logger.debug("backId = " + backId);
			
			//搜索关键字
			String keyword = request.getParameter("keyword")==null?"":request.getParameter("keyword");
			//数据来源
			String sourcetype = request.getParameter("sourcetype")==null?"":request.getParameter("sourcetype");
			//记事本
			String selectedKeyword = request.getParameter("selectedKeyword")==null?"":request.getParameter("selectedKeyword");
			//创意名称
			String title = request.getParameter("title")==null?"":request.getParameter("title");
			//创意描述
			String content = request.getParameter("content")==null?"":request.getParameter("content");
			
			logger.debug("keyword = " + keyword +" , sourcetype = " + sourcetype);
			logger.debug("selectedKeyword = " + selectedKeyword);
			logger.debug("title = " + title);
			logger.debug("content = " + content);
			
			Object userId = session.getAttribute("userId");
			logger.debug("userId = " + userId);
			
			Object uid = session.getAttribute("uid");
			logger.debug("uid = " + uid);
			
			long id = 0;
			MyCreativeTO to = new MyCreativeTO(); 
			if(backId == null || backId.isEmpty()){
				//新增
				to = new MyCreativeTO();
				to.setStartCreativeTitle(title);
				to.setStartCreativeContent(content);
				to.setEndCreativeTitle(title);
				to.setEndCreativeContent(content);
				to.setSearchKey(keyword);//搜索关键字
				to.setSourcetype(sourcetype);//数据来源
				to.setNotepad(selectedKeyword);//记事本
				to.setUid(Long.parseLong(uid.toString()));
				to.setUserId(Long.parseLong(userId.toString()));//用户id
				id = myCreativeService.save(to);
			}else{
				//修改
				to = myCreativeService.getMyCreativeById(Long.parseLong(backId));
				to.setStartCreativeTitle(title);
				to.setStartCreativeContent(content);
				to.setSearchKey(keyword);
				to.setSourcetype(sourcetype);
				to.setNotepad(selectedKeyword);
				myCreativeService.updateCreative(to);
				id =  to.getId();
			}
			
			map.put("code", "1000");
			map.put("id", id);
			
		} catch (Exception e) {
			e.printStackTrace();
			map.put("result", "1001");
			map.put("errorMsg", e.getMessage());
		}

		return map;
	}
	
	/**
	 * 创意发布
	 * @param session
	 * @param request
	 * @param respon
	 * @return
	 */
	@RequestMapping("/creativePublic")
	@ResponseBody
	public Map<String, Object> creativePublic(HttpSession session,HttpServletRequest request,HttpServletResponse respon){
		logger.debug("Enter,");
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		String id = request.getParameter("id");
		String score = request.getParameter("score")==null||request.getParameter("score").isEmpty()?
				"0":request.getParameter("score");//悬赏积分
		logger.debug("id = " + id + ",score = " + score);
		
		Object uid = session.getAttribute("uid");
		logger.debug("uid = " + uid);
		
		long num = Long.parseLong(id);
		try {
			MyCreativeTO createTo = myCreativeService.getMyCreativeById(Long.parseLong(id));
			
			//add sde_aws_question
			SdeAwsQuestionTO to = new SdeAwsQuestionTO();
			to.setCyydId(num);//创意id
			to.setQuestionContent(createTo.getStartCreativeTitle());//标题
			to.setQuestionDetail(createTo.getStartCreativeContent());//描述
			to.setScore(score);//悬赏得分
			to.setAddTime(System.currentTimeMillis()/1000);
			to.setUpdateTime(System.currentTimeMillis()/1000);
			to.setCategoryId(2);//1.解决方案,2.创意方案
			to.setSource(3);//来源
			to.setPublishedUid(Integer.parseInt(uid.toString()));//知乎平台id
			long questionId = sdeAwsQuestionService.saveSdeAwsQuestion(to);
			
			//add sde_aws_posts_index
			SdeAwsPostsIndexTO to1 = new SdeAwsPostsIndexTO();
			to1.setPostId(questionId);
			to1.setAddTime(System.currentTimeMillis()/1000);
			to1.setUpdateTime(System.currentTimeMillis()/1000);
			to1.setCategoryId(2);//1.解决方案,2.创意方案
			to1.setPostType("question");//固定值
			to1.setuId(Integer.parseInt(uid.toString()));//知乎平台id
			sdeAwsQuestionService.saveSdeAwsPostsIndex(to1);
			
			//update t_mycreative
			createTo.setStatus(1);
			myCreativeService.updateCreative(createTo);
			map.put("code", 1000);
		} catch (Exception e) {
			map.put("code", 1001);
			map.put("msg", e.getMessage());
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 创意归档
	 * @param session
	 * @param request
	 * @param respon
	 * @return
	 */
	@RequestMapping("/creativeArchived")
	@ResponseBody
	public Map<String, Object> creativeArchived(HttpSession session,HttpServletRequest request,HttpServletResponse respon){
		logger.debug("Enter,");
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		String id = request.getParameter("id");
		logger.debug("id = " + id);
		
		long num = Long.parseLong(id);
		try {
			MyCreativeTO createTo = myCreativeService.getMyCreativeById(Long.parseLong(id));
			
			//update SdeAwsQuestion lock
			long questionId = sdeAwsQuestionService.updateSdeAwsQuestion(num);
			
			
			if(questionId > -1){//发布的问题>-1
				//获取评论内容
				List<AnswerUserTO> comments = sdeAwsQuestionService.getCommentsAndUser(questionId);
				logger.debug("comments size = " + comments.size());
				
				//获取4个维度信息
				List<String> scores = sdeAwsQuestionService.getQuestionScore(questionId);
				logger.debug("scores size = " + scores.size());
				
				//根据评论数据进行分词,将分词结果存入数据库
				List<String> list = new ArrayList<>();
				for (int i = 0; i < comments.size(); i++) {
					list.add(comments.get(i).getAnswerContent());
				}
				
				StringBuilder str = new StringBuilder();
				List<String> reuslt = new Recommend().recommend(list, 5);//5=分词数量
				for (int i = 0; i < reuslt.size(); i++) {
					str.append(reuslt.get(i)).append(",");
				}
				if(str.length() > 1){
					createTo.setKeyword(str.substring(0,str.length()-1));//分词结果
				}
			}
			
			Object uid = session.getAttribute("uid");
			logger.debug("uid = " + uid);
			
			//add SdeSolution
			SdeSolutionTO sdeSolution = new SdeSolutionTO();
			sdeSolution.setName(createTo.getStartCreativeTitle());
			sdeSolution.setQuestion(createTo.getStartCreativeContent());
			sdeSolution.setQuestionTime(System.currentTimeMillis()/1000);
			sdeSolution.setSource(2);//1创新方法推荐系统 2创意引导系统
			sdeSolution.setCyydId(num);
			sdeSolution.setUid(Long.parseLong(String.valueOf(uid)));
			sdeAwsQuestionService.saveSdeSolution(sdeSolution);
			

			
			//t_mycreative
			createTo.setStatus(2);
			myCreativeService.updateCreative(createTo);
			map.put("code", 1000);
		} catch (Exception e) {
			e.printStackTrace();
			map.put("code", 1001);
			map.put("msg", e.getMessage());
		}
		return map;
	}
	
	public MyCreativeService getMyCreativeService() {
		return myCreativeService;
	}

	public void setMyCreativeService(MyCreativeService myCreativeService) {
		this.myCreativeService = myCreativeService;
	}
	
	
}
