package com.highgo.cyyd.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.highgo.cyyd.maker.query.Recommend;
import com.highgo.cyyd.model.AnswerUserTO;
import com.highgo.cyyd.model.MyCreativeTO;
import com.highgo.cyyd.model.SdeAwsQuestionTO;
import com.highgo.cyyd.service.MyCreativeService;
import com.highgo.cyyd.service.SdeAwsQuestionService;

@Controller
@RequestMapping("/creative")
public class CreativeEditController {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private MyCreativeService myCreativeService;
	
	@Autowired
	private SdeAwsQuestionService sdeAwsQuestionService;
	
//	private List<String> createList = new ArrayList<>();
	
	/**
	 * 创意编辑
	 * @param session
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/creativeEdit")
	public ModelAndView creativeEdit(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Enter,");
		
		ModelAndView mv = new ModelAndView("creativeEdit");
		
		String selectedKeyword = request.getParameter("selectedKeyword")==null?"":request.getParameter("selectedKeyword");
		String keyword = request.getParameter("keyword")==null?"":request.getParameter("keyword");
		String sourcetype = request.getParameter("sourcetype")==null?"":request.getParameter("sourcetype");
		
		if(request.getParameter("id") != null && !request.getParameter("id").isEmpty()){
			//从发布页面，点击上一步
			String id = request.getParameter("id");
			logger.debug(" id = " + id);
			
			MyCreativeTO to = myCreativeService.getMyCreativeById(Long.parseLong(id));
			
			//有可能在第二部返回修改数据，也有可能在第4步点击上一步
			if(!selectedKeyword.isEmpty() && !selectedKeyword.equals(to.getNotepad())){
				to.setNotepad(selectedKeyword);
			}
			//有可能在第二部返回修改数据，也有可能在第4步点击上一步
			if(!keyword.isEmpty() && !keyword.equals(to.getSearchKey())){
				to.setNotepad(keyword);
			}
			//有可能在第二部返回修改数据，也有可能在第4步点击上一步
			if(!sourcetype.isEmpty() && !sourcetype.equals(to.getSourcetype())){
				to.setNotepad(sourcetype);
			}
			myCreativeService.updateCreative(to);
			
			keyword = to.getSearchKey();
			sourcetype = to.getSourcetype();
			selectedKeyword = to.getNotepad();
			
			mv.addObject("id", id);
			mv.addObject("title", to.getStartCreativeTitle());
			mv.addObject("content", to.getStartCreativeContent());
		}


//		String keys = request.getParameter("keys");
		
//		if(request.getParameter("selectedKeyword") != null && request.getParameter("keys") != null){
//			createList = new ArrayList<>();
//			
////			String[] keyArray = keys.split(" ");
//			String[] selectedArray = selectedKeyword.split(System.lineSeparator());
////			for (int i = 0; i < keyArray.length; i++) {
////				createList.add(keyArray[i] + "(关注主题)");
////			}
//			for (int i = 0; i < selectedArray.length; i++) {
//				createList.add(selectedArray[i]);
////				createList.add(selectedArray[i] + "(标记内容)");
//			}
//		}
//
//		mv.addObject("createList", createList);
		logger.debug("keyword = " + keyword);
		logger.debug("sourcetype = " + sourcetype);
		logger.debug("selectedKeyword = "+ selectedKeyword);
		
		mv.addObject("keyword", keyword);
		mv.addObject("sourcetype", sourcetype);
		mv.addObject("selectedKeyword", selectedKeyword);
		
		return mv;
	}
	
	/**
	 * 创意编辑--跳转到创意发布页面
	 * @param session
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/creativePublic")
	public ModelAndView creativePublic(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		logger.debug("Enter,");
		
		ModelAndView mv = new ModelAndView("creativePublic");
		
		//创意id
		String id = request.getParameter("id")==null?"":request.getParameter("id");
		logger.debug(" id = " + id);
		
		MyCreativeTO to = myCreativeService.getMyCreativeById(Long.parseLong(id));
		
		try {
			//根据创意id找问题questionId
			SdeAwsQuestionTO question = sdeAwsQuestionService.getQuestionIdByCyydId(Long.parseLong(id));
			
			//question == null 说明没有发布
			if(question != null){
				//根据问题id,获取评论内容和4个维度得分,进行分词
				List<AnswerUserTO> comments = sdeAwsQuestionService.getCommentsAndUser(question.getQuestionId());
				logger.debug("comments size = " + comments.size());
				mv.addObject("comments", comments);
				
				List<String> scores = sdeAwsQuestionService.getQuestionScore(question.getQuestionId());
				logger.debug("scores size = " + scores.size());
				mv.addObject("scores_c", scores.get(0));
				mv.addObject("scores_p", scores.get(1));
				mv.addObject("scores_f", scores.get(2));
				mv.addObject("scores_e", scores.get(3));
				
				List<String> reuslt = new ArrayList<>();
				//判断是否已归档,已归档的就不用再分词
				if(to.getStatus() == 2){
					String keyword = to.getKeyword();
					logger.debug("已分词. keyword = " + keyword);
					reuslt = Arrays.asList(keyword.split(","));
				}else{
					logger.debug("调用分词方法");
					List<String> list = new ArrayList<>();
					for (int i = 0; i < comments.size(); i++) {
						list.add(comments.get(i).getAnswerContent());
					}
					reuslt = new Recommend().recommend(list, 5);//5=分词数量
					logger.debug("已分词. reuslt size = " + reuslt.size());
				}
				mv.addObject("keyword", reuslt);
			}

			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		mv.addObject("entity", to);
		return mv;
	}
	
	@RequestMapping(value = "/test")
	public void test(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		sdeAwsQuestionService.getCommentsAndUser(48);
	}
}
