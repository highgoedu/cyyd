package com.highgo.cyyd.model.convertor;

import org.springframework.beans.BeanUtils;

import com.highgo.cyyd.entity.MyCreative;
import com.highgo.cyyd.model.MyCreativeTO;

public class MyCreativeConvertor
{
	public static MyCreativeTO convert(MyCreative entity)
	{
		if (entity == null)
		{
			return null;
		}
		
		MyCreativeTO to = new MyCreativeTO();
		BeanUtils.copyProperties(entity, to);
		
		return to;
	}
	
	public static MyCreative convert(MyCreativeTO to)
	{
		if (to == null)
		{
			return null;
		}
		
		MyCreative entity = new MyCreative();
		BeanUtils.copyProperties(to, entity);
		
		return entity;
	}
}
