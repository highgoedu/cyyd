package com.highgo.cyyd.model.convertor;

import org.springframework.beans.BeanUtils;

import com.highgo.cyyd.entity.SdeAwsPostsIndex;
import com.highgo.cyyd.model.SdeAwsPostsIndexTO;

public class SdeAwsPostsIndexConvertor
{
	public static SdeAwsPostsIndexTO convert(SdeAwsPostsIndex entity)
	{
		if (entity == null)
		{
			return null;
		}
		
		SdeAwsPostsIndexTO to = new SdeAwsPostsIndexTO();
		BeanUtils.copyProperties(entity, to);
		
		return to;
	}
	
	public static SdeAwsPostsIndex convert(SdeAwsPostsIndexTO to)
	{
		if (to == null)
		{
			return null;
		}
		
		SdeAwsPostsIndex entity = new SdeAwsPostsIndex();
		BeanUtils.copyProperties(to, entity);
		
		return entity;
	}
}
