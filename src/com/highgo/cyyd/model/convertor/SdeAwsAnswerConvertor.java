package com.highgo.cyyd.model.convertor;

import org.springframework.beans.BeanUtils;

import com.highgo.cyyd.entity.SdeAwsAnswer;
import com.highgo.cyyd.model.SdeAwsAnswerTO;

public class SdeAwsAnswerConvertor
{
	public static SdeAwsAnswerTO convert(SdeAwsAnswer entity)
	{
		if (entity == null)
		{
			return null;
		}
		
		SdeAwsAnswerTO to = new SdeAwsAnswerTO();
		BeanUtils.copyProperties(entity, to);
		
		return to;
	}
	
	public static SdeAwsAnswer convert(SdeAwsAnswerTO to)
	{
		if (to == null)
		{
			return null;
		}
		
		SdeAwsAnswer entity = new SdeAwsAnswer();
		BeanUtils.copyProperties(to, entity);
		
		return entity;
	}
}
