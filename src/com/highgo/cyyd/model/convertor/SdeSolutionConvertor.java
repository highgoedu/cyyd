package com.highgo.cyyd.model.convertor;

import org.springframework.beans.BeanUtils;

import com.highgo.cyyd.entity.SdeSolution;
import com.highgo.cyyd.model.SdeSolutionTO;

public class SdeSolutionConvertor
{
	public static SdeSolutionTO convert(SdeSolution entity)
	{
		if (entity == null)
		{
			return null;
		}
		
		SdeSolutionTO to = new SdeSolutionTO();
		BeanUtils.copyProperties(entity, to);
		
		return to;
	}
	
	public static SdeSolution convert(SdeSolutionTO to)
	{
		if (to == null)
		{
			return null;
		}
		
		SdeSolution entity = new SdeSolution();
		BeanUtils.copyProperties(to, entity);
		
		return entity;
	}
}
