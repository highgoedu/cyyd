package com.highgo.cyyd.model.convertor;

import org.springframework.beans.BeanUtils;

import com.highgo.cyyd.entity.SdeAwsQuestion;
import com.highgo.cyyd.model.SdeAwsQuestionTO;

public class SdeAwsQuestionConvertor
{
	public static SdeAwsQuestionTO convert(SdeAwsQuestion entity)
	{
		if (entity == null)
		{
			return null;
		}
		
		SdeAwsQuestionTO to = new SdeAwsQuestionTO();
		BeanUtils.copyProperties(entity, to);
		
		return to;
	}
	
	public static SdeAwsQuestion convert(SdeAwsQuestionTO to)
	{
		if (to == null)
		{
			return null;
		}
		
		SdeAwsQuestion entity = new SdeAwsQuestion();
		BeanUtils.copyProperties(to, entity);
		
		return entity;
	}
}
