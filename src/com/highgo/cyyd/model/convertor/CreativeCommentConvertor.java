package com.highgo.cyyd.model.convertor;

import org.springframework.beans.BeanUtils;

import com.highgo.cyyd.entity.CreativeComment;
import com.highgo.cyyd.model.CreativeCommentTO;

public class CreativeCommentConvertor
{
	public static CreativeCommentTO convert(CreativeComment entity)
	{
		if (entity == null)
		{
			return null;
		}
		
		CreativeCommentTO to = new CreativeCommentTO();
		BeanUtils.copyProperties(entity, to);
		
		return to;
	}
	
	public static CreativeComment convert(CreativeCommentTO to)
	{
		if (to == null)
		{
			return null;
		}
		
		CreativeComment entity = new CreativeComment();
		BeanUtils.copyProperties(to, entity);
		
		return entity;
	}
}
