package com.highgo.cyyd.model;

public class SdeSolutionTO {
	
	public long id;
	
	public String name;//创意标题
	
	public String question;//创意描述
	
	public long questionTime;//创建时间
	
	public long uid;//知乎id
	
	public long cyydId;//创意id
	
	public int source;//1创新方法推荐系统 2创意引导系统

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public long getQuestionTime() {
		return questionTime;
	}

	public void setQuestionTime(long questionTime) {
		this.questionTime = questionTime;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public long getCyydId() {
		return cyydId;
	}

	public void setCyydId(long cyydId) {
		this.cyydId = cyydId;
	}

	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}

}
