package com.highgo.cyyd.model;

public class SdeAwsQuestionTO {
	private long questionId;

	private int actionHistoryId;

	private long addTime;

	private int againstCount;

	private int agreeCount;

	private int anonymous;

	private int answerCount;

	private int answerUsers;

	private int bestAnswer;

	//1.解决方案,2.创意方案
	private int categoryId;

	private int commentCount;

	private int focusCount;

	private int hasAttach;

	private String ip;

	private int isRecommend;

	private int lastAnswer;

	private int lock;

	private double popularValue;

	private int popularValueUpdate;

	private int publishedUid;

	private String questionContent;

	private String questionContentFulltext;

	private String questionDetail;

	private int sort;

	private int thanksCount;

	private String unverifiedModify;

	private int unverifiedModifyCount;

	private long updateTime;

	private int viewCount;

	private int source;
	
	//悬赏得分
	private String score;
	
	//创意id
	private long cyydId;
	
	public long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}

	public int getActionHistoryId() {
		return actionHistoryId;
	}

	public void setActionHistoryId(int actionHistoryId) {
		this.actionHistoryId = actionHistoryId;
	}

	public long getAddTime() {
		return addTime;
	}

	public void setAddTime(long addTime) {
		this.addTime = addTime;
	}

	public int getAgainstCount() {
		return againstCount;
	}

	public void setAgainstCount(int againstCount) {
		this.againstCount = againstCount;
	}

	public int getAgreeCount() {
		return agreeCount;
	}

	public void setAgreeCount(int agreeCount) {
		this.agreeCount = agreeCount;
	}

	public int getAnonymous() {
		return anonymous;
	}

	public void setAnonymous(int anonymous) {
		this.anonymous = anonymous;
	}

	public int getAnswerCount() {
		return answerCount;
	}

	public void setAnswerCount(int answerCount) {
		this.answerCount = answerCount;
	}

	public int getAnswerUsers() {
		return answerUsers;
	}

	public void setAnswerUsers(int answerUsers) {
		this.answerUsers = answerUsers;
	}

	public int getBestAnswer() {
		return bestAnswer;
	}

	public void setBestAnswer(int bestAnswer) {
		this.bestAnswer = bestAnswer;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}

	public int getFocusCount() {
		return focusCount;
	}

	public void setFocusCount(int focusCount) {
		this.focusCount = focusCount;
	}

	public int getHasAttach() {
		return hasAttach;
	}

	public void setHasAttach(int hasAttach) {
		this.hasAttach = hasAttach;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getIsRecommend() {
		return isRecommend;
	}

	public void setIsRecommend(int isRecommend) {
		this.isRecommend = isRecommend;
	}

	public int getLastAnswer() {
		return lastAnswer;
	}

	public void setLastAnswer(int lastAnswer) {
		this.lastAnswer = lastAnswer;
	}

	public int getLock() {
		return lock;
	}

	public void setLock(int lock) {
		this.lock = lock;
	}

	public double getPopularValue() {
		return popularValue;
	}

	public void setPopularValue(double popularValue) {
		this.popularValue = popularValue;
	}

	public int getPopularValueUpdate() {
		return popularValueUpdate;
	}

	public void setPopularValueUpdate(int popularValueUpdate) {
		this.popularValueUpdate = popularValueUpdate;
	}

	public int getPublishedUid() {
		return publishedUid;
	}

	public void setPublishedUid(int publishedUid) {
		this.publishedUid = publishedUid;
	}

	public String getQuestionContent() {
		return questionContent;
	}

	public void setQuestionContent(String questionContent) {
		this.questionContent = questionContent;
	}

	public String getQuestionContentFulltext() {
		return questionContentFulltext;
	}

	public void setQuestionContentFulltext(String questionContentFulltext) {
		this.questionContentFulltext = questionContentFulltext;
	}

	public String getQuestionDetail() {
		return questionDetail;
	}

	public void setQuestionDetail(String questionDetail) {
		this.questionDetail = questionDetail;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public int getThanksCount() {
		return thanksCount;
	}

	public void setThanksCount(int thanksCount) {
		this.thanksCount = thanksCount;
	}

	public String getUnverifiedModify() {
		return unverifiedModify;
	}

	public void setUnverifiedModify(String unverifiedModify) {
		this.unverifiedModify = unverifiedModify;
	}

	public int getUnverifiedModifyCount() {
		return unverifiedModifyCount;
	}

	public void setUnverifiedModifyCount(int unverifiedModifyCount) {
		this.unverifiedModifyCount = unverifiedModifyCount;
	}

	public long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(int updateTime) {
		this.updateTime = updateTime;
	}

	public int getViewCount() {
		return viewCount;
	}

	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}

	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}

	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public long getCyydId() {
		return cyydId;
	}

	public void setCyydId(long cyydId) {
		this.cyydId = cyydId;
	}
	
}
