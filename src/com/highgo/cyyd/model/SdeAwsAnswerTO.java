package com.highgo.cyyd.model;

public class SdeAwsAnswerTO {

	private long answerId;

	private long questionId;
	
	private String answerContent;//回复内容
	
	private long addTime;

	private long uid;//知乎id
	
	//创新性-平均值
	private double avgCreativity;
	
	public long getAnswerId() {
		return answerId;
	}

	public void setAnswerId(long answerId) {
		this.answerId = answerId;
	}

	public long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}

	public String getAnswerContent() {
		return answerContent;
	}

	public void setAnswerContent(String answerContent) {
		this.answerContent = answerContent;
	}

	public long getAddTime() {
		return addTime;
	}

	public void setAddTime(long addTime) {
		this.addTime = addTime;
	}

	public double getAvgCreativity() {
		return avgCreativity;
	}

	public void setAvgCreativity(double avgCreativity) {
		this.avgCreativity = avgCreativity;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

}