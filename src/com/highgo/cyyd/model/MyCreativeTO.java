package com.highgo.cyyd.model;

public class MyCreativeTO {
	public long id;
	
	public String startCreativeTitle;
	
	public String startCreativeContent;
	
	public String endCreativeTitle;
	
	public String endCreativeContent;
	
	//页面展示内容描述的缩放
	public String content;
	
	public String createTime;
	//停用
	public String endTime;
	//知乎id
	private long uid;
	//门户id
	public long userId;
	//分词
	private String keyword;
	
	//0:已完成,1： 已发布,2：已归档
	private int status;
	//0:可见,1:不可见	
	private int isVisible;
	//搜索关键字
	private String searchKey;
	//数据来源
	private String sourcetype;
	//记事本
	private String notepad;
	//知乎名称
	private String userName;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStartCreativeTitle() {
		return startCreativeTitle;
	}

	public void setStartCreativeTitle(String startCreativeTitle) {
		this.startCreativeTitle = startCreativeTitle;
	}

	public String getStartCreativeContent() {
		return startCreativeContent;
	}

	public void setStartCreativeContent(String startCreativeContent) {
		this.startCreativeContent = startCreativeContent;
	}

	public String getEndCreativeTitle() {
		return endCreativeTitle;
	}

	public void setEndCreativeTitle(String endCreativeTitle) {
		this.endCreativeTitle = endCreativeTitle;
	}

	public String getEndCreativeContent() {
		return endCreativeContent;
	}

	public void setEndCreativeContent(String endCreativeContent) {
		this.endCreativeContent = endCreativeContent;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getIsVisible() {
		return isVisible;
	}

	public void setIsVisible(int isVisible) {
		this.isVisible = isVisible;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getNotepad() {
		return notepad;
	}

	public void setNotepad(String notepad) {
		this.notepad = notepad;
	}

	public String getSearchKey() {
		return searchKey;
	}

	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}

	public String getSourcetype() {
		return sourcetype;
	}

	public void setSourcetype(String sourcetype) {
		this.sourcetype = sourcetype;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
