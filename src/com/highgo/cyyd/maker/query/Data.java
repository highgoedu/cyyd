package com.highgo.cyyd.maker.query;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 数据类的父类。将来可能会出现很多种数据，都继承这个类。
 * @author sde
 *
 */
public abstract class Data {
	
	private Logger logger = LoggerFactory.getLogger(Data.class);
	
	public static String PATENT = "专利";
	protected String path = null;
	protected HashMap<String, String> allInfo = new HashMap<String, String>();
	
	public Data(){
	}
	public Data(String path){
		this.path=path;
	}
	
	/**
	 * 将读取到的数据格式化存储到Map对象中。方便读取。
	 * @param text 使用readText方法读取到的数据。
	 */
	public  abstract void getInfo(String text);
	
	/**
	 *	利用path路径读取对应的txt文件。 
	 * @return
	 */
	public  String readText(){
		String codes = "";
		File file = new File(this.path);   
//		File file = new File("");
		try {  
		    BufferedReader reader = new BufferedReader(new FileReader(file));  
		    String line = reader.readLine();  
		    while(line!=null){
		    	line +="\n";
		    	codes+=line;
		        line = reader.readLine();  
		    }  
		    reader.close();
		
		} catch (FileNotFoundException e) {  
//		    e.printStackTrace();
			logger.error(e.getMessage());
		} catch (IOException e) {  
//		    e.printStackTrace();  
			logger.error(e.getMessage());
		} catch (Exception e) {
//			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return codes;
	}
	public void setPath(String path){
		this.path=path;
	}
	public String getPath(){
		return this.path;
	}
	public HashMap<String, String> getAllInfo(){
		return this.allInfo;
	}
}
