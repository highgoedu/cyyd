package com.highgo.cyyd.maker.query;

public class Patent extends Data {
	public Patent(){
		super();
	}
	public  Patent(String path) {
		super(path);
	}
	@Override
	public void getInfo(String text) {
		// TODO Auto-generated method stub
		String infos[] = text.split("\n");
		String info = "";
		
		for (int i = 0; i < infos.length; i++) {
			if (i==0) {
				this.allInfo.put("title", infos[i]);
			}
			if (i==2) {
				this.allInfo.put("abstract", infos[i]);
			}
			if (i>3) {
				info+=infos[i]+"\n";
			}
		}
		this.allInfo.put("info", info);
		this.allInfo.put("url", "");
	}
}
