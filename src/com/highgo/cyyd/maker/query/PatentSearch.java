package com.highgo.cyyd.maker.query;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wltea.analyzer.lucene.IKAnalyzer;

public class PatentSearch extends Search {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public PatentSearch(String indexPath) {
		super(indexPath);
	}

	public PatentSearch(String indexPath, int maxSearchNum) {
		super(indexPath, maxSearchNum);
	}

	@Override
	public ArrayList<String> query(String keyword) throws IOException,
			ParseException {
		logger.debug("Enter, new File 存储位置 = " + this.indexPath);
		
		DirectoryReader ireader = DirectoryReader.open(FSDirectory
				.open(new File(this.indexPath))); // 打开存储位置

		IndexSearcher searcher = new IndexSearcher(ireader); // 创建搜索器

		Analyzer analyzer = new IKAnalyzer(true);

		QueryParser qp = new QueryParser(Version.LUCENE_47,this.searchField, analyzer);
		Query IKquery = qp.parse(keyword);

		Term t = new Term(this.searchField, keyword);
		Query FuQuery = new FuzzyQuery(t, 2); // 模糊度查询
		ArrayList<String> list = new ArrayList<String>();
		int number = 0;
		if (keyword.length() >= 5) {// 字符串长度大于等于5，采用语句查询
			number = search(searcher, IKquery, list);
//			System.out.println("共有" + number + "条结果");
//			for (String path : list) {
//				System.out.println(path);
//			}
		} else {// 小于5的时候，采用模糊查询
			number = search(searcher, FuQuery, list);
			if (number == 0) {// 模糊查询没有查到的话，再次采用语句查询
				number = search(searcher, IKquery, list);
			}
//			System.out.println("共有" + number + "条结果");
//			for (String path : list) {
//				System.out.println(path);
//			}
		}
		return list;
	}

	public int search(IndexSearcher searcher, Query query, List<String> list)
			throws IOException {

		TopDocs td = null;
		try {
			if (searcher != null) {
				Sort sort = new Sort();
				td = searcher.search(query, this.maxSearchNum, sort);
				ScoreDoc[] scoreDocs = td.scoreDocs;
				for (ScoreDoc scoreDoc : scoreDocs) {
					Document doc = searcher.doc(scoreDoc.doc);
					//System.out.println(doc.getFields().toString());
					list.add(adjustFilePath(doc.get(this.returnField)));
				}
				return td.totalHits;

			} else {
				return -1;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}

	}
}
