package com.highgo.cyyd.maker.query;

import java.util.ArrayList;
import java.util.List;import javax.naming.spi.DirStateFactory.Result;

import com.hankcs.hanlp.HanLP;

public class Recommend {
	
	public List<String> recommend(List<String> list,int size){
		String info = "";
		for (int i = 0; i < list.size(); i++) {
			info+=list.get(i);
		}
		return recommend(info, size);
	}
	public List<String> recommend(String str,int size){
		return HanLP.extractPhrase(str, size);
	}
	
	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		list.add("能家居系统各设备间需要通讯线进行连接，控制器到执行器需要连接，由于灯光等执行器较多，并且分布在多个房间，所以造成现场布线复杂。[b][/b] 技术矛盾1：如果采用分散控制，可以减少布线的数量，但由于模块需要分成多个小模块去生产，导致可制造性差。[b][/b] 技术矛盾2：如果系统中电能传输介质体积大幅减小，将会改善对墙体的破坏，但是会导致生产中需要更多的稀有金属材料。[b][/b]");
		list.add("企鹅企鹅企鹅");
		List<String> reuslt = new Recommend().recommend(list, 5);
		for (int i = 0; i < reuslt.size(); i++) {
			System.out.println(reuslt.get(i));
		}
	}
}
