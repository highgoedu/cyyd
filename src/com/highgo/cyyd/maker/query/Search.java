package com.highgo.cyyd.maker.query;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.lucene.queryparser.classic.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 查询的父类。提供一个查询的抽象方法。
 * 
 * @author sde
 */
public abstract class Search {
	
	private Logger logger = LoggerFactory.getLogger(Search.class);

	/* 搜索的索引位置 */
	protected String indexPath = "F:/数据/index";
	/* 搜索的条目最大数目 */
	protected int maxSearchNum = 20;
	/*搜索索引时搜索的域名*/
	protected String searchField ="content";
	/*搜索之后返回的信息的域名*/
	protected String returnField ="path"; 

	public Search(String indexPath) {
		this.indexPath = indexPath;
	}
	public Search(String indexPath, int maxSearchNum) {
		this.indexPath = indexPath;
		this.maxSearchNum = maxSearchNum;
	}
	
	public abstract ArrayList<String> query(String keyword) throws IOException,
			ParseException;
	/**
	 * 当文件存储的路径发生变化，但索引存储的路径没有变化。用于将两者调整一下，达成一致。
	 * 比如，索引位置是：F：/数据/index。某个文件的索引存储的位置是：F：/数据/服装/1.txt。
	 * 当文件发生位置变化，索引位置是：E：/data/index。此文件对应的存储位置是：F：/data/服装/1.txt。
	 * 直接拿出索引的存储路径，肯定打不开。首先得到根目录：E：/data。然后得到文件目录：服装/1.txt。
	 * 然后将两者进行拼接。
	 * @param path 要被调整的路径
	 * @return 调整之后的路径。
	 */
	public String adjustFilePath(String path){
		logger.debug("path = "+ path);
		
//		int index = indexPath.lastIndexOf("\\");//2016.11.03注释，原因需要兼容linux
		int index = indexPath.lastIndexOf("/");
		
		String leftPath = indexPath.substring(0,index);
		String str[] = path.split("\\\\");
		String rightPath = str[str.length-2]+File.separator+str[str.length-1];
		return leftPath+File.separator+rightPath;
	}
	
	public void setIndexPath(String indexPath) {
		this.indexPath = indexPath;
	}

	public String getIndexPath() {
		return indexPath;
	}

	public void setMaxSearchNum(int maxSearchNum) {
		this.maxSearchNum = maxSearchNum;
	}

	public int getMaxSearchNum() {
		return maxSearchNum;
	}
	
	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}

	public String getSearchField() {
		return searchField;
	}
	
	public void setReturnField(String returnField) {
		this.returnField = returnField;
	}

	public String getReturnField() {
		return returnField;
	}
}
