package com.highgo.cyyd.maker.query;

import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import com.hankcs.hanlp.HanLP;

import java.util.List;
import org.apache.lucene.queryparser.classic.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KeywordSearch {
	
	private Logger logger = LoggerFactory.getLogger(KeywordSearch.class);

	//用于保存搜索到的所有数据中的所有关键词句，然后进行推荐。
	private String info ="";
	/**
	 * 搜索关键词的执行类
	 * 
	 * @param keyword
	 *            搜索关键词
	 * @param type
	 *            搜索的数据类型
	 * @param indexPath
	 *            索引保存的位置
	 * @param maxSearchNum
	 *            检索到的条目的最大数目
	 * @param searchField
	 *            搜索的域
	 * @return 包含所有与关键词相关的信息的list
	 */
	public ArrayList<HashMap<String, String>> execute(String keyword,
			String type, String indexPath, int maxSearchNum, String searchField) {
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		ArrayList<String> pathList;
		Data data = null;
		Search search = null;
		switch (type) {
		case "专利":
			search = new PatentSearch(indexPath);
			search.setMaxSearchNum(maxSearchNum);
			search.setSearchField(searchField);
			try {
				pathList = search.query(keyword);
				for (int i = 0; i < pathList.size(); i++) {
					data = new Patent();
					data.setPath(pathList.get(i));
					data.getInfo(data.readText());
					data.getAllInfo().put("id", Data.PATENT+(i+1));
					list.add(data.getAllInfo());
					info+=data.getAllInfo().get("title")+data.getAllInfo().get("abstract");
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				e.printStackTrace();
			}catch (Exception e) {
				logger.error("switch (type) " + e.getMessage());
			}
			break;
		default:
			System.out.println("请输入正确的爬取类型");
			break;
		}
		

		return list;
	}

	public ArrayList<HashMap<String, String>> execute(String keyword,
			String type, String indexPath) {
		return execute(keyword, type, indexPath, 20, "content");
	}

	public ArrayList<HashMap<String, String>> execute(String keyword,
			String type, String indexPath, int maxSearchNum) {
		return execute(keyword, type, indexPath, maxSearchNum, "content");
	}

	public ArrayList<HashMap<String, String>> execute(String keyword,
			String type, String indexPath, String searchField) {
		return execute(keyword, type, indexPath, 20, searchField);
	}
	
	public List<String>  recommendWords(int maxNum){
		return HanLP.extractPhrase(info, maxNum);
	}
}
