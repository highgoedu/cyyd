package com.highgo.cyyd.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;

public interface BaseDao {
	void save(Object obj);
	void deleteObject(Object obj);
	void updateObject(Object obj);
	void saveOrUpdateObject(Object obj);
	List<?> pageQueryViaParam(String hql, Integer pageSize, Integer page, String[] params, Object... p);
	<T> List<?> getAllObject(Class<T> claze);
	
	public List<?> executeHql(String hql);
	
	/**
	 * 根据id查询对象记录
	 * 
	 * @param 查询对象类型
	 * @param 查询对象ID
	 * @return 查询对象结果
	 */
	public <T> T getObjectById(Class<T> c, Serializable id);
	
	/**
	 * 执行sql
	 * 
	 * @param sql
	 *            执行sql语句
	 */
	public List<Object[]> executeSql(String sql);
	
	public List<Object[]> SqlPageQuery(String sql , final Integer pageSize, final Integer page, final Object... p);
}
