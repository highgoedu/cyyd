package com.highgo.cyyd.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.highgo.cyyd.dao.BaseDao;

@Repository
public class BaseDaoImpl implements BaseDao {
	@Autowired
	@Resource(name = "sessionFactory")
	private SessionFactory sessionFactory ;
	
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public void save(Object obj) {
		getSession().save(obj);
	}
	
	public void deleteObject(Object obj) {
		getSession().delete(obj);
		
	}
	
	public void updateObject(Object obj) {
		getSession().update(obj);
	}
	
	public void saveOrUpdateObject(Object obj) {
		getSession().saveOrUpdate(obj);
	}
	
	/**
	 * 分页查询
	 * @param hql
	 * @param pageSize
	 * @param page
	 * @param params
	 * @param p
	 * @return
	 */
	public List<?> pageQueryViaParam(String hql, Integer pageSize, Integer page, String[] params, Object... p) {
		Query query = getSession().createQuery(hql);
		if (p != null) {
			for (int i = 0; i < p.length; i++) {
				query.setParameter(params[i], p[i]);
			}
		}
		if (pageSize != null && pageSize > 0 && page != null && page > 0) {
			query.setFirstResult((page - 1) * pageSize).setMaxResults(pageSize);
		}
		return query.list();
	}
	
	public <T> List<?> getAllObject(Class<T> claze) {
		
		Query query = getSession().createQuery("from " + claze.getName());
		return query.list();
	}

	/**
	 * 执行hql
	 * 
	 * @param hql
	 *            执行hql语句
	 */
	public List<?> executeHql(String hql) {
		Query query = getSession().createQuery(hql);
		return query.list();
	}
	
	/**
	 * 根据id查询对象记录
	 * 
	 * @param 查询对象类型
	 * @param 查询对象ID
	 * @return 查询对象结果
	 */
	public <T> T getObjectById(Class<T> c, Serializable id) {
		return (T) getSession().get(c, id);
	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	/**
	 * 执行sql
	 * 
	 * @param sql
	 *            执行sql语句
	 */
	public List<Object[]> executeSql(String sql) {
		SQLQuery query = getSession().createSQLQuery(sql);
		return query.list();
	}
	
	public List<Object[]> SqlPageQuery(String sql , final Integer pageSize, final Integer page, final Object... p){
		Query query = getSession().createSQLQuery(sql);
		if (p != null) {
			for (int i = 0; i < p.length; i++) {
				query.setParameter(i, p[i]);
			}
		}
		if (pageSize != null && pageSize > 0 && page != null && page > 0) {
			query.setFirstResult((page - 1) * pageSize).setMaxResults(pageSize);
		}
		return query.list();
	}
}
