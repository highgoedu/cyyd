package com.highgo.cyyd.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;


/**
 * The persistent class for the sde_aws_question database table.
 * 
 */
@Entity
@Table(name="sde_aws_question")
public class SdeAwsQuestion implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="question_id")
	private long questionId;

	@Column(name="action_history_id")
	private int actionHistoryId;

	@Column(name="add_time")
	private long addTime;

	@Column(name="against_count")
	private int againstCount;

	@Column(name="agree_count")
	private int agreeCount;

	private int anonymous;

	@Column(name="answer_count")
	private int answerCount;

	@Column(name="answer_users")
	private int answerUsers;

	@Column(name="best_answer")
	private int bestAnswer;

	@Column(name="category_id")//1.解决方案,2.创意方案
	private int categoryId;

//	@Column(name="chapter_id")
//	private int chapterId;

	@Column(name="comment_count")
	private int commentCount;

	@Column(name="focus_count")
	private int focusCount;

	@Column(name="has_attach")
	private int hasAttach;

	@Column(name="ip")
	private String ip;

	@Column(name="is_recommend")
	private int isRecommend;

	@Column(name="last_answer")
	private int lastAnswer;

	@Column(name="`lock`")
	private int lock;

	@Column(name="popular_value")
	private double popularValue;

	@Column(name="popular_value_update")
	private int popularValueUpdate;

	@Column(name="published_uid")
	private int publishedUid;

	@Column(name="question_content")
	private String questionContent;

	@Lob
	@Column(name="question_content_fulltext")
	private String questionContentFulltext;

	@Lob
	@Column(name="question_detail")
	private String questionDetail;

//	@Column(name="received_email_id")
//	private int receivedEmailId;

	private int sort;

	@Column(name="thanks_count")
	private int thanksCount;

	@Lob
	@Column(name="unverified_modify")
	private String unverifiedModify;

	@Column(name="unverified_modify_count")
	private int unverifiedModifyCount;

	@Column(name="update_time")
	private long updateTime;

	@Column(name="view_count")
	private int viewCount;

//	@Column(name="weibo_msg_id")
//	private int weiboMsgId;

	@Column(name="source")
	private int source;
	
	//悬赏得分
	@Column(name="score")
	private String score;
	
	@Column(name="cyyd_post_id")//创意id
	private long cyydId;
	
	public SdeAwsQuestion() {
	}

	public long getQuestionId() {
		return this.questionId;
	}

	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}

	public int getActionHistoryId() {
		return this.actionHistoryId;
	}

	public void setActionHistoryId(int actionHistoryId) {
		this.actionHistoryId = actionHistoryId;
	}

	public long getAddTime() {
		return this.addTime;
	}

	public void setAddTime(long addTime) {
		this.addTime = addTime;
	}

	public int getAgainstCount() {
		return this.againstCount;
	}

	public void setAgainstCount(int againstCount) {
		this.againstCount = againstCount;
	}

	public int getAgreeCount() {
		return this.agreeCount;
	}

	public void setAgreeCount(int agreeCount) {
		this.agreeCount = agreeCount;
	}

	public int getAnonymous() {
		return this.anonymous;
	}

	public void setAnonymous(int anonymous) {
		this.anonymous = anonymous;
	}

	public int getAnswerCount() {
		return this.answerCount;
	}

	public void setAnswerCount(int answerCount) {
		this.answerCount = answerCount;
	}

	public int getAnswerUsers() {
		return this.answerUsers;
	}

	public void setAnswerUsers(int answerUsers) {
		this.answerUsers = answerUsers;
	}

	public int getBestAnswer() {
		return this.bestAnswer;
	}

	public void setBestAnswer(int bestAnswer) {
		this.bestAnswer = bestAnswer;
	}

	public int getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getCommentCount() {
		return this.commentCount;
	}

	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}

	public int getFocusCount() {
		return this.focusCount;
	}

	public void setFocusCount(int focusCount) {
		this.focusCount = focusCount;
	}

	public int getHasAttach() {
		return this.hasAttach;
	}

	public void setHasAttach(int hasAttach) {
		this.hasAttach = hasAttach;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getIsRecommend() {
		return this.isRecommend;
	}

	public void setIsRecommend(int isRecommend) {
		this.isRecommend = isRecommend;
	}

	public int getLastAnswer() {
		return this.lastAnswer;
	}

	public void setLastAnswer(int lastAnswer) {
		this.lastAnswer = lastAnswer;
	}

	public int getLock() {
		return this.lock;
	}

	public void setLock(int lock) {
		this.lock = lock;
	}

	public double getPopularValue() {
		return this.popularValue;
	}

	public void setPopularValue(double popularValue) {
		this.popularValue = popularValue;
	}

	public int getPopularValueUpdate() {
		return this.popularValueUpdate;
	}

	public void setPopularValueUpdate(int popularValueUpdate) {
		this.popularValueUpdate = popularValueUpdate;
	}

	public int getPublishedUid() {
		return this.publishedUid;
	}

	public void setPublishedUid(int publishedUid) {
		this.publishedUid = publishedUid;
	}

	public String getQuestionContent() {
		return this.questionContent;
	}

	public void setQuestionContent(String questionContent) {
		this.questionContent = questionContent;
	}

	public String getQuestionContentFulltext() {
		return this.questionContentFulltext;
	}

	public void setQuestionContentFulltext(String questionContentFulltext) {
		this.questionContentFulltext = questionContentFulltext;
	}

	public String getQuestionDetail() {
		return this.questionDetail;
	}

	public void setQuestionDetail(String questionDetail) {
		this.questionDetail = questionDetail;
	}

	public int getSort() {
		return this.sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public int getThanksCount() {
		return this.thanksCount;
	}

	public void setThanksCount(int thanksCount) {
		this.thanksCount = thanksCount;
	}

	public String getUnverifiedModify() {
		return this.unverifiedModify;
	}

	public void setUnverifiedModify(String unverifiedModify) {
		this.unverifiedModify = unverifiedModify;
	}

	public int getUnverifiedModifyCount() {
		return this.unverifiedModifyCount;
	}

	public void setUnverifiedModifyCount(int unverifiedModifyCount) {
		this.unverifiedModifyCount = unverifiedModifyCount;
	}

	public long getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}

	public int getViewCount() {
		return this.viewCount;
	}

	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}

	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public long getCyydId() {
		return cyydId;
	}

	public void setCyydId(long cyydId) {
		this.cyydId = cyydId;
	}

}