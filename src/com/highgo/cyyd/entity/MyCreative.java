package com.highgo.cyyd.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="t_mycreative")
public class MyCreative {
	@Id
	@GeneratedValue
	public long id;
	
	public String startCreativeTitle;
	
	public String startCreativeContent;
	
	public String endCreativeTitle;
	
	public String endCreativeContent;
	
	public String createTime;
	
	//停用
	public String endTime;
	
	//知乎id
	private long uid;
	//门户id
	public long userId;
	
	private String keyword;

	//0:已完成,1： 已发布,2：已归档
	private int status;
	
	//0:可见,1:不可见
	private int isVisible;
	
	//评论数量
	private int commentCount;
	
	//搜索关键字
	private String searchKey;
	//数据来源
	private String sourcetype;
	//记事本
	private String notepad;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStartCreativeTitle() {
		return startCreativeTitle;
	}

	public void setStartCreativeTitle(String startCreativeTitle) {
		this.startCreativeTitle = startCreativeTitle;
	}

	public String getStartCreativeContent() {
		return startCreativeContent;
	}

	public void setStartCreativeContent(String startCreativeContent) {
		this.startCreativeContent = startCreativeContent;
	}

	public String getEndCreativeTitle() {
		return endCreativeTitle;
	}

	public void setEndCreativeTitle(String endCreativeTitle) {
		this.endCreativeTitle = endCreativeTitle;
	}

	public String getEndCreativeContent() {
		return endCreativeContent;
	}

	public void setEndCreativeContent(String endCreativeContent) {
		this.endCreativeContent = endCreativeContent;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getIsVisible() {
		return isVisible;
	}

	public void setIsVisible(int isVisible) {
		this.isVisible = isVisible;
	}

	public String getNotepad() {
		return notepad;
	}

	public void setNotepad(String notepad) {
		this.notepad = notepad;
	}

	public String getSearchKey() {
		return searchKey;
	}

	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}

	public String getSourcetype() {
		return sourcetype;
	}

	public void setSourcetype(String sourcetype) {
		this.sourcetype = sourcetype;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}
	
}
