package com.highgo.cyyd.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the sde_aws_answer database table.
 * 
 */
@Entity
@Table(name="sde_aws_answer")
public class SdeAwsAnswer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="answer_id")
	private int answerId;

	@Column(name="question_id")
	private int questionId;
	
	@Column(name="answer_content")
	private String answerContent;//回复内容
	
	@Column(name="add_time")
	private long addTime;

	@Column(name="creativity")
	private String creativity;//创新性
	@Column(name="economy")
	private String economy;//经济性
	@Column(name="feasibility")
	private String feasibility;//可行性
	@Column(name="practicability")
	private String practicability;//实用性
	
//	@Column(name="against_count")
//	private int againstCount;//反对人数

//	@Column(name="agree_count")
//	private int agreeCount;//支持人数

//	@Column(name="anonymous")
//	private int anonymous;//匿名

//	@Column(name="category_id")
//	private int categoryId;//分类，//1.解决方案,2.创意方案

//	@Column(name="comment_count")
//	private int commentCount;//评论总数

//	@Column(name="force_fold")
//	private byte forceFold;//是否折叠

//	@Column(name="has_attach")
//	private byte hasAttach;//附件

//	@Column(name="ip")
//	private String ip;

//	@Column(name="publish_source")
//	private String publishSource;//发布来源

//	@Column(name="thanks_count")
//	private int thanksCount;//感谢数量

//	@Column(name="uninterested_count")
//	private int uninterestedCount;//不感兴趣数量
	
	@Column(name="uid")
	private int uid;//知乎发布id

	public SdeAwsAnswer() {
	}

	public int getAnswerId() {
		return answerId;
	}

	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public String getAnswerContent() {
		return answerContent;
	}

	public void setAnswerContent(String answerContent) {
		this.answerContent = answerContent;
	}

	public long getAddTime() {
		return addTime;
	}

	public void setAddTime(long addTime) {
		this.addTime = addTime;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getCreativity() {
		return creativity;
	}

	public void setCreativity(String creativity) {
		this.creativity = creativity;
	}

	public String getEconomy() {
		return economy;
	}

	public void setEconomy(String economy) {
		this.economy = economy;
	}

	public String getFeasibility() {
		return feasibility;
	}

	public void setFeasibility(String feasibility) {
		this.feasibility = feasibility;
	}

	public String getPracticability() {
		return practicability;
	}

	public void setPracticability(String practicability) {
		this.practicability = practicability;
	}

}