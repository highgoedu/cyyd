package com.highgo.cyyd.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the sde_aws_question database table.
 * 
 */
@Entity
@Table(name="sde_aws_posts_index")
public class SdeAwsPostsIndex implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private long id;
	
	@Column(name="post_id")
	private long postId;

	@Column(name="post_type")
	private String postType;

	@Column(name="add_time")
	private long addTime;

	@Column(name="update_time")
	private long updateTime;
	
	@Column(name="anonymous")
	private int anonymous;

	@Column(name="category_id")
	private int categoryId;

	@Column(name="is_recommend")
	private int isRecommend;

	@Column(name="`lock`")
	private int lock;

	@Column(name="popular_value")
	private double popularValue;

	@Column(name="uid")
	private int uId;

	@Column(name="answer_count")
	private int answerCount;

	@Column(name="agree_count")
	private int agreeCount;

	@Column(name="view_count")
	private int viewCount;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getPostId() {
		return postId;
	}

	public void setPostId(long postId) {
		this.postId = postId;
	}

	public String getPostType() {
		return postType;
	}

	public void setPostType(String postType) {
		this.postType = postType;
	}

	public long getAddTime() {
		return addTime;
	}

	public void setAddTime(long addTime) {
		this.addTime = addTime;
	}

	public long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}

	public int getAnonymous() {
		return anonymous;
	}

	public void setAnonymous(int anonymous) {
		this.anonymous = anonymous;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getIsRecommend() {
		return isRecommend;
	}

	public void setIsRecommend(int isRecommend) {
		this.isRecommend = isRecommend;
	}

	public int getLock() {
		return lock;
	}

	public void setLock(int lock) {
		this.lock = lock;
	}

	public double getPopularValue() {
		return popularValue;
	}

	public void setPopularValue(double popularValue) {
		this.popularValue = popularValue;
	}

	public int getuId() {
		return uId;
	}

	public void setuId(int uId) {
		this.uId = uId;
	}

	public int getAgreeCount() {
		return agreeCount;
	}

	public void setAgreeCount(int agreeCount) {
		this.agreeCount = agreeCount;
	}

	public int getViewCount() {
		return viewCount;
	}

	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}

	public int getAnswerCount() {
		return answerCount;
	}

	public void setAnswerCount(int answerCount) {
		this.answerCount = answerCount;
	}

	
}