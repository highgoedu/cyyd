package com.highgo.cyyd.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="t_myattention",uniqueConstraints = {@UniqueConstraint(columnNames="attentiveWord")})
public class MyAttention {
	
	@Id
	@GeneratedValue
	private long id;
	
	
	private String attentiveWord;
	
	private long userId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAttentiveWord() {
		return attentiveWord;
	}

	public void setAttentiveWord(String attentiveWord) {
		this.attentiveWord = attentiveWord;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	
}
