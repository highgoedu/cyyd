package com.highgo.cyyd.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="sde_solution")
public class SdeSolution implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="id")
	public long id;
	
	@Column(name="name")
	public String name;//创意标题
	
	@Column(name="question")
	public String question;//创意描述
	
	@Column(name="question_time")
	public long questionTime;//创建时间
	
	@Column(name="uid")
	public long uid;//知乎id
	
	@Column(name="cyyd_id")
	public long cyydId;//创意id
	
	@Column(name="source")
	public int source;//1创新方法推荐系统 2创意引导系统

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public long getQuestionTime() {
		return questionTime;
	}

	public void setQuestionTime(long questionTime) {
		this.questionTime = questionTime;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public long getCyydId() {
		return cyydId;
	}

	public void setCyydId(long cyydId) {
		this.cyydId = cyydId;
	}

	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}

}
