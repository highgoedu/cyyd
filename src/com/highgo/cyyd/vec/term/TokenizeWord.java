package com.highgo.cyyd.vec.term;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import com.highgo.cyyd.vec.Learn;
import com.highgo.cyyd.vec.Word2VEC;

public class TokenizeWord {
	public ArrayList<String> existWordList=new ArrayList<String>();
	HanlpFenci hp = new HanlpFenci();
	// public Word2VEC word2(String path) throws IOException {
	// Word2VEC w1 = new Word2VEC();
	// w1.loadGoogleModel(path);
	// return w1;
	// }
public static void main(String[] args) throws IOException {
	
	Word2VEC w = new Word2VEC();
	TokenizeWord tw = new TokenizeWord();
	long t1 = new Date().getTime();
	w.loadGoogleModel("C:/Users/sde/Desktop/vectors2.bin");
	long t2 = new Date().getTime();
	System.out.println("加载："+(t2-t1));
	
	tw.add("鞋子");
	Node node2 = new Node(new Term("鞋子", 0.1));
	long t3 = new Date().getTime();
	Node node = tw.tokenizedToTree(w, node2, 2,5);
	long t4 = new Date().getTime();
	System.out.println("创建："+(t4-t3));
//	
//	new D3Tree(node, "");
//	new D3Pack(node,"");
	D3Force force = new D3Force(node,"");
	System.out.println(force.getNode());
	System.out.println(force.getEdge());
	System.out.println(force.getLinkLength());
	long t5 = new Date().getTime();
	System.out.println("遍历："+(t5-t4));
	
	for (String string :tw.existWordList) {
		System.out.println(string);
	}
	
}
	public  List<Term> tokenize(String s) {
		String string = s.replaceAll("\\[|\\]", "");
		String[] array = string.split(",");
		List<Term> listNode = new ArrayList<Term>();
		if (array.length > 0) {
			for (String term : array) {
				String[] t = term.split("\t");
				listNode.add(new Term(t[0], Double.parseDouble(t[1])));
			}
		}
		return listNode;
	}
	
	public void add(String e){
		existWordList.add(e);
	}
	public Node[] tokenizeToTree(String s,int layerCount) {
		String string = s.replaceAll("\\[|\\]", "");
		String[] array = string.split(",");
		Node[] listNode = new Node[layerCount];
		if (array.length > 1) {
			int count=0;//listNode 数组的索引
			for (int i = 0; i < array.length; i++) {
				String[] t = array[i].split("\t");	
				if (count>listNode.length-1) {
					break;//数组满的时候，跳出循环。
				}
				if (existWordList.indexOf(t[0].trim())==-1) {//之前并没有保存这个词的时候，添加进去，去重。
					System.out.println(t[0]+" "+t[1]);
					listNode[count] = (new Node(new Term(t[0].trim(),Double.parseDouble(t[1]))));
					add(t[0].trim());
					count++;                         //添加一个，数组位置先前进1
				}
			}
		}
		return listNode;
	}
	public ArrayList<String> addParent(Node node){
		System.out.println(node.source.name);
		ArrayList<String> list = node.source.name;
		while (node.parent!=null) {
			for (int i = 0; i < node.parent.source.name.size(); i++) {
				list.add(node.parent.source.name.get(i));
			}
			node = node.parent;
			System.out.println("执行while循环");
		}
		System.out.println(node.source.name);

		return list;
	}
	
	/**
	 * 专门处理首次搜索的词或者句子，用来分词。然后进入递归。
	 * @param w
	 * @param node
	 * @param layerNum
	 * @param layerCount
	 * @return
	 */
	public  Node tokenizedToTree(Word2VEC w, Node node, int layerNum,int layerCount) {
		ArrayList<String> list =hp.Tokenize(node.source.name.get(0));
		//把分词结果赋值给当前节点名称 新加一个方法。
		System.out.println(list.toString());
		node.source.name = new ArrayList<String>();
		//将搜索的词添加在“已经存在的列表”中，表示后续节点将不会再出现这个词。
		for (int i = 0; i < list.size(); i++) {
			add(list.get(i));
		}
		node.source.name=list;
		return tokenizeToTree(w, node, layerNum, layerCount);
	}
	
	
	/**
	 * 
	 * @param w
	 * @param node
	 * @param layerNum
	 * @param layerCount
	 * @return
	 */
public  Node tokenizeToTree(Word2VEC w, Node node, int layerNum,int layerCount) {
		layerNum--;
		System.out.println("当前节点的内容1："+node.source.name.toString());
		Object ob[] = node.source.name.toArray();
		ArrayList<String> list = new ArrayList<String>();
		for (int i = 0; i < ob.length; i++) {
			list.add((String)ob[i]);
		}
		Node node2 = new Node();
		node2.parent=node.parent;
		
		Term t = new Term();
		t.name=list;
		node2.source=t;
		ArrayList<String> list2 = addParent(node2);   //将此节点与上一层关联起来。
		String s = w.distance(list2).toString();
		System.out.println(s);
		Node[] listNode = tokenizeToTree(s,layerCount); // 将与搜索词相关的词语保存起来
		
		node.target = listNode;// 建立本层关系，并开始向下递归。
		System.out.println("当前节点的内容2："+node.source.name.toString());
		if (layerNum > 0) {
			for (int i = 0; i < node.target.length; i++) {// 遍历target中的每一个term,建立下一层关系
				if (node.target[i]!=null) {
				node.target[i].parent=node;    	
				node.target[i] = tokenizeToTree(w, node.target[i], layerNum,layerCount);// 递归建立每一层关系
			}
			}
		}
		return node;
	}
	
	
	public static String NodetoString(Node node) {
		String returnString = "";
		Node n = node;

		returnString += n.source.name + "\t" + n.source.number + ",";
		if (n.target != null) {
			returnString += "{";
			for (Node node1 : n.target) {
				// returnString+=node1.source.name+"\t"+node1.source.number+",";
				returnString += NodetoString(node1);
			}
			returnString += "}";
		}

		return returnString;
	}

	// public static Node tokenized(Word2VEC w, Node node,int num) {
	// num--;
	// String s = w.distance(node.source.name).toString();
	// List<Node> listNode = tokenize(s); // 将与搜索词相关的词语保存起来
	//
	// node.target=listNode;// 建立第一层关系
	//
	// if (num>0) {
	// for (Node n1 : node.target) {//遍历target中的每一个term,建立下一层关系
	// node.target.add(tokenized(w, n1,num)); //递归建立每一层关系
	// }
	// }
	// return node;
	// }

	public static void learn(String txtPath, String libraryPath)
			throws IOException {
		Learn learn = new Learn();
		learn.learnFile(new File(txtPath));
		learn.saveModel(new File(libraryPath));
	}

}