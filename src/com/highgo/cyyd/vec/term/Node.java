package com.highgo.cyyd.vec.term;


public class Node {
	public Node parent=null;
	public Term source=null;
	public Node[] target =null;
	
	public Node(Term source,Node[] target) {
		this.source=source;
		this.target=target;
	}
	public Node(Term source){
		this.source=source;
	}
	public Node(){
	}
}
