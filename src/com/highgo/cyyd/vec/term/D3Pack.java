package com.highgo.cyyd.vec.term;

import java.io.IOException;

public class D3Pack {
	private String edge;
	private String node;
	private String circleR;

	public D3Pack(Node nodes, String path) throws IOException {

		TextOperation tp = new TextOperation();
		String s = NodetoD3Pack(nodes);
		System.out.println(s);
		 tp.createText(path, s);

	}

	public String NodetoD3Pack(Node nodes) {
		String returnString = "";
		Node n = nodes;
		returnString += "{";
		returnString += "\"name\"" + ":\"" + n.source.name + "\"";
		returnString += ",\n"; // 如果有子节点，加逗号并且换行添加子节点。

		returnString += "\"children\":\n"; // 添加children字符串并换行

		returnString += "[\n";

		for (int i = 0; i < n.target.length; i++) {
			if (n.target[i] != null) { // 保存数据的数组可能未满，跳过那些空数据
				returnString += "{";
				returnString += "\"name\"" + ":\"" + n.target[i].source.name+ "\",\n";
				returnString += "\"children\":\n"; // 添加children字符串并换行
				returnString += "[\n";
				returnString += "{\"name\"" + ":\"" + n.target[i].source.name+ "\"},\n";
				for (int j = 0; j < n.target[i].target.length-1; j++) {
					if (n.target[i].target[j] != null) { // 保存数据的数组可能未满，跳过那些空数据
					 
					 returnString += "{\"name\"" + ":\""+ n.target[i].target[j].source.name + "\"}";
					
						if (j != n.target[i].target.length - 2) {// 如果不是最后一个节点，那就添加一个逗号
							returnString += ",\n";
						} else {
							returnString += "\n";
						}
					}
				}
				returnString += "]\n";
				
				if (i!=n.target.length-1) {//如果不是最后一个节点，那就添加一个逗号
					returnString += "},\n";	
				}
				else {
					returnString+="}\n";
				}
			}
		}
		returnString += "]\n";
		returnString += "}";
		return returnString;
	}

	public String addSpace(int num) {
		String returnString = "";
		for (int i = 0; i < num; i++) {
			returnString += "\t";
		}
		return returnString;
	}

	public String getEdge() {
		return edge;
	}

	public String getNode() {
		return node;
	}

	public String getCircleR() {
		return circleR;
	}
}
