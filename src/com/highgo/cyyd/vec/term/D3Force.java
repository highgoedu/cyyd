package com.highgo.cyyd.vec.term;

import java.io.IOException;
import java.util.ArrayList;

public class D3Force {
	private String edge="\"edges\":[\n";
	private String node="\"nodes\":[\n";
	private String circleR="\"circleR\":[\n";
	private String linkLength="\"LinkLength\":[\n";
	private int index = 0; //用来标记节点的编号，循环中每执行一次，就自增一次。
	
	
	public D3Force(Node nodes,String path) throws IOException{
		
		TextOperation tp = new TextOperation();
		String s = NodetoD3Force(nodes,0,5);
		System.out.println(s);
		tp.createText(path, s);
	}
	
//	public String NodetoD3Force(Node nodes,int num,int depth){
//		Node n = nodes;
//		node+=createNode(n.source.name);//添加一个node
//		circleR+=n.source.number*depth+",";
//		if (n.target != null) {
//			
//			for (int i = 0; i < n.target.length; i++) {
//				if (n.target[i]!=null) {   //保存数据的数组可能未满，跳过那些空数据
//					edge+=createEdge(num, num*n.target.length+i+1);
//					linkLength+=depth*30+",";
//					NodetoD3Force(n.target[i],num+i+1,depth-1);
//				}
//			}
//		}
//		
//		
//		return "{\n"+getEdge()+"\n"+getNode()+"\n"+getCircleR()+"\n"+getLinkLength()+"\n}";
//	}
	/**
	 * 用来形成force图所需要json文件的方法
	 * @param nodes 主节点
	 * @param num 无用数据。无任何实际意义，只是与之前接口保持一致。
	 * @param depth 用来调整节点圆圈半径的数据。
	 * @return
	 */
	public String NodetoD3Force(Node nodes,int num,int depth){
		Node n = nodes;
		node+=createNode(n.source.name);//添加一个node
		circleR+=n.source.number*depth+",";
		int current = index;  //curent是力导向图中每两层结果中的父节点，它在每一次循环中保持不变。
		if (n.target != null) {
			
			for (int i = 0; i < n.target.length; i++) {
				if (n.target[i]!=null) {   //保存数据的数组可能未满，跳过那些空数据
					edge+=createEdge(current, ++index);
					linkLength+=depth*30+",";
					NodetoD3Force(n.target[i],num,depth-1);
				}
			}
		}
		
		
		return "{\n"+getEdge()+"\n"+getNode()+"\n"+getCircleR()+"\n"+getLinkLength()+"\n}";
	}

	public String createEdge(int source,int target){
	return "{ \"source\" : "+source+"  , \"target\": "+target+" },"; 
	}
	
	public String createNode(ArrayList<String> name){
		return "{ \"name\" : \""+name.toString().replaceAll("\\[|\\]", "")+"\" },"; 
		}
	
	public String addSpace(int num){
		String returnString="";
		for (int i = 0; i < num; i++) {
			returnString+="\t";
		}
		return returnString;
	}
	public String getEdge(){
		return edge.substring(0, edge.length()-1)+"],";
	}
	public String getNode(){
		return node.substring(0, node.length()-1)+"],";
	}
	public String getCircleR(){
		return circleR.substring(0, circleR.length()-1)+"],";
	}
	public String getLinkLength(){
		return linkLength.substring(0, linkLength.length()-1)+"]";
	}
}
