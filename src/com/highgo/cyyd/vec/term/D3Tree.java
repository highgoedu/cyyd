package com.highgo.cyyd.vec.term;

import java.io.IOException;

import com.highgo.cyyd.vec.term.Node;
import com.highgo.cyyd.vec.term.TextOperation;

public class D3Tree {
	private String edge;
	private String node;
	private String circleR;
	
	public D3Tree(Node nodes,String path) throws IOException{
		
		
		TextOperation tp = new TextOperation();
		String s = NodetoD3Tree(nodes);
		System.out.println(s);
		tp.createText(path, s);
		
	}
	public String NodetoD3Tree(Node nodes){
		String returnString = "";
		Node n = nodes;
		returnString += "{";
		System.out.println(n.source.name.toString().replaceAll("\\[|\\]", ""));
		returnString += "\"name\""+":\""+n.source.name.toString().replaceAll("\\[|\\]", "")+"\"";
		if (n.target != null) {
			returnString += ",\n";		//如果有子节点，加逗号并且换行添加子节点。
			
			returnString += "\"children\":\n";   //添加children字符串并换行
			//returnString += "_children:\n"; //用这个变量开控制
			returnString += "[\n";
			for (int i = 0; i < n.target.length; i++) {
				if (n.target[i]!=null) {   //保存数据的数据可能未满，跳过那些空数据
					returnString += NodetoD3Tree(n.target[i]);
					if (i!=n.target.length-1) {//如果不是最后一个节点，那就添加一个逗号
						returnString += ",\n";	
					}
					else {
						returnString+="\n";
					}
				}
				
				
			}
			returnString += "]\n";
		}
		returnString += "}";
		return returnString;
	}
	public String addSpace(int num){
		String returnString="";
		for (int i = 0; i < num; i++) {
			returnString+="\t";
		}
		return returnString;
	}
	public String getEdge(){
		return edge;
	}
	public String getNode(){
		return node;
	}
	public String getCircleR(){
		return circleR;
	}
}
