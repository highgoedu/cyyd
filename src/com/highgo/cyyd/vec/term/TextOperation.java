package com.highgo.cyyd.vec.term;

import java.io.BufferedReader;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;  
import java.util.Collections;  
import java.util.Comparator;
import java.util.HashMap;  
import java.util.LinkedHashMap;
import java.util.List;  
import java.util.Map;  
import java.util.Map.Entry;


public class TextOperation {
		
	public static void main(String[] args) throws IOException {
//termCounts("F:/轮胎/info4.txt");
	}
	public  void addString(String writePath,String codes){ 
		File dest = new File(writePath);  
		try {  
		    BufferedWriter writer  = new BufferedWriter(new FileWriter(dest,true));  

		    writer.write(codes);  
		    writer.flush(); 
		    writer.close();  
		} catch (FileNotFoundException e) {  
		    e.printStackTrace();  
		} catch (IOException e) {  
		    e.printStackTrace();  
		}
		}
	public static String[] stringSort(String [] s) {
		List<String> list = new ArrayList<String>(s.length);
		for (int i = 0; i < s.length; i++) {
		list.add(s[i]);
		}
		Collections.sort(list);
		return list.toArray(s);
		}
public static void addText(String readPath,String writePath){
	File file = new File(readPath);  
	File dest = new File(writePath);  
	try {  
	    BufferedReader reader = new BufferedReader(new FileReader(file));  
	    BufferedWriter writer  = new BufferedWriter(new FileWriter(dest,true));  
	    String line = reader.readLine();  
	    line = "\r\n"+line;
	    while(line!=null){
	    	System.out.println(line);
	        writer.write(line);  
	        line = reader.readLine();  
	    }  
	    writer.flush();  
	    reader.close();  
	    writer.close();  
	} catch (FileNotFoundException e) {  
	    e.printStackTrace();  
	} catch (IOException e) {  
	    e.printStackTrace();  
	}
	}
public  String readText(String readPath){
	String codes = "";
	File file = new File(readPath);   
	try {  
	    BufferedReader reader = new BufferedReader(new FileReader(file));  
	    String line = reader.readLine();  
	    while(line!=null){
	    	line +="\n";
	    	codes+=line;
	        line = reader.readLine();  
	    }  
	    reader.close();
	
	} catch (FileNotFoundException e) {  
	    e.printStackTrace();  
	} catch (IOException e) {  
	    e.printStackTrace();  
	}
	return codes;
	}

public void createText(String path,String codes) throws IOException{
	 PrintWriter pw = new PrintWriter(new FileWriter(path));
     pw.print(codes);
     pw.flush();
     pw.close();
}
public String createTextAndWriteInfo(String path,String content) throws IOException{
	      File file = new File(path);
	      BufferedWriter bw = new BufferedWriter(new FileWriter(file));
	      bw.write(content);
	      bw.close();
	      return file.getAbsolutePath();
	  }
public  boolean createFile(File fileName)throws Exception{  
	  boolean flag=false;  
	  try{  
	   if(!fileName.exists()){  
	    fileName.createNewFile();  
	    flag=true;  
	   }  
	  }catch(Exception e){  
	   e.printStackTrace();  
	  }  
	  return true;  
	 }   
public  String readPatentText(String readPath){
	String codes = "";
	File file = new File(readPath);   
	try {  
	    BufferedReader reader = new BufferedReader(new FileReader(file));  
	    String line = reader.readLine();
	    int lineNum = 1;
	    while(line!=null){
	    	if(lineNum<5){
	    	line +="。";
	    	codes+=line;
	    	}
	    	else {
	    		if (line.length()>4) {
	    			String linesString = line.substring(0, 3);
		    		if (linesString.equals("主权项")) {
		    			codes+=line.substring(4, line.length()-1);
					}	
				}	    		
			}
	    	lineNum++;
	        line = reader.readLine();  
	    }  
	    reader.close();
	
	} catch (FileNotFoundException e) {  
	    e.printStackTrace();  
	} catch (IOException e) {  
	    e.printStackTrace();  
	}
	return codes;
	}

 public  void termCounts(String readPath,String storedPath) throws IOException{
	 String codes = "";
	 HashMap<String, List> infoMap =new HashMap();
	 int documentsCounts=0;
	 int tokensCounts =0;
		File file = new File(readPath);   
		try {  
		    BufferedReader reader = new BufferedReader(new FileReader(file));  
		    String line = reader.readLine();  
		    while(line!=null){
		    	documentsCounts++;//求得文档数目
		    	//System.out.println(line);
		    	String str[] = line.split("\t");
		    	String[] str1={};
		    	if (str.length>1) {
					str1=str[1].split(" ");
				}
		    	List<String> list = new ArrayList<>();
		    	for (String string : str1) {
					list.add(string);
				}
		    	infoMap.put(str[0], list);
		    	line = reader.readLine();  
		    }  
		    reader.close();
		} catch (FileNotFoundException e) {  
		    e.printStackTrace();  
		} catch (IOException e) {  
		    e.printStackTrace();  
		}
	
		List<String> list =  new ArrayList();
	
		//把所有的词添加到list中
		 for(String obj : infoMap.keySet()){
	        	List listTemp = infoMap.get(obj);
	        	for (int i = 0; i < listTemp.size(); i++) {
					list.add((String) listTemp.get(i));
				}
	        	}
		 tokensCounts=list.size();
			 	Map tokensCountsMap = new HashMap();  
			 	
			//统计list中每个token的数量
	        for (String temp : list) {  
	            Integer count = (Integer) tokensCountsMap.get(temp);  
	            tokensCountsMap.put(temp, (count == null) ? 1 : count + 1);  
	        } 
	        Map sortedTokensCountsMap = sortMap(tokensCountsMap);
//	        int o = 0;
//	        for(Object obj : sortedTokensCountsMap.keySet()){
//	        	if (o<100) {
//	        		//System.out.println(obj.toString());
//	        		System.out.println((String)obj+":"+sortedTokensCountsMap.get(obj ));
//	        		o++;
//				}
//	        	else
//	        		break;
//	        	}
	        
	        Map tokensDocumentCountsMap= new HashMap<>();
	        //HashMap<String,Map> tokensEachDocumentCountsMap = new HashMap();
	        
	        int i =0;
	        for(Object obj : sortedTokensCountsMap.keySet()){
	        	//Map eachDocumentMap = new HashMap<>();//路径映射到次数
	        	int counts=0;
	        	if (i<150) {//只要前200个
	        		for(Object obj1 : infoMap.keySet()){//遍历所有路径

	        			List eachDocumentTokensList = infoMap.get(obj1);
	        			
	        			if (eachDocumentTokensList.indexOf(obj)>1) {
							counts++;
							//eachDocumentMap.put(obj1, Collections.frequency(list,obj));
						}
	    	        	}
	        		tokensDocumentCountsMap.put(obj, counts);
	        		//tokensEachDocumentCountsMap.put((String)obj, eachDocumentMap);
		        	//System.out.println(obj+":"+sortedTokensCountsMap.get(obj ));	
				}
	        	else
	        	{
	        		break;
	        	}
	        i++;	
	        }
	        System.out.println(tokensCounts);
	        System.out.println(documentsCounts);
	        int j =0;
	        Map tokensTfIdfMap= new HashMap<>();
	        for(Object obj : sortedTokensCountsMap.keySet()){
	        	if (j<150) {
	        		int eachTokensCounts = (Integer)sortedTokensCountsMap.get(obj );
	        		int eachTokensDocumentsCounts = (Integer)tokensDocumentCountsMap.get(obj);
	        		tokensTfIdfMap.put(obj, tfIdf(eachTokensCounts, tokensCounts, eachTokensDocumentsCounts, documentsCounts));
				}
	        	else
	        	{
	        		break;
	        	}
	        	j++;
		        }
	        String codeString ="词\t次数\t文档数目\t权重\n";
	        Map tokensTfIdfSortedMap =sortMap(tokensTfIdfMap);
	        for(Object obj : tokensTfIdfSortedMap.keySet()){
	        		int eachTokensCounts = (Integer)sortedTokensCountsMap.get(obj );
	        		int eachTokensDocumentsCounts = (Integer)tokensDocumentCountsMap.get(obj);
	        		int tfIdf = (Integer)tokensTfIdfSortedMap.get(obj);
	        		codeString+=obj+"\t"+eachTokensCounts+"\t"+eachTokensDocumentsCounts+"\t"+tfIdf+"\n";
	        		System.out.println(codeString);
		        }
	        createTextAndWriteInfo(storedPath,codeString);
	        
 }
 public static int tfIdf(int a,int b,int c,int d){
	int i = 0;
	 if (c!=0) {
		 Float f = new Float(a/(float)b*(d/c)*1000);
		 i = f.intValue();
	}
	 return i;
 }
 public static Map sortMap(Map oldMap) {  
     ArrayList<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(oldMap.entrySet());  
     Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {  
    	 
         @Override  
         public int compare(Entry<java.lang.String, Integer> arg0,  
                 Entry<java.lang.String, Integer> arg1) {  
             return arg1.getValue() - arg0.getValue();  
         }  
     });  
     Map newMap = new LinkedHashMap();  
     for (int i = 0; i < list.size(); i++) {  
         newMap.put(list.get(i).getKey(), list.get(i).getValue());  
     }  
     return newMap;  
 }  
 public List removeDuplicate(List<Object> list){
	 List<Object> list1 = (List) new ArrayList<String>();
	 
	 for(Object tmp:list)  
     {  
         int index = list1.indexOf(tmp) ;
         if (index>0) {
			list1.add(tmp);
		}
     }  
	
	 return list1;
 }
}
