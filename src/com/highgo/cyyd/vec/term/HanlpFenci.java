package com.highgo.cyyd.vec.term;

import java.util.ArrayList;
import java.util.List;
import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.dictionary.stopword.CoreStopWordDictionary;
import com.hankcs.hanlp.seg.common.Term;
import com.hankcs.hanlp.tokenizer.StandardTokenizer;

public class HanlpFenci {
	public  ArrayList<String> Tokenize(String codes){
		 HanLP.Config.ShowTermNature = false;    // 关闭词性显示
		 CoreStopWordDictionary.add("本发明");
		List<Term> termList = StandardTokenizer.segment(codes);
		CoreStopWordDictionary.apply(termList);
		//System.out.println(termList.toString());
		String string  = termList.toString().replaceAll("\\[|\\]", "");
		String arr[] = string.split(", ");
		ArrayList<String> list = new ArrayList<String>();
		System.out.println("分词结果：");
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
			list.add(arr[i]);
		}
		System.out.println("分词结束...");
		return list;
	}
//	public static void main(String[] args) {
//		String arr[] = Tokenize("我是一个聪明的机器人");
//		for (int i = 0; i < arr.length; i++) {
//			System.out.println(arr[i]);
//		}
	
}
