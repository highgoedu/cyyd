<%@ page language="java" contentType="text/html; charset=utf-8"
	import="java.util.*,com.highgo.cyyd.vec.term.*,com.highgo.cyyd.vec.*,java.io.*"
    pageEncoding="utf-8"%>
    <%
		String path = request.getContextPath();
	%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<%=path %>/jquery-easyui-1.5/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="<%=path %>/jquery-easyui-1.5/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="<%=path %>/css/demo.css">
    <script type="text/javascript" src="<%=path %>/jquery-easyui-1.5/jquery.min.js"></script>
    <script type="text/javascript" src="<%=path %>/jquery-easyui-1.5/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="<%=path %>/script/searchres.js"></script>
<title>搜索结果</title>
<style>
.node circle {
	fill: #fff;
	stroke: steelblue;
	stroke-width: 1.5px;
}

.node {
	font: 12px sans-serif;
}

.link {
	fill: none;
	stroke: #ccc;
	stroke-width: 1.5px;
}
</style>
</head>
<body>
	<div style="width:100%;text-align:center;margin:auto">
		<div id="p" class="easyui-panel" title="搜索结果" style="width:75%;height:800px;padding:10px;margin:auto">
			<div style="margin:auto">
				<!-- <input id = "all" type="checkbox">
				<label for="all">全部来源</label>
				<input id = "productdata" type="checkbox" value="产品趋势">
				<label for = "productdata">产品数据</label>
				<input id = "patentdata" type="checkbox" value = "相关专利">
				<label for = "patentdata">专利数据</label>
				<input id = "techblog" type="checkbox" value="技术趋势">
				<label for = "techblog">科技博客</label>
				<input id = "professional" type="checkbox" value="专业评价">
				<label for = "professional">专业人士</label>
				<input id = "user" type="checkbox" value="用户评价">
				<label for = "user">用户评价</label> -->
				<form id = "searchForm" method="post">
				<table>
					<tr>
						<td>关键字</td>
						<td><input id = "keyinput" name = "keyword" class="easyui-textbox" value="${keyword}"/></td>
						<td>层数</td>
						<td>
							<%String layerNum = (String)request.getAttribute("layerNum"); %>
							<select class="easyui-combobox" id = "layernum" name="layerNum" style="width:100%;">
								<option value="3" <%=layerNum.equals("3") ? "selected" : ""%>>3</option>
								<option value="4" <%=layerNum.equals("4") ? "selected" : ""%>>4</option>
							</select>
						</td>
						<td>每层个数</td>
						<td>
							<%
								String layerCount = (String)request.getAttribute("layerCount"); 
							%>
							<select class="easyui-combobox" id = "layercount" name="layerCount" style="width:100%;">
								<option value="5" <%=layerCount.equals("5") ? "selected" : ""%>>5</option>
								<option value="6" <%=layerCount.equals("6") ? "selected" : ""%>>6</option>
								<option value="7" <%=layerCount.equals("7") ? "selected" : ""%>>7</option>
								<option value="8" <%=layerCount.equals("8") ? "selected" : ""%>>8</option>
								<option value="9" <%=layerCount.equals("9") ? "selected" : ""%>>9</option>
								<option value="10" <%=layerCount.equals("10") ? "selected" : ""%>>10</option>
							</select>
						</td>
						<td>数据来源</td>
						<td>
							<%
								String sourceType = (String)request.getAttribute("sourcetype"); 
								if(sourceType == null || "".equals(sourceType)) {
									sourceType = "productData";
								}
							%>
							<select class="easyui-combobox" name="sourcetype" style="width:100%;">
<%-- 								<option value="productData" <%=sourceType.equals("productData") ? "selected" : ""%>>产品数据</option> --%>
								<option value="patentdata" <%=sourceType.equals("patentdata") ? "selected" : ""%>>专利数据</option>
<%-- 								<option value="techBlog" <%=sourceType.equals("techBlog") ? "selected" : ""%>>科技博客</option> --%>
<%-- 								<option value="professionalData" <%=sourceType.equals("professionalData") ? "selected" : ""%>>专业人士</option> --%>
<%-- 								<option value="userData" <%=sourceType.equals("userData") ? "selected" : ""%>>用户评价</option> --%>
							</select>
						</td>
						<td>
							<a href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="width:80px" onclick="searchKey()">搜索</a>
						</td>
					</tr>
				</table>
				</form>
			</div>
			<div style="width:100%">
				<table>
					<tr>
						<td valign="top" width="60%">
							<div id = "contentid">
								<%
								//String readJsonPath = (String)request.getAttribute("jsonpath"); 
								String rootPath = request.getContextPath();
								
								String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
								//readJsonPath = basePath + readJsonPath;
								//System.out.println("=============" + readJsonPath);
								%>
								<script type="text/javascript" src="http://d3js.org/d3.v3.js"></script>
								<script>
									
									
								</script>
							</div>
							<div>
								<a href="#" class="easyui-linkbutton" style="width:100px" onclick="overHead('<%=basePath%>','<%=rootPath%>')">上钻</a>
								<a href="#" class="easyui-linkbutton" style="width:100px" onclick="goDown('<%=basePath%>','<%=rootPath%>')">下钻</a>
								<a href="#" class="easyui-linkbutton" style="width:100px">TOP10</a>
							</div>
						</td>
					</tr>
				</table>
				
			</div>
			
			<div>
				关键词
				<input id = "keys" class="easyui-textbox" style="width:500px"/>
				<a href="#" class="easyui-linkbutton" style="width:100px">生成创意集合</a>
			</div>
		</div>
		
	</div>
</body>
</html>
<script>
	$(function(){
		var path = '<%=path%>';
		
		loadResult('<%=basePath%>',path);
	})
	function searchKey() {
		var path = '<%=path%>';
		
		loadResult('<%=basePath%>',path);
	}
</script>