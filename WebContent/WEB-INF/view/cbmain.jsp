<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%
    	String path = request.getContextPath();
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<%=path %>/jquery-easyui-1.5/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="<%=path %>/jquery-easyui-1.5/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="<%=path %>/css/demo.css">
    <script type="text/javascript" src="<%=path %>/jquery-easyui-1.5/jquery.min.js"></script>
    <script type="text/javascript" src="<%=path %>/jquery-easyui-1.5/jquery.easyui.min.js"></script>
<title></title>
</head>
<body>
	<div>
		<!-- <div>
			我的所有创意评价
		</div> -->
		<div style="display:inline;float:left">
			<table class="easyui-datagrid" title="创意评价" style="width:300px;height:500px"
            data-options="singleSelect:true,url:'creativeevaluate.json',method:'get'">
		        <thead>
		            <tr>
		                <th data-options="field:'name',width:80">名字</th>
		                <th data-options="field:'descrytion',width:100">描述</th>
		                <th data-options="field:'date',width:100">日期</th>
		            </tr>
		        </thead>
		    </table>
		</div>
		<div style="display:inline;float:left;margin:100px">
			<a href="#" class="easyui-linkbutton" data-options="iconCls:''" style="">获取评价结果</a>
		</div>
		<div>
			<table class="easyui-datagrid" title="评价结果" style="width:300px;height:500px"
            data-options="singleSelect:true,url:'creativeevaluate.json',method:'get'">
		        <thead>
		            <tr>
		                <th data-options="field:'name',width:80">创意汇聚</th>
		                <th data-options="field:'descrytion',width:100">Y(同意)</th>
		                <th data-options="field:'date',width:100">N(反对)</th>
		                <th data-options="field:'date',width:100">选择</th>
		            </tr>
		        </thead>
		    </table>
		</div>
		
		
	</div>
</body>
</html>