<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js fixed-layout">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>山东大学 创意引导系统</title>
<meta name="description" content="这是一个 index 页面">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" type="image/png" href="/i/favicon.png">
<link rel="apple-touch-icon-precomposed" href="/i/app-icon72x72@2x.png">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" href="<%=path%>/AmazeUI/css/amazeui.min.css" />
<link rel="stylesheet" href="<%=path%>/AmazeUI/css/admin.css">
<link rel="stylesheet" href="<%=path%>/css/common.css">

<script>
	var path = '<%=path%>';
</script>
</head>
<body>

	<jsp:include page="/include/head.jsp" />

	<div class="am-cf admin-main">
		<jsp:include page="/include/tree.jsp" />

		<!-- content start -->
		<div class="admin-content">
			<div class="admin-content-body">
				<div class="am-cf am-padding am-padding-bottom-0">
					<div class="am-fl am-cf">
						<strong class="am-text-primary am-text-lg">创意引导</strong> / <small>创意发布</small>
					</div>
				</div>

				<hr />

				<div class="am-u-sm-12">
					<div style="width: 68%; float: left;">
						<section class="am-panel am-panel-default" id="step4-ques">
						<header class="am-panel-hd">
						<h3 class="am-panel-title">创意名称</h3>
						</header>
						<div class="am-panel-bd step4_question"
							style="max-height: 200px; overflow-y: scroll">
							<p>${entity.startCreativeTitle }</p>
						</div>
						<div style="clear: both"></div>
						</section>

						<section class="am-panel am-panel-default" id="step4-solution">
						<header class="am-panel-hd">
						<h3 class="am-panel-title">创意描述</h3>
						</header>
						<div class="am-panel-bd step4_solution"
							style="max-height: 200px; overflow-y: scroll">
							<p>${entity.startCreativeContent }</p>
						</div>
						<div style="clear: both"></div>
						</section>

						<section class="am-panel am-panel-default" id="step4-opt" style="border: 0px"> 
						<c:if test="${entity.status == 0}">
							<div style="width: 20%; float: left">
								<form class="am-form">
									<div class="am-form-group am-form-warning am-form-icon">
										<i class="am-icon-money"></i> 
										<input class="am-form-field" id="score" placeholder="悬赏分数" type="text">
									</div>
								</form>
							</div>
							<div style="width: 80%; float: left; text-align: right">
								<button style="width: 150px; float: right;" class="am-btn am-btn-danger am-btn-block" id="fb"
									onclick="recommend_publish(this)">发布</button>
							</div>
						</c:if> 
						<c:if test="${entity.status == 1 || entity.status == 2}">
							<div style="width: 20%; float: left">
								<form class="am-form">
									<div class="am-form-group am-form-icon">
										<i class="am-icon-money"></i> 
										<input class="am-form-field" placeholder="悬赏分数" type="text">
									</div>
								</form>
							</div>
							<div style="width: 80%; float: left; text-align: right">
								<a style="width: 150px; float: right;" class="am-btn am-btn-danger am-btn-block" disabled="disabled">
									发布 
								</a>
							</div>
						</c:if>
						<div style="width: 100%; float: left; margin-top: 20px; margin-bottom: 20px">
							<div class="am-panel am-panel-default">
								<div class="am-panel-hd">
									<c:if test="${comments != null && comments != '[]' }">
										<h4 class="am-panel-title am-collapsed" data-am-collapse="{parent: '#accordion', target: '#pingjia'}">查看评价</h4>									
									</c:if>
									<c:if test="${comments == null || comments == '[]'}">
										<h4 class="am-panel-title am-collapsed">查看评价</h4>									
									</c:if>									
								</div>

								<div id="pingjia" class="am-panel-collapse am-collapse" style="height: 0px;">
									<div class="am-panel-bd">
										<!--顶部-->
										<article class="am-comment" style="margin-bottom: 20px">
										<div class="comment-left">
											<div class="comment-row">
												<div class="dimension-name">创新性</div>
												<div class="dimension-value">
													<div class="am-progress">
														<div class="am-progress-bar am-progress-bar-danger" 
															style="width: ${scores_c*20 }%">${scores_c }分</div>
													</div>
												</div>
											</div>

											<div class="comment-row">
												<div class="dimension-name">实用性</div>
												<div class="dimension-value">
													<div class="am-progress">
														<div class="am-progress-bar am-progress-bar-danger" 
															style="width: ${scores_p*20 }%">${scores_p }分</div>
													</div>
												</div>
											</div>

											<div class="comment-row">
												<div class="dimension-name">可行性</div>
												<div class="dimension-value">
													<div class="am-progress">
														<div class="am-progress-bar am-progress-bar-danger" 
															style="width: ${scores_f*20 }%">${scores_f }分</div>
													</div>
												</div>
											</div>

											<div class="comment-row">
												<div class="dimension-name">经济性</div>
												<div class="dimension-value">
													<div class="am-progress">
														<div class="am-progress-bar am-progress-bar-danger" 
															style="width: ${scores_e*20 }%">${scores_e }分</div>
													</div>
												</div>
											</div>
										</div>
										<div class="comment-right">
											<c:forEach var="p" items="${keyword }">
												<span class="am-badge  am-text-xl comment-keywords">${p }</span>
											</c:forEach>
										</div>

										</article>
										<!-- 评论列表开始 -->
										<c:forEach var="p" items="${comments }">
											<article class="am-comment"><!-- 评论容器 --> 
												<c:if test="${p.avatarFile == ''}"><!-- 没有头像，默认头像 -->
													<a href="http://sf.bigdatainnovation.cn/?/people/${p.userName }" target="_blank">
														<img class="am-comment-avatar" alt=""
														src="http://sf.bigdatainnovation.cn/static/common/avatar-max-img.png">
														<!-- 评论者头像 -->
													</a>
												</c:if>
												<c:if test="${p.avatarFile != ''}">
												<a href="http://sf.bigdatainnovation.cn/?/people/${p.userName }" target="_blank">
													<img class="am-comment-avatar" alt=""
													src="http://sf.bigdatainnovation.cn/uploads/avatar/${p.avatarFile }">
													<!-- 评论者头像 -->
												</a>
												</c:if>
												
												<div class="am-comment-main">
													<!-- 评论内容容器 -->
													<header class="am-comment-hd"> <!--<h3 class="am-comment-title">评论标题</h3>-->
													<div class="am-comment-meta">
														<!-- 评论元数据 -->
														<a href="http://sf.bigdatainnovation.cn/?/people/${p.userName }" target="_blank"
															class="am-comment-author">${p.userName }</a>
														<!-- 评论者 -->
														评论于
														<time datetime="">${p.addTime }</time>
													</div>
													</header>
		
													<div class="am-comment-bd">${p.answerContent }</div>
													<!-- 评论内容 -->
												</div>
											</article>
											<br>
										</c:forEach>
									</div>
								</div>
							</div>
						</div>

						<div style="width: 20%; float: left">
							<c:if test="${entity.status == 0 || entity.status == 1 }">
								<a style="width: 150px; float: left;" class="am-btn am-btn-danger am-btn-block" id="top"
									onclick="$('#from1').submit()"> 
									<i class="am-icon-arrow-circle-left"></i> 上一步
								</a>
							</c:if>
							<c:if test="${entity.status == 2}">
								<a style="width: 150px; float: left;" class="am-btn am-btn-danger am-btn-block" disabled="disabled">
									<i class="am-icon-arrow-circle-left"></i> 上一步
								</a>
							</c:if>
						</div>

						<div style="width: 80%; float: left; text-align: right">
							<c:if test="${entity.status == 0 || entity.status == 1}">
								<a style="width: 150px; float: right;" class="am-btn am-btn-danger am-btn-block"
									onclick="recommend_lock(this)"> 归档 </a>
							</c:if>
							<c:if test="${entity.status == 2}">
								<a style="width: 150px; float: right;" class="am-btn am-btn-danger am-btn-block" disabled="disabled">
									归档 
								</a>
							</c:if>
						</div>

						<br>

						<div style="clear: both"></div>
						</section>
					</div>

					<div style="width: 30%; float: right;">
						<section class="am-panel am-panel-default"> <header
							class="am-panel-hd">
						<h3 class="am-panel-title">记事本</h3>
						</header>
						<div class="am-panel-bd" style="padding: 10px">
							<form class="am-form">
								<div class="am-form-group">
									<textarea class="" rows="10" id="noteContent">${entity.notepad }</textarea>
								</div>
							</form>
						</div>
						</section>
					</div>
				</div>
			</div>
		</div>
		<!-- content end -->
	</div>
<div id="snackbar-container">
	<div id="snackbar1478851090981" class="snackbar">
		<span class="snackbar-content">发布成功</span>
	</div>
</div>

<div class="am-modal am-modal-confirm" tabindex="-1" id="my-confirm1">
  <div class="am-modal-dialog">
    <div class="am-modal-hd">归档后终止评论</div>
    <div class="am-modal-bd">
		确定要归档创意吗？
    </div>
    <div class="am-modal-footer">
      <span class="am-modal-btn" data-am-modal-cancel>取消</span>
      <span class="am-modal-btn" data-am-modal-confirm>确定</span>
    </div>
  </div>
</div>
<div class="am-modal am-modal-confirm" tabindex="-1" id="my-confirm2">
  <div class="am-modal-dialog">
    <div class="am-modal-hd">操作提示</div>
    <div class="am-modal-bd">
		确定要发布创意吗？
    </div>
    <div class="am-modal-footer">
      <span class="am-modal-btn" data-am-modal-cancel>取消</span>
      <span class="am-modal-btn" data-am-modal-confirm>确定</span>
    </div>
  </div>
</div>
<div class="am-modal am-modal-alert" tabindex="-1" id="my-alert1">
  <div class="am-modal-dialog">
    <div class="am-modal-bd">
      	归档成功
    </div>
    <div class="am-modal-footer">
      <span class="am-modal-btn">确定</span>
    </div>
  </div>
</div>
<div class="am-modal am-modal-alert" tabindex="-1" id="my-alert2">
  <div class="am-modal-dialog">
    <div class="am-modal-bd">
      	发布成功
    </div>
    <div class="am-modal-footer">
      <span class="am-modal-btn">确定</span>
    </div>
  </div>
</div>

	<form id="from1" action="<%=path%>/creative/creativeEdit"
		method="post">
		<input type="text" id="id" name="id" value="${entity.id }" />
	</form>

	<script src="<%=path%>/AmazeUI/js/jquery.min.js"></script>
	<script src="<%=path%>/AmazeUI/js/amazeui.min.js"></script>
	<script src="<%=path%>/AmazeUI/js/app.js"></script>
	<script src="<%=path%>/script/creativePublic.js"></script>

<script>

</script>
</body>
</html>
