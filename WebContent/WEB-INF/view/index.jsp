<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String userId = (String)request.getSession().getAttribute("userId");
	Object uid = request.getSession().getAttribute("uid");
	System.out.println("Enter,index.jsp .... userId = " + userId + "uid = " + uid);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js fixed-layout">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>山东大学 创意引导系统</title>
<meta name="description" content="这是一个 index 页面">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" type="image/png" href="/i/favicon.png">
<link rel="apple-touch-icon-precomposed" href="/i/app-icon72x72@2x.png">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" href="<%=path%>/AmazeUI/css/amazeui.min.css" />
<link rel="stylesheet" href="<%=path%>/AmazeUI/tree/amazeui.tree.min.css" />
<link rel="stylesheet" href="<%=path%>/AmazeUI/css/admin.css">
<script>
	var path = '<%=path%>';
</script>
</head>
<body>

	<jsp:include page="/include/head.jsp" />

	<div class="am-cf admin-main">
		<jsp:include page="/include/tree.jsp" />

		<!-- content start -->
		<div class="admin-content">
			<div class="admin-content-body">
				<div class="am-cf am-padding am-padding-bottom-0">
					<div class="am-fl am-cf">
						<strong class="am-text-primary am-text-lg">创意引导</strong> / <small>搜索关键字</small>
					</div>
				</div>

				<hr />

				<div class="am-g" style="position:relative">
					<div class="am-panel am-panel-default" style="width: 80%; margin: 50px auto; border: 0;position:relative">
						
						<form class="am-form-inline" role="form" action="<%=path%>/search/gosearch" method="post">
							<div class="am-form-group" style="width: 50%; margin-left: 10%">
								<input type="text" id="keyword" name="keyword" class="am-form-field" style="width: 100%" 
									placeholder="请输入关键字" autocomplete="off" ><!-- onkeyup="readSearch(this.value)" -->
							</div>
							<div class="am-form-group">
								<select name="sourcetype" data-am-selected="{btnSize: 'default', btnStyle: 'default'}">
<!-- 									<option value="productData">产品数据</option> -->
									<option value="patentdata">专利数据</option>
<!-- 									<option value="techBlog">科技博客</option> -->
<!-- 									<option value="professionalData">专业人士</option> -->
<!-- 									<option value="userData">用户评价</option> -->
								</select>
							</div>
							<button type="submit" class="am-btn am-btn-default am-radius">搜索</button>
						</form>
			
						<div class="am-form-group .am-hide" id="search_position" style="width: 50%;margin-left: 10%;border: 1px solid #dedede;z-index:99;position: absolute;background:white">
							<table class="am-table" style="margin-bottom: 0" id="history">

							</table>
						</div>
						
						<div data-am-widget="tabs" class="am-tabs am-tabs-d2" style="margin-top:6%;z-index: 9;position: relative;">
							<ul class="am-tabs-nav am-cf">
								<li class="am-active"><a href="[data-tab-panel-0]">推荐</a></li>
								<li class=""><a href="[data-tab-panel-1]">分类</a></li>
								<li class=""><a href="[data-tab-panel-2]">我的关注</a></li>
							</ul>
							<div class="am-tabs-bd">
								<div data-tab-panel-0 class="am-tab-panel am-active">
									<div id="myRecommend">
										【青春】那时候有多好，任雨打湿裙角。忍不住哼起，心爱的旋律。绿油油的树叶，自由地在说笑。燕子忙归巢，风铃在舞蹈。经过青春的草地，彩虹忽然升起。即使视线渐渐模糊，它也在我心里。就像爱过的旋律，没人能抹去。因为生命存在失望，歌唱，所以才要歌唱。
									</div>
									<div class="am-panel-footer" style="border: 0;text-align: right;background: none;">
										<button class="am-btn am-btn-default" id="next">
											<i class="am-icon-spinner am-icon-spin"></i> 换一换
										</button>
									</div>
								</div>
								<div data-tab-panel-1 class="am-tab-panel ">
									<ul class="am-tree" id="firstTree">
									  <li class="am-tree-branch am-hide" data-template="treebranch">
									    <div class="am-tree-branch-header">
									      <button class="am-tree-branch-name">
									        <span class="am-tree-icon am-tree-icon-folder"></span>
									        <span class="am-tree-label"></span>
									      </button>
									    </div>
									    <ul class="am-tree-branch-children"></ul>
									    <div class="am-tree-loader"><span class="am-icon-spin am-icon-spinner"></span></div>
									  </li>
									  <li class="am-tree-item am-hide" data-template="treeitem">
									    <button class="am-tree-item-name">
									      <span class="am-tree-icon am-tree-icon-item"></span>
									      <span class="am-tree-label"></span>
									    </button>
									  </li>
									</ul>
								</div>
								<div data-tab-panel-2 class="am-tab-panel ">
									<ul class="am-avg-sm-4 am-thumbnails am-avg-sm-hover"
										style="text-align: center;">
										<c:forEach var="p" items="${historyList }">
											<li onclick="setValue($(this).text())">${p }</li>
										</c:forEach>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- content end -->
	</div>

	<script src="<%=path%>/AmazeUI/js/jquery.min.js"></script>
	<script src="<%=path%>/AmazeUI/js/amazeui.min.js"></script>
	<script src="<%=path%>/AmazeUI/js/app.js"></script>
	<script src="<%=path%>/AmazeUI/tree/amazeui.tree.min.js"></script>

<script>
	
// 	$('#keyword').focus(function (){
// 		readSearch('');
// 	});
// 	$('#keyword').blur(function (){
// 		$('#search_position').hide(400);
// 	});
	function setValue(value){
		$('#keyword').val(value);
	}
	$('#next').click(function(){
		$('#myRecommend').text('');
		$.ajax({ 
		    url: path + '/search/getRecommend',
		    dataType: 'json',
		    type: 'post', 
		    scriptCharset: 'utf-8',
		    success: function(data){
		    	$('#myRecommend').html(data.content);
		    }
		});
	})
	
	//搜索引擎
// 	function readSearch(value){
// 		$('#history').html('');
// 		$.ajax({ 
// 		    url: path + '/search/getSearchContent?value='+value,
// 		    dataType: 'json',
// 		    type: 'post', 
// 		    scriptCharset: 'utf-8',
// 		    success: function(data){
// 		    	if(data.list != null){
// 		    		var content = "<tbody>";
// 		    		for(var i=0;i<data.list.length;i++){
// 		    			content += "<tr onclick='setValue($(this).text())'><td>"+data.list[i]+"</td></tr>";
// 		    		}
// 		    		content += "</tbody>";
// 		    		$('#history').append(content);
// 		    		$('#search_position').show(400);
// 		    	}
// 		    }
// 		});
// 	}
	
	$('#firstTree').tree({
	    dataSource: function(options, callback) {
	    	$.ajax({ 
			    url: path + '/JSON/data.json',
			    dataType: 'json',
			    type: 'post', 
			    scriptCharset: 'utf-8',
			    success: function(data){
			    	setTimeout(function() {
			        callback({data: options.classes || data});
			      }, 400); 
			    }
			});
	      // 模拟异步加载
	      
	    },
	    multiSelect: false,
	    cacheItems: true,
	    folderSelect: false
	  });
	  $('#firstTree').on('selected.tree.amui', function (event, data) {
		  $("#keyword").val(data.selected[0].title);
		  //alert(data.selected[0].title);
	  });
</script>
</body>
</html>
