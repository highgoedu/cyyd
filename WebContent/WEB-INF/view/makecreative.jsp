<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
	String userId = (String)request.getSession().getAttribute("userId");
	Object uid = request.getSession().getAttribute("uid");
	System.out.println("Enter,makecreative.jsp .... userId = " + userId + ",uid = " + uid);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js fixed-layout">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>山东大学 创意引导系统</title>
<style>
.node circle {
	fill: #fff;
	stroke: steelblue;
	stroke-width: 1.5px;
}

.node {
	font: 12px sans-serif;
}

.link {
	fill: none;
	stroke: #ccc;
	stroke-width: 1.5px;
}
</style>
<meta name="description" content="这是一个 index 页面">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" type="image/png" href="/i/favicon.png">
<link rel="apple-touch-icon-precomposed" href="/i/app-icon72x72@2x.png">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" href="<%=path%>/AmazeUI/css/amazeui.min.css" />
<link rel="stylesheet" href="<%=path%>/AmazeUI/css/admin.css">

<script>
	var path = '<%=path%>';
</script>
</head>
<body>
	<jsp:include page="/include/head.jsp" />
	<div class="am-cf admin-main">
		<jsp:include page="/include/tree.jsp" />
		<div class="admin-content">
			<div class="admin-content-body">
				<div class="am-cf am-padding am-padding-bottom-0">
					<div class="am-fl am-cf">
						<strong class="am-text-primary am-text-lg">创意引导</strong> / <small>关键字描述</small>
					</div>
				</div>
				<hr />
				<div class="am-g" style="position: relative">
					<div class="" style="width: 80%; border: 0; position: relative">
						<form class="am-form-inline" role="form"
							action="<%=path%>/search/searchkey" method="post">
							<div class="am-form-group" style="width: 100%; margin-left: 10px">
								<div class="am-form-group">
									<small>关键字</small> <input id="keyinput" name="keyword"
										class="am-form-field" value="${keyword}" />
								</div>
								<div class="am-form-group">
									<small>数据来源</small>
									<%
										String sourceType = (String) request.getAttribute("sourcetype");
										if (sourceType == null || "".equals(sourceType)) {
											sourceType = "productData";
										}
									%>
									<select id="sourcetype" name="sourcetype"
										data-am-selected="{btnSize: 'default', btnStyle: 'default'}">
<!-- 										<option value="productData" -->
<%-- 											<%=sourceType.equals("productData") ? "selected" : ""%>>产品数据</option> --%>
										<option value="patentdata"
											<%=sourceType.equals("patentdata") ? "selected" : ""%>>专利数据</option>
<!-- 										<option value="techBlog" -->
<%-- 											<%=sourceType.equals("techBlog") ? "selected" : ""%>>科技博客</option> --%>
<!-- 										<option value="professionalData" -->
<%-- 											<%=sourceType.equals("professionalData") ? "selected" : ""%>>专业人士</option> --%>
<!-- 										<option value="userData" -->
<%-- 											<%=sourceType.equals("userData") ? "selected" : ""%>>用户评价</option> --%>
									</select>
								</div>
								<div class="am-form-group">
									<small>显示方式</small> <select id="showType" name="showType"
										data-am-selected="{btnWidth:'100px',btnSize: 'default', btnStyle: 'default'}">
										<option value="tree">树形</option>
										<option value="force">力导向图</option>
									</select>
								</div>
								<div class="am-form-group">
									<button type="button" class="am-btn am-btn-default am-radius"
										onclick="searchKey()">
										<i class="am-icon-search"></i> 搜索
									</button>
								</div>

							</div>
						</form>
					</div>
					<div class="am-form-group">
						<div class="am-panel-group am-u-sm-7">
							<div id="contentid">
								<%
									String rootPath = request.getContextPath();
									String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
											+ request.getContextPath() + "/";
								%>
								<script type="text/javascript" src="http://d3js.org/d3.v3.js"></script>
							</div>
						</div>

						<div class="am-u-md-4" style="width: 400px; heigth: 100%; margin-top: 20px">

							<div class="am-panel am-panel-default">
								<div class="am-panel-hd">
									<h4 class="am-panel-title">
										<label name="msg"></label>&nbsp;&nbsp;相关信息 Top60
<!-- 										<span style="float: right;" onclick="refresh()"><i class="am-icon-refresh"></i>换一换</span> -->
									</h4>
								</div>
								<div id="do-not-say-1" class="am-panel-collapse am-collapse am-in">
									<div id="collapse-panel-2" style="max-height: 200px; overflow: auto">
										<table class="am-table am-table-hover" id="infoList">
											<tbody>

											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="am-panel am-panel-default">
								<div class="am-panel-hd">
									<h4 class="am-panel-title"><label name="msg"></label>&nbsp;&nbsp;相关信息  Top60  高频词</h4>
								</div>
								<div id="do-not-say-3" class="am-panel-collapse am-collapse am-in">
									<div id="collapse-panel-3" style="max-height: 150px; overflow: auto">
										<table class="am-table am-table-hover" id="infoListMsg">
											<tbody>

											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="am-panel am-panel-default">
								<div class="am-panel-hd">
									<h4 class="am-panel-title">记事本</h4>
								</div>
								<div id="do-not-say-2" class="am-panel-collapse am-collapse am-in">
									<div class="am-panel-bd" style="max-height: 250px;">
										<textarea id="selectedKeyword" rows="9" name="selectedKeyword"
											cols="37" style="margin: -12px; border-style: none;">${selectedKeyword }
											      		</textarea>
									</div>
								</div>
							</div>
						</div>



					</div>
					<div class="am-form-group" style="margin-left: 10px; display: none">
						<button type="button" class="am-btn am-btn-default am-radius"
							onclick="overHead('<%=basePath%>','<%=rootPath%>')">上钻</button>
						<button type="button" class="am-btn am-btn-default am-radius"
							onclick="goDown('<%=basePath%>','<%=rootPath%>')">下钻</button>
						<button type="button" class="am-btn am-btn-default am-radius"
							onclick="previewInfo()">显示相关信息</button>
					</div>
					<div class="am-form-group"
						style="margin-left: 10px; width: 50%; text-align: right;">
						<table style="display: none;">
							<tr>
								<td width="100px" style="display: none;"><small>已选关键词</small></td>
								<td style="display: none;"><input id="keys" name="keys"
									class="am-form-field" style="width: 500px" /></td>
								<td>
									<div style="margin-left: 5px">
										<button type="button" class="am-btn am-btn-default am-radius"
											onclick="generateCretiveSet()">下一步</button>
									</div>
								</td>
							</tr>
						</table>
						<div style="margin-left: 5px;">
							<a href="<%=path%>/search/index" class="am-btn am-btn-danger"
								style="margin-right: 80px">上一步</a>
							<button type="button" class="am-btn am-btn-danger"
								onclick="generateCretiveSet()">下一步</button>
						</div>
					</div>
				</div>

<div class="am-popup" id="keyword-info" style="height: 80%; width: 620px; z-index: 10000">
    <div class="am-popup-inner" onmouseup="addNote()">
        <div class="am-popup-hd">
            <h4 class="am-popup-title" id="infoTitle"></h4>
      		<span data-am-modal-close="" class="am-close">×</span>
        </div>
        <div class="am-popup-bd" style="height: 92%;overflow-y:auto;overflow-x:hidden;">
        	<pre id="infoContent"></pre>
        </div>
        <div class="am-modal-footer" style="bottom: 0px;position: fixed">
  			<button class="am-btn" onclick="signWord()" style="float:left;width:50%;height:44px;background: #0e90d2;color: white;">加入记事本</button>
  			<button class="am-btn" style="width:50%;height:44px;background: #ffffff;" data-am-modal-close="">关闭</button>
        </div>
    </div>
</div>


			</div>
		</div>
	</div>
	<script src="<%=path%>/AmazeUI/js/jquery.min.js"></script>
	<script src="<%=path%>/AmazeUI/js/amazeui.min.js"></script>
	<script src="<%=path%>/AmazeUI/js/app.js"></script>
	<script src="<%=path%>/script/searchres.js"></script>
	<input type="hidden" id="id" name="id" value="${id }">


<script>
//清空文本框内的空格
$('#selectedKeyword').val($('#selectedKeyword').val().trim())

var seltxt='';
	$(function(){
		
		var path = '<%=path%>';
		loadResult('<%=basePath%>',path);
	})
	function searchKey() {
		var path = '<%=path%>';
		loadResult('<%=basePath%>',path);
	}
	
	function generateCretiveSet() {
		var path = '<%=path%>';

		var keyinput = $("#keyinput").val();//关键字
		var sourcetype = $('#sourcetype').val();//数据来源
		var selectedKeword = $("#selectedKeyword").val();//记事本
		var id = $("#id").val();//有可能是修改返回

		// 		var keys = $("#keys").val();
		var PARAMS = {
			selectedKeyword : selectedKeword,
			keyword : keyinput,
			sourcetype : sourcetype,
			id : id
		};
		window.location.href = path + "/creative/creativeEdit";

		var temp = document.createElement("form");
		temp.action = path + "/creative/creativeEdit";
		temp.method = "post";
		temp.style.display = "none";
		for ( var x in PARAMS) {
			var opt = document.createElement("textarea");
			opt.name = x;
			opt.value = PARAMS[x];
			temp.appendChild(opt);
		}
		//console.log(temp);
		document.body.appendChild(temp);
		temp.submit();
		return temp;
	}

	function addNote(){
		content = window.getSelection().toString();
		if(content!=''){
			seltxt = content;
		}
	}

	function signWord() {
		if(seltxt == ''){
			return;
		}
		var content = $("#selectedKeyword").val();
		if (content == "") {
			$("#selectedKeyword").val(seltxt);
		} else {
			$("#selectedKeyword").val(content + "\n" + seltxt);
		}
		var t = $("#selectedKeyword");
		t.scrollTop(500000);
		return;
	}
</script>
</body>
</html>