<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js fixed-layout">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>山东大学 创意引导系统</title>
<meta name="description" content="这是一个 index 页面">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" type="image/png" href="/i/favicon.png">
<link rel="apple-touch-icon-precomposed" href="/i/app-icon72x72@2x.png">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" href="<%=path%>/AmazeUI/css/amazeui.min.css" />
<link rel="stylesheet" href="<%=path%>/AmazeUI/css/admin.css">
<link rel="icon" href="http://sde.yangzhe.net/themes/sde/Public/images/favicon.png" type="image/x-icon">
<script>
	var path = '<%=path%>';
</script>
</head>
<body>

	<jsp:include page="/include/head.jsp" />

	<div class="am-cf admin-main">
		<jsp:include page="/include/tree.jsp" />

		<!-- content start -->
		<div class="admin-content">
			<div class="admin-content-body">
				<div class="am-cf am-padding am-padding-bottom-0">
					<div class="am-fl am-cf">
						<strong class="am-text-primary am-text-lg">我的创意</strong> / <small>创意归档</small>
					</div>
				</div>

				<hr>

				<div class="am-g">
					<div class="am-u-md-6">
						<div class="am-btn-toolbar">
							<div class="am-btn-group am-btn-group-xs">
								<button type="button" class="am-btn am-btn-default"
									onclick="window.location.href=path + '/search/index'">
									<span class="am-icon-plus"></span> 新增
								</button>
							</div>
						</div>
					</div>
				</div>

				<div class="am-g">
					<div class="am-u-sm-12">
						<form class="am-form">
							<table
								class="am-table am-table-striped am-table-hover table-main">
								<thead>
									<tr>
										<th class="table-id"></th>
										<th class="table-title">标题</th>
<!-- 										<th class="table-author am-hide-sm-only">作者</th> -->
										<th class="table-date am-hide-sm-only">创建日期</th>
										<th class="table-date am-hide-sm-only">投票截止日期</th>
										<th class="table-date am-hide-sm-only">好评度</th>
										<th class="table-date am-hide-sm-only">评论</th>
										<th class="table-set" style="padding-left: 60px">操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="p" items="${list }" varStatus="index">
										<tr>
											<td>${index.index +1}</td>
											<td>${p.startCreativeTitle }</td>
<%-- 											<td class="am-hide-sm-only">${p.userId }</td> --%>
											<td class="am-hide-sm-only">${p.createTime }</td>
											<td class="am-hide-sm-only">
												${p.endTime}
											</td>
											<td class="am-hide-sm-only">
												${p.vote}
											</td>
											<td class="am-hide-sm-only">${p.commentCount }</td>											
											<td>
												<div class="am-btn-toolbar">
													<div class="am-btn-group am-btn-group-xs">
														<button type="button" class="am-btn am-btn-default am-btn-xs am-text-secondary"
															data-am-modal="{target: '#doc-modal-1', closeViaDimmer: 0, width: 600, height: 320}">
															<span class="am-icon-search"></span> 初始创意
														</button>
														<button type="button" class="am-btn am-btn-default am-btn-xs am-text-danger"
															data-am-modal="{target: '#doc-modal-1', closeViaDimmer: 0, width: 600, height: 320}">
															<span class="am-icon-home"></span> 最终创意
														</button>														
<!-- 														<button type="button" class="am-btn am-btn-default am-btn-xs am-text-warning" -->
<!-- 															data-am-modal="{target: '#doc-modal-2', closeViaDimmer: 0, width: 600, height: 275}"> -->
<!-- 															<span class="am-icon-sellsy"></span> 投票 -->
<!-- 														</button> -->
														<button type="button" class="am-btn am-btn-default am-btn-xs am-text-success">
															<span class="am-icon-pencil-square-o"></span> 评论
														</button>
														<input type="hidden" name="id" value="${p.id }" /> 													
														<input type="hidden" name="startCreativeTitle" value="${p.startCreativeTitle }" /> 
														<input type="hidden" name="startCreativeContent" value="${p.startCreativeContent }" /> 
														<input type="hidden" name="endCreativeTitle" value="${p.endCreativeTitle }" /> 
														<input type="hidden" name="endCreativeContent" value="${p.endCreativeContent }" /> 
														<input type="hidden" name="vote" value="${p.vote }" /> 
														<input type="hidden" name="keyword" value="${p.keyword }" /> 
													</div>
												</div>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<div class="am-cf">
								共 ${total }条记录
								<div class="am-fr">
									<ul class="am-pagination">
<!-- 										<li class="am-disabled"> -->
<!-- 											<a href="JavaScript:void(0)">«</a> -->
<!-- 										</li> -->
<%-- 										<li class="am-active"><a href="<%=path %>/myCreative/createListArchived?pageIndex=1">1</a></li> --%>
<!-- 										<li><a href="#">2</a></li> -->
<!-- 										<li><a href="#">3</a></li> -->
<!-- 										<li><a href="#">4</a></li> -->
<!-- 										<li><a href="#">5</a></li> -->
<!-- 										<li><a href="#">»</a></li> -->
									</ul>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- content end -->
	</div>
	
	<!-- view -->
	<div class="am-modal am-modal-no-btn" tabindex="-1" id="doc-modal-1">
		<div class="am-modal-dialog">
			<div class="am-modal-hd">
				<a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a>
			</div>
			<div class="am-modal-bd">
				<div class="am-form">
					<div class="am-form-group">
						<label for="doc-vld-name-2-1">创意名称：</label> 
						<input type="text" id="doc-vld-name-2-1" value="" />
					</div>
					<div class="am-form-group">
						<label for="doc-vld-ta-2-1" style="text-align: left;">描述：</label>
						<textarea id="doc-vld-ta-2-1" rows="7" class="am-sans-serif"></textarea>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 投票 -->
<!-- 	<div class="am-modal am-modal-no-btn" tabindex="-1" id="doc-modal-2"> -->
<!-- 		<div class="am-modal-dialog"> -->
<!-- 			<div class="am-modal-hd"> -->
<!-- 				<a href="javascript: void(0)" class="am-close am-close-spin" data-am-modal-close>&times;</a> -->
<!-- 			</div> -->
<!-- 			<div class="am-modal-bd"> -->
<!-- 				<div class="am-panel am-panel-default" style="margin-top: 16px"> -->
<!-- 					<div class="am-panel-hd">评价结果</div> -->
<!-- 					<div class="am-panel-bd"> -->
<!-- 						<table class="am-table"> -->
<!-- 							<thead> -->
<!-- 								<tr> -->
<!-- 									<td>名称</td> -->
<!-- 									<td>Y(同意)</td> -->
<!-- 									<td>N(反对)</td> -->
<!-- 								</tr> -->
<!-- 							</thead> -->
<!-- 							<tbody> -->
<!-- 								<tr> -->
<!-- 									<td id="commentName"></td> -->
<!-- 									<td id="y"></td> -->
<!-- 									<td id="n"></td> -->
<!-- 								</tr> -->
<!-- 							</tbody> -->
<!-- 						</table> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 		</div> -->
<!-- 	</div> -->

<!-- 评论 -->
<div class="am-popup" id="my-popup" style="z-index: 12000;min-height: 500px;width: 50%">
	<div class="am-popup-inner">
		<div class="am-popup-hd">
			<h4 class="am-popup-title" id="commentTitle"></h4>
			<span data-am-modal-close class="am-close">&times;</span>
		</div>
		<div class="am-popup-bd" style="background-color: white">

			<table class="am-table am-table-bordered am-table-centered" id="commentTable">
				<tr>
					<th>好评度</th>
			    	<th>评价印象</th>
				</tr>
				<tr>
			    <td rowspan="2" class="am-text-middle am-text-danger" width="40%">
			    	<span id="vote">95分</span>
					<div class="am-progress">
					  <div class="am-progress-bar am-progress-bar-warning" style="width: 90%" id="voteWidth"></div>
					</div>
			    </td>
			    <td rowspan="2" class="am-text-middle" id="keyword">
<!-- 					<span  class="am-badge am-radius am-text-sm">是正品</span > -->
<!-- 					<span  class="am-badge am-radius am-text-sm">效果不错</span > -->
<!-- 					<span  class="am-badge am-radius am-text-sm">套装实惠</span > -->
<!-- 					<span  class="am-badge am-radius am-text-sm">很好用</span > -->
<!-- 					<span  class="am-badge am-radius am-text-sm">物流速度快</span > -->
<!-- 					<span  class="am-badge am-radius am-text-sm">性价比很高</span > -->
<!-- 					<span  class="am-badge am-radius am-text-sm">脸上很舒服</span > -->
<!-- 					<span  class="am-badge am-radius am-text-sm">非常极品</span > -->
			    </td>
			  </tr>
			</table>
			
			<ul class="am-comments-list">
<%-- 				<c:forEach var="p" items="${list }"> --%>
<!-- 				<li class="am-comment"> -->
<!-- 					<div class="am-comment-main" style="margin-left: 10px"> -->
<!-- 						<header class="am-comment-hd"> -->
<!-- 				      		<div class="am-comment-meta"> -->
<!-- 				        		<span class="am-comment-author">某人</span> -->
<!-- 								评论于 <span>2014-7-12 15:30</span> -->
<!-- 				      		</div> -->
<!-- 						</header> -->
				
<!-- 					    <div class="am-comment-bd"> -->
<!-- 							三星的手机，每年都有不同的灵感作为当年新品的设计语言，例如去年三星S5的设计理念是“数字城市”，而今年的S6设计语言则是“流动金属”。S6全新的设计语言，也带来了全新的机身材质，玻璃和金属，放弃了在之前的Galaxy S设备上常用的塑料，这一点也让三星摘掉了被吐槽许久的“万年大塑料”的帽子。 -->
<!-- 					    </div> -->
<!-- 					</div> -->
<!-- 			  	</li> -->
<%-- 			  	</c:forEach> --%>
			</ul>
	    </div>
	</div>
</div>

<div class="am-modal am-modal-alert" tabindex="-1" id="my-alert">
	<div class="am-modal-dialog">
		<div class="am-modal-bd">
      		没有投票与评论
    	</div>
		<div class="am-modal-footer">
      	<span class="am-modal-btn">确定</span>
    	</div>
	</div>
</div>

	<script src="<%=path%>/AmazeUI/js/jquery.min.js"></script>
	<script src="<%=path%>/AmazeUI/js/amazeui.min.js"></script>
	<script src="<%=path%>/AmazeUI/js/app.js"></script>
	<script src="<%=path%>/script/createListArchived.js"></script>
	<script>
		$(function() {
			
			var $page = $('.am-pagination');
			var total = '${total}';//总数量
			var pageCount = '${pageCount}';//总页数
			var pageIndex = '${pageIndex}';//当前页
			
			if(pageCount > 1){
				if(pageIndex == 1){
					$page.append('<li class="am-disabled"><a href="JavaScript:void(0)">«</a></li>');
				}else{
					$page.append('<li><a href="'+path+'/myCreative/createListArchived?pageIndex=1">«</a></li>');
				}
				
				for(var i = 1; i <= pageCount ; i++){
					if(i == pageIndex){
						$page.append('<li class="am-active"><a href="'+path+'/myCreative/createListArchived?pageIndex='+i+'">'+i+'</a></li>');
					}else{
						$page.append('<li><a href="'+path+'/myCreative/createListArchived?pageIndex='+i+'">'+i+'</a></li>');
					}
				}
				if(pageIndex == pageCount){
					$page.append('<li class="am-disabled"><a href="JavaScript:void(0)">»</a></li>');
				}else{
					$page.append('<li><a href="'+path+'/myCreative/createListArchived?pageIndex='+pageCount+'">»</a></li>');
				}				
			}
		})
	</script>
</body>
</html>
