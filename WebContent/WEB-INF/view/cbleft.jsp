<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%
    	String path = request.getContextPath();
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<%=path %>/jquery-easyui-1.5/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="<%=path %>/jquery-easyui-1.5/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="<%=path %>/css/demo.css">
    <script type="text/javascript" src="<%=path %>/jquery-easyui-1.5/jquery.min.js"></script>
    <script type="text/javascript" src="<%=path %>/jquery-easyui-1.5/jquery.easyui.min.js"></script>
<title></title>
</head>
<body>
	<div>
		<ul class="easyui-tree" data-options="iconCls:''">
		   <li>
		       <span>社区反馈</span>
		       <ul>
		        	<li>
						<span>创意评价</span>
					</li>
					<li>
						<span>方案反馈</span>
					</li>
					<li>
						<span>产品评价</span>
					</li>
		        </ul>
		    </li>
		        					
		 </ul>
	</div>
</body>
</html>

<script>
	$(function(){
		$(".tree-icon,.tree-file").removeClass("tree-icon tree-file");
		$(".tree-icon,.tree-folder").removeClass("tree-icon tree-folder tree-folder-open tree-folder-closed");
	})
</script>