<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
%>
<!doctype html>
<html class="no-js fixed-layout">
<head>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>山东大学 创意引导系统</title>
<meta name="description" content="这是一个 index 页面">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" type="image/png" href="/i/favicon.png">
<link rel="apple-touch-icon-precomposed" href="/i/app-icon72x72@2x.png">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" href="<%=path%>/AmazeUI/css/amazeui.min.css" />
<link rel="stylesheet" href="<%=path%>/AmazeUI/css/admin.css">
<script>
	var path = '<%=path%>';
</script>
</head>
<body>
	<jsp:include page="/include/head.jsp" />

	<div class="am-cf admin-main">
		<jsp:include page="/include/tree.jsp" />

		<!-- content start -->
		<div class="admin-content">
			<div class="admin-content-body">
				<div class="am-cf am-padding">
					<div class="am-fl am-cf">
						<strong class="am-text-primary am-text-lg">首页</strong>
					</div>
				</div>

				<ul
					class="am-avg-sm-1 am-avg-md-4 am-margin am-padding am-text-center admin-content-list ">
					<li><span class="am-text-success"> <span
							class="am-icon-btn am-icon-file-text"></span><br />创意总数<br />${total}</span>
					</li>
					<li><span class="am-text-warning"> <span
							class="am-icon-btn am-icon-briefcase"></span><br />已完成创意<br />${finishCount }
					</span></li>
					<li><span class="am-text-danger"><span
							class="am-icon-btn am-icon-recycle"></span><br />已发布创意<br />${publicCount }</span></li>
					<li><span class="am-text-secondary"><span
							class="am-icon-btn am-icon-user-md"></span><br />已归档创意<br />${archivedCount }</span></li>
				</ul>

				<div class="am-g">
					<div class="am-u-sm-12">
						<form class="am-form">
							<table
								class="am-table am-table-striped am-table-hover table-main">
								<thead>
									<tr>
										<th class="table-id"></th>
										<th class="table-title">标题</th>
										<th class="table-author am-hide-sm-only">内容描述</th>
										<th class="table-set">状态</th>
										<th class="table-set">创建人</th>
										<th class="table-date am-hide-sm-only">创建日期</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="p" items="${list }" varStatus="index">
										<tr>
											<td><c:if test="${pageIndex == 1}">
													${index.index +1}
												</c:if> <c:if test="${pageIndex != 1}">
													${(pageIndex -1)*pageSize + index.index +1}
												</c:if></td>
											<td width="20%"><a
												href="<%=path%>/creative/creativePublic?id=${p.id }">${p.startCreativeTitle }</a></td>
											<td width="35%">
												<div style="display: none">${p.startCreativeContent }</div>
												<span name="contentShow"></span>
											</td>
											<td><c:if test="${p.status == 0 }">
													已完成
												</c:if> <c:if test="${p.status == 1 }">
													已发布
												</c:if> <c:if test="${p.status == 2 }">
													已归档
												</c:if></td>
											<td class="am-hide-sm-only">${p.userName }</td>
											<td class="am-hide-sm-only">${p.createTime }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<div class="am-cf">
								显示第
								<c:if test="${pageIndex == 1}">
									${pageIndex}
								</c:if>
								<c:if test="${pageIndex != 1}">
									${(pageIndex -1)*pageSize}
								</c:if>
								至 ${pageIndex*pageSize} 项结果，共${total}项
								<div class="am-fr">
									<ul class="am-pagination"></ul>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- content end -->

	</div>

	<script src="<%=path%>/AmazeUI/js/jquery.min.js"></script>
	<script src="<%=path%>/AmazeUI/js/amazeui.min.js"></script>
	<script src="<%=path%>/AmazeUI/js/app.js"></script>
	<script>
		$(function() {

			var $page = $('.am-pagination');
			var total = '${total}';//总数量
			var pageCount = '${pageCount}';//总页数
			var pageIndex = '${pageIndex}';//当前页

			if (pageCount > 1) {
				if (pageIndex == 1) {
					$page.append('<li class="am-disabled"><a href="JavaScript:void(0)">«</a></li>');
				} else {
					$page.append('<li><a href="' + path
							+ '/main/index?pageIndex=1">«</a></li>');
				}

				for (var i = 1; i <= pageCount; i++) {
					if (i == pageIndex) {
						$page.append('<li class="am-active"><a href="' + path
								+ '/main/index?pageIndex=' + i + '">' + i
								+ '</a></li>');
					} else {
						$page.append('<li><a href="' + path
								+ '/main/index?pageIndex=' + i + '">' + i
								+ '</a></li>');
					}
				}
				if (pageIndex == pageCount) {
					$page.append('<li class="am-disabled"><a href="JavaScript:void(0)">»</a></li>');
				} else {
					$page.append('<li><a href="' + path
							+ '/main/index?pageIndex=' + pageCount
							+ '">»</a></li>');
				}
			}

			$('span[name="contentShow"]').each(function(i){
				var text = $(this).siblings('div').text();
				$(this).html(text);
			});
		})
	</script>
</body>
</html>
