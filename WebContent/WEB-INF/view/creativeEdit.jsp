<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js fixed-layout">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>山东大学 创意引导系统</title>
<meta name="description" content="这是一个 index 页面">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" type="image/png" href="/i/favicon.png">
<link rel="apple-touch-icon-precomposed" href="/i/app-icon72x72@2x.png">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" href="<%=path%>/AmazeUI/css/amazeui.min.css" />
<link rel="stylesheet" href="<%=path%>/AmazeUI/css/admin.css">

    <!--Ueditor相关-->
    <script type="text/javascript" src="<%=path %>/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" src="<%=path %>/ueditor/ueditor.all.min.js"></script>
    <script type="text/javascript" src="<%=path %>/ueditor/lang/zh-cn/zh-cn.js"></script>
    
<script>
	var path = '<%=path%>';
</script>
</head>
<body>

	<jsp:include page="/include/head.jsp" />

	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/include/tree.jsp" />
		<!-- sidebar end -->

		<!-- content start -->
		<div class="admin-content">
			<div class="admin-content-body">
				<div class="am-cf am-padding am-padding-bottom-0">
					<div class="am-fl am-cf">
						<strong class="am-text-primary am-text-lg">创意引导</strong> / <small>创意编辑</small>
					</div>
				</div>
				
				<hr />
				
				<div class="am-u-sm-12" >
					<div style="width: 68%;float: left;" >
						<section class="am-panel am-panel-default" id="question-content">
	                        <header class="am-panel-hd">
	                            <h3 class="am-panel-title">创意描述</h3>
	                        </header>
	                        <div class="am-panel-bd">
	                            <form id="question" class="am-form">
	                                <div class="am-form-group">
	                                    <label for="doc-select-1">创意名称</label>
	                                    <input name="title" id="title" type="text" placeholder="创意名称" value="${title }">
	                                </div>
	
	                                <div class="am-form-group">
										<label for="doc-select-1">创意描述</label>
										<script id="que" name="que" type="text/plain">${content }</script>
									    <!-- 实例化编辑器 -->
									    <script type="text/javascript">
									        var ue = UE.getEditor('que', {
									            toolbars: [
									                ['source', 'undo', 'redo','bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc','simpleupload', 'insertimage'],
									            ],
									            initialFrameHeight: 200,
									        });
									    </script>
	                                </div>
	                                
	                                <div id="que-bt">
	                                    <div style="margin-left: 20px;float: right">
	                                        <a id="next" style="width:150px" class="am-btn am-btn-danger am-btn-block">下一步</a>
	                                    </div>
	                                    <div style="float: right">
	                                        <a id="top" style="width:150px;" class="am-btn am-btn-danger am-btn-block">上一步</a>
	                                    </div>
	                                </div>
	                                <br>
	                                <br>
	                            <textarea name="que" id="ueditor_textarea_que" style="display: none;"></textarea></form>
	                        </div>
                        	<div style="clear: both"></div>
                    	</section>
					</div>
					<div style="width: 30%;float: right;">
		                <section class="am-panel am-panel-default">
		                    <header class="am-panel-hd">
		                        <h3 class="am-panel-title">记事本</h3>
		                    </header>
		                    <div class="am-panel-bd" style="padding: 10px">
		                        <form class="am-form">
		                            <div class="am-form-group">
		                                <textarea class="" rows="10" id="noteContent">${selectedKeyword }</textarea>
		                            </div>
		                        </form>
		                    </div>
		                </section>
					</div>
				</div>
			</div>
		</div>
		<!-- content end -->
	</div>
	<form id="form1" action="<%=path%>/search/gosearch" method="post">
		<input type="hidden" id="id" name="id" value="${id }"/>
		<input type="hidden" id="keyword" name="keyword" value="${keyword }"/>
		<input type="hidden" id="sourcetype" name="sourcetype" value="${sourcetype }"/>
		<textarea style="display: none" id="selectedKeyword" name="selectedKeyword">${selectedKeyword }</textarea>
	</form>
		
	<script src="<%=path%>/AmazeUI/js/jquery.min.js"></script>
	<script src="<%=path%>/AmazeUI/js/amazeui.min.js"></script>
	<script src="<%=path%>/AmazeUI/js/app.js"></script>

	<script>
	
	//上一步
	$('#top').on('click', function() {
		$('#selectedKeyword').val($('#noteContent').val());
		$('#form1').submit();
	});
	
	
	//下一步
	$('#next').on('click', function() {
		var title = $('#title').val();
		var content = UE.getEditor('que').getContent();

    	$.post(path+'/myCreative/creativeSave',
        		{	'title':title,
        			'content':content,
        			'keyword':$('#keyword').val(),
        			'sourcetype':$('#sourcetype').val(),
        			'selectedKeyword':$('#noteContent').val(),
        			'id':$('#id').val()
        		},function(msg){
//         			alert(msg.code)
        		if(msg.code == '1000'){
    	         	window.location.href = path + '/creative/creativePublic?id=' + msg.id;
        		}else{
        			alert(msg.errorMsg);
        		}
        	})
	});	
	</script>
</body>
</html>
