<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js fixed-layout">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>山东大学 创意引导系统</title>
<meta name="description" content="这是一个 index 页面">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" type="image/png" href="/i/favicon.png">
<link rel="apple-touch-icon-precomposed" href="/i/app-icon72x72@2x.png">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" href="<%=path%>/AmazeUI/css/amazeui.min.css" />
<link rel="stylesheet" href="<%=path%>/AmazeUI/css/admin.css">
<script>
	var path = '<%=path%>';
</script>
</head>
<body>
	<jsp:include page="/include/head.jsp" />

	<div class="am-cf admin-main">
		<jsp:include page="/include/tree.jsp" />
		
		
		<div class="admin-content">
			<form action="" method="get" class="am-form am-form-horizontal" style="margin-top: 40px">
			  <div class="am-form-group">
			    <label for="doc-ipt-3" class="am-u-sm-2 am-form-label">标题</label>
			    <div class="am-u-sm-10">
			      <input type="text" id="text1" id="doc-ipt-3" placeholder="输入你的标题">
			    </div>
			  </div>
			
			  <div class="am-form-group">
			    <div class="am-u-sm-10 am-u-sm-offset-2">
			      <button type="button" class="am-btn am-btn-default" id="get">get提交</button>
			    </div>
			  </div>
			</form>
					
			<form action="http://localhost:8080/test/api/getPost" method="post" class="am-form am-form-horizontal" style="margin-top: 40px">
			  <div class="am-form-group">
			    <label for="doc-ipt-pwd-2" class="am-u-sm-2 am-form-label">内容</label>
			    <div class="am-u-sm-10">
			      <input type="text" id="text2" placeholder="输入你的内容">
			    </div>
			  </div>
			
			  <div class="am-form-group">
			    <div class="am-u-sm-10 am-u-sm-offset-2">
			      <button type="button" class="am-btn am-btn-default" id="post">post提交</button>
			    </div>
			  </div>
			</form>
			
			<form action="http://www.sml163.com/sdcyyd/myCreative/testPost" method="post" class="am-form am-form-horizontal" style="margin-top: 40px">
			  <div class="am-form-group">
			    <label for="doc-ipt-pwd-2" class="am-u-sm-2 am-form-label">内容</label>
			    <div class="am-u-sm-10">
			      <input type="text" id="text2" placeholder="输入你的内容">
			    </div>
			  </div>
			  <div class="am-form-group">
			    <div class="am-u-sm-10 am-u-sm-offset-2">
			      <button type="submit" class="am-btn am-btn-default" id="post1">submit提交</button>
			    </div>
			  </div>
			</form>			
		</div>
	</div>

	<script src="<%=path%>/AmazeUI/js/jquery.min.js"></script>
	<script src="<%=path%>/AmazeUI/js/amazeui.min.js"></script>
	<script src="<%=path%>/AmazeUI/js/app.js"></script>

<script>
$('#get').click(function(){
	$.ajax({ 
	    url: 'http://www.sml163.com/cors/api/testGet',
	    dataType: 'json',
	    type: 'get', 
	    scriptCharset: 'utf-8',
	    data:{'text':$('#text1').val()},
	    success: function(data){
    		alert(data.message)
	    }
	});
})
$('#post').click(function(){
	var s = '{"success":true,"messageType":1,"message":"'+$('#text2').val()+'"}';
	$.ajax({ 
	    url: 'http://localhost:8080/test/api/testPost',
	    dataType: 'json',
	    type: 'post', 
	    contentType:'application/json',
	    scriptCharset: 'utf-8',
	   	data:s,
	   	processData:false,
	    success: function(data){
    		alert(data.message)
	    }
	});
})
	
</script>
</body>
</html>