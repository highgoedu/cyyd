<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
	<header class="am-topbar am-topbar-inverse admin-header" style="background:#981808" >
	  <div class="am-topbar-brand">
	    <strong>大数据驱动创新方法工作服务平台</strong><small> 创意引导系统</small>
	  </div>
	
	  <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>
	
	  <div class="am-collapse am-topbar-collapse" id="topbar-collapse">
	
	    <ul class="am-nav am-nav-pills am-topbar-nav am-topbar-right admin-header-list">
		<li>
                <button style="margin-left: 20px;float: right;margin-top: 7px" type="button" class="am-btn am-btn-warning am-radius" data-am-modal="{target: '#doc-modal-1', closeViaDimmer: 0, width: 900, height: 500}">
                    视频演示
                </button>
               
 <button style="margin-left: 20px;float: right;margin-top: 7px" type="button" class="am-btn am-btn-warning am-radius" data-am-modal="{target: '#doc-modal-2', closeViaDimmer: 0, width: 850, height: 500}">
                    使用指南
                </button>

            </li>

            <div class="am-modal am-modal-no-btn" tabindex="-1" id="doc-modal-1">
                <div class="am-modal-dialog">
                    <div class="am-modal-hd" style="color: black">创意引导系统-视频演示
                        <a href="javascript: void(0)" style="color:black" class="am-close am-close-spin"
                           data-am-modal-close>&times;</a>
                    </div>
                    <div class="am-modal-bd">
                     
<embed src='http://player.youku.com/player.php/sid/XMjc1MjM2MDE0NA==/v.swf' allowFullScreen='true' quality='high' width='800' height='450' align='middle' allowScriptAccess='always' type='application/x-shockwave-flash'></embed> </div>
                </div>
            </div>		


            <div class="am-modal am-modal-no-btn" tabindex="-1" id="doc-modal-2" style="width: 100%">
                <div class="am-modal-dialog">
                    <div class="am-modal-hd" style="color: black">使用指南(<span id="pic_count"></span>/14)
                        <a href="javascript: void(0)" style="color:black" class="am-close am-close-spin"
                           data-am-modal-close>&times;</a>
                    </div>
                    <div class="am-modal-bd">
                    <img src="http://www.bigdatainnovation.cn/public/images/cyyd/1.png" width="100%" id="demo_pic">

		 </div>
            </div>
  			</div>


<!-- 	      <li><a href="javascript:;"> -->
<!-- 	      	<span class="am-icon-envelope-o"></span>我的创意<span class="am-badge am-badge-warning">5</span></a> -->
<!-- 	      </li> -->
	      <li class="am-dropdown" data-am-dropdown>
	        <a class="am-dropdown-toggle" data-am-dropdown-toggle href="javascript:;">
	          <span class="am-icon-users"></span> ${name }<span class="am-icon-caret-down"></span>
	        </a>
	        <ul class="am-dropdown-content">
<!-- 	          <li><a href="javascript:;"><span class="am-icon-user"></span> 资料</a></li> -->
<!-- 	          <li><a href="javascript:;"><span class="am-icon-cog"></span> 设置</a></li> -->
	          <li><a href="javascript:;" onclick="exit()"><span class="am-icon-power-off"></span> 退出</a></li>
	        </ul>
	      </li>
	      <li class="am-hide-sm-only"><a href="javascript:;" id="admin-fullscreen"><span class="am-icon-arrows-alt"></span> <span class="admin-fullText">开启全屏</span></a></li>
	    </ul>
	  </div>
	</header>



	<script src="<%=path%>/AmazeUI/js/jquery.min.js"></script>

<script>
function exit(){
	$.ajax({ 
	    url: path + '/user/exit',
	    dataType: 'json',
	    type: 'post', 
	    scriptCharset: 'utf-8',
	    success: function(data){
	    	window.location.href = path + "/main/index";
	    }
	});
}


var path1 = 'http://www.bigdatainnovation.cn/public/images/cyyd/';
num=2;
$('#demo_pic').on('click',function(){
    var next_pic=path1+num+'.png';
    $('#pic_count').html(num);
    num++;
    if (num==15) num=1;
    $('#demo_pic').attr('src',next_pic);
})
</script>
</body>
</html>
