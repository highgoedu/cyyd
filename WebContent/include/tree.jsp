<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
<div class="admin-sidebar am-offcanvas" id="admin-offcanvas">
	<div class="am-offcanvas-bar admin-offcanvas-bar">
		<ul class="am-list admin-sidebar-list">
		<li><a href="http://www.bigdatainnovation.cn/index.php/Index/index" target="_blank"><span class="am-icon-home"></span> 首页</a></li>
		<li><a href="<%=path %>/main/index"><span class="am-icon-wpforms"></span> 创意管理</a></li>
		<li><a href="<%=path %>/search/index"><span class="am-icon-pencil-square-o"></span> 创意引导</a></li>
		<li><a href="http://sf.bigdatainnovation.cn/" target="_blank"><span class="am-icon-table"></span> 创客社区</a></li>
      </ul>
    </div>
</div>
</body>
</html>
