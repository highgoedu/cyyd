<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String userId = (String)request.getSession().getAttribute("userId");
	System.out.println("userId = " + userId);
	if(userId != null){
		response.sendRedirect(path + "/main/index"); 
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script>
	var path = '<%=path%>';
</script>
</head>
<body>
	<script src="<%=path%>/AmazeUI/js/jquery.min.js"></script>
<script>
	$(function() {
		var userId = '<%=userId%>';
		if(userId != null && userId != "null" && userId != "" && userId != "undefined") {
			return;
		}
		$.ajax({
            type: 'get',
            url: 'http://www.bigdatainnovation.cn/User/login/checkLogin',
            data: "",
            dataType: "jsonp",
            jsonp: "callback",//跨域
            jsonpCallback: "success_jsonpCallback",
            success: function (data) {
            	if(data.user) {
            		var userId = data.user.id;
                	if(userId && userId != "") {
                		console.log("data.user.id = " + userId);
                		console.log("data.user.uid = " + data.user.sf_uid);
                		console.log("data.user.user_nicename = " + data.user.user_nicename);
                		loginUser(userId,data.user.sf_uid,data.user.user_nicename);
                	} else {
                		window.location.href = "http://www.bigdatainnovation.cn/User/login";
                	}
            	} else {
            		window.location.href = "http://www.bigdatainnovation.cn/User/login";
            	}
            },
            error: function () {
                alert("请求出错！");
            }
        });
	})
	
	function loginUser(userId,uid,user_nicename) {
		$.ajax({ 
		    url: path + '/user/login?userId='+userId + '&uid='+uid+'&name='+user_nicename,
		    dataType: 'json',
		    type: 'post', 
		    scriptCharset: 'utf-8',
		    success: function(data){
		    	window.location.href = path + "/main/index";
		    }
		});
	}
</script>
</body>
</html>
