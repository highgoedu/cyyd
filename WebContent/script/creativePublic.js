
var $modal_1 = $('#my-alert1');//归档
var $modal_2 = $('#my-alert2');//发布

/**
 * 发布
 */
function recommend_publish(obj){

	$('#my-confirm2').modal({
	    relatedTarget: this,
	    onConfirm: function(options) {
	    	$.post( path + '/myCreative/creativePublic',{'id':$('#id').val(),'score':$('#score').val()},function(msg){
	    		if(msg.code == '1000'){
	    			$modal_2.modal('toggle');
	    			//置灰按钮和文本框
	    			$(obj).attr('disabled','disabled');
	    			$(obj).removeAttr('onclick');
	    			$('#score').parent('div').removeClass('am-form-warning');
	    			//window.location.href = path + "/main/index";
	    		}else{
	    			alert(msg.msg)
	    		}
	    	})
	    },
	    // closeOnConfirm: false,
	    onCancel: function() {
	      //alert('算求，不弄了');
	    }
	  });
}

/**
 * 归档
 */
function recommend_lock(obj){
	$('#my-confirm1').modal({
	    relatedTarget: this,
	    onConfirm: function(options) {
	    	$.post( path + '/myCreative/creativeArchived',{'id':$('#id').val()},function(msg){
	    		if(msg.code == '1000'){
	    			$modal_1.modal('toggle');
	    			
	    			//置灰得分
	    			$('#score').parent('div').removeClass('am-form-warning');
	    			//置灰归档
	    			$(obj).attr('disabled','disabled');
	    			$(obj).removeAttr('onclick');
	    			//置灰上一步
	    			$('#top').removeAttr('onclick');
	    			$('#top').attr('disabled','disabled');
	    			//置灰发布
	    			$('#fb').removeAttr('onclick');
	    			$('#fb').attr('disabled','disabled');
//	    			window.location.href = path + "/main/index";
	    		}else{
	    			alert(msg.msg)
	    		}
	    	})	    	
//	      var $link = $(this.relatedTarget).prev('a');
//	      var msg = $link.length ? '你要删除的链接 ID 为 ' + $link.data('id') :
//	        '确定了，但不知道要整哪样';
//	      alert(msg);
	    },
	    // closeOnConfirm: false,
	    onCancel: function() {
	      //alert('算求，不弄了');
	    }
	  });
}

