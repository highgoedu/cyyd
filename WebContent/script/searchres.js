/**
 * 
 */
	var selectedKey = "";
	var keyList = new Array();
	var historyList = new Array();
	
	function loadResult(basePath,path) {
		var layerNum = $("#layernum").val();
		var layerCount = $("#layercount").val();
		var keyword = $("#keyinput").val();
		var showType = $("#showType").val();
		
		$.ajax({ 
		    url: path + "/search/searchkey?keyword=" + keyword + "&layerNum=" + layerNum + "&layerCount=" + layerCount + "&showType=" + showType,
		    dataType: 'json',
		    type: 'post', 
		    scriptCharset: 'utf-8',
		    success: function(data){
		    		var jsonPath = data.jsonpath;
		    		jsonPath = basePath + jsonPath;
		    		if(jsonPath != null) {
		    			if(showType == "tree") {
		    				showResult(basePath,jsonPath,path);
		    			} else if(showType == "force") {
		    				showForceResult(basePath,jsonPath,path);
		    			} else if(showType == "pack") {
		    				//pack停用
		    				showPackResult(basePath,jsonPath,path);
		    			} else {
		    				showResult(basePath,jsonPath,path);
		    			}
		    		
		    			loadInfo(keyword);
		    			$("#keys").val(keyword);
		    		}
		    	}
		});
	}
	
	function previewInfo() {
		if(keyList.length <= 0) {
			alert("未选择关键词");
			return;
		}
		var keyword = keyList[keyList.length - 1]
		loadInfo(keyword);
	}
	
	function loadInfo(keyword) {

		$.ajax({ 
		    url: path + "/search/showinfo?keyword=" + keyword,
		    dataType: 'json',
		    type: 'post', 
		    scriptCharset: 'utf-8',
		    success: function(data){
		    		var infoList = data.titlelist;
		    		var table = $('#collapse-panel-2 table tbody');
		    		table.empty();
		    		for(var i = 0; i < infoList.length; i ++) {
		    			var info = infoList[i];
		    			var index = info.indexOf("$$");
		    			var title = info.substring(0,index);
		    			var id = info.substring(index + 2,info.length);
		    			if(i == 0){
			    			table.append("<tr><td style=\"display:none;border-top:0;\">" + id + "</td><td style=\"padding-left: 3.2rem;;border-top:0;\">" + title  + "</td></tr>");
		    			}else{
			    			table.append("<tr><td style=\"display:none\">" + id + "</td><td style=\"padding-left: 3.2rem;\">" + title  + "</td></tr>");
		    			}
		    		}
		    		//给相关信息标题赋值
//		    		$('#msg').html(keyword);
		    		$('label[name="msg"]').text(keyword);
		    		//2016.11.04注释
//		    		var keyList = data.keylist;
//		    		var keys = "";
//		    		for(var i = 0; i < keyList.length; i++) {
//		    			if(keys != "") {
//		    				keys = keys + "&#13;&#10;" + keyList[i];
//		    			} else {
//		    				keys = keyList[i];
//		    			}
//		    			
//		    		}
//		    		$("#selectedKeyword").html(keys);
		    		
		    		$("#infoList").find("tr").dblclick(function(){
		    			var id = $(this).find("td").eq(0).text();
		    			showDetailInfo(id);
		    			$("#keyword-info").modal('open');
		    		});
		    		
		    		
		    		//2016.11.08新增,点击关键字加入记事本
		    		var keyList = data.keylist;
		    		var keys = "";
		    		var table = $('#collapse-panel-3 table tbody');
		    		table.empty();
		    		for(var i = 0; i < keyList.length; i++) {
		    			if(i == 0){
			    			table.append("<tr><td style=\"padding-left: 3.2rem;;border-top:0;\">" + keyList[i]  + "</td></tr>");
		    			}else{
			    			table.append("<tr><td style=\"padding-left: 3.2rem;\">" + keyList[i]  + "</td></tr>");
		    			}
		    		}
		    		//双击最相关词汇，加入记事本
		    		$("#infoListMsg").find("tr").dblclick(function(){
		    			var text = $(this).find("td").eq(0).text();
		    			var keywordText = $("#selectedKeyword").val();
		    			if(keywordText == ''){
		    				$("#selectedKeyword").val(text);
		    			}else{
		    				$("#selectedKeyword").val($("#selectedKeyword").val() + "\n" + text);
		    			}
		    		});
		    	}
		});
	}
	
	function showDetailInfo(infoID) {
		$.ajax({ 
		    url: path + "/search/showdetail?id=" + infoID,
		    dataType: 'json',
		    type: 'post', 
		    scriptCharset: 'utf-8',
		    success: function(data){
		    	var content = data.abstract + data.info;
		    	var infoTitle = data.title;
		    	$("#infoTitle").text(infoTitle);
		    	$("#infoContent").text(content + data.abstract);
		    }
		    	
		});
	}
	
	var arr = ["","","","",""];
	function checkSource() {
		var all = $("#all");
		var productData = $("#productdata");
		var patentData = $("#patentdata");
		var techBlog = $("#techblog");
		var professional = $("#professional");
		var user = $("#user");
		
		if(all.prop("checked")) {
			arr[0] = productData.val();
			arr[1] = patentData.val();
			arr[2] = techBlog.val();
			arr[3] = professional.val();
			arr[4] = user.val();
			addSource();
			return;
		}
		if(productData.prop("checked")) {
			arr[0] = productData.val();
		}
		if(patentData.prop("checked")) {
			arr[1] = patentData.val();
		}
		if(techBlog.prop("checked")) {
			arr[2] = techBlog.val();
		}
		if(professional.prop("checked")) {
			arr[3] = professional.val();
		}
		if(user.prop("checked")) {
			arr[4] = user.val();
		}
		addSource();
	}
	function addSource() {
		var dataStr = "[";
		for (var i = 0; i < arr.length; i++) {
			if(arr[i] != null && arr[i] != "") {
				dataStr = dataStr + "{value:'" + arr[i] + "',text:'" + arr[i] + "'},";
			}
		}
		if(dataStr.length > 1) {
			dataStr = dataStr.substr(0,dataStr.length - 1);
			dataStr = dataStr + "]";
		}
		//alert(dataStr);
		$("#datasource").datalist('load',{value:"fdsdf",text:"wwww"});
		
	}
	function clearArr() {
		arr = ["","","","",""];
	}
	
	function test() {
		alter("dddd");
	}
	
	function removeFromList(inputList,target) {
		for(var i = 0; i < inputList.length; i++) {
			if(inputList[i] == target) {
				if(i >= 0 && i<inputList.length) {
					for(var j=i; j<inputList.length; j++) {
						inputList[j] = inputList[j+1];
					}
					inputList.length = inputList.length-1;
				 }
				 return inputList;
			}
		}
	}
	
	function list2Text(inputList) {
		var text = "";
		for(var i = 0; i < inputList.length; i++) {
			if(i == inputList.length - 1) {
				text = text + inputList[i];
			} else {
				text = text + inputList[i] + " ";
			}
		}
		var searchKey = $("#keyinput").val();
		return searchKey + " "+ text;
	}
	
	function goDown(basePath,rootPath) {
		if(keyList.length <= 0) {
			alert("请选择关键字");
			return;
		}
		var oldKeyword = $("#keyinput").val();
		historyList.push(oldKeyword);
		var keyword = keyList[keyList.length-1];
		keyList = new Array();
		$("#keys").val("");
		$("#keyinput").val(keyword);
		loadResult(basePath,rootPath);
	}
	
	function overHead(basePath,rootPath) {
		var layerNum = $("#layernum").val();
		var layerCount = $("#layercount").val();
		var keyword = "";
		if(historyList.length > 0) {
			keyword = historyList.pop();
			$("#keyinput").val(keyword);
			loadResult(basePath,rootPath);
			
		} else {
			alert("已经为最初状态");
			return;
		}
	}
	

	function showResult(basePath,jsonPath,rootPath) {
		var width = 600;
		var height = 600;
		var item=0;
		//2016.11.04注释,调整大小
//		var padding = {
//			left : 80,
//			right : 50,
//			top : 20,
//			bottom : 20
//		};
		var padding = {
				left : 80,
				right : 0,
				top : 20,
				bottom : 20
			};		
		$("#contentid").html("");
		var svg = d3.select("div[id=contentid]").append("svg").attr("width",
				width + padding.left + padding.right).attr("height",
				height + padding.top + padding.bottom).append("g").attr(
				"transform",
				"translate(" + padding.left + "," + padding.top + ")");
		var tree = d3.layout.tree().size([ height, width ]);
		var diagonal = d3.svg.diagonal().projection(function(d) {
			return [ d.y, d.x ];
		});
		d3.json(jsonPath, function(error, root) {
			root.x0 = height / 2;
			root.y0 = 0;
			redraw(root);
			function redraw(source) {
				var nodes = tree.nodes(root);
				var links = tree.links(nodes);
				//console.log(nodes);
				//console.log(links);
				nodes.forEach(function(d) {
					d.y = d.depth * 180;
				});
				var nodeUpdate = svg.selectAll(".node").data(nodes,
						function(d) {
							return d.name;
						});

				var nodeEnter = nodeUpdate.enter();
				var nodeExit = nodeUpdate.exit();
				var enterNodes = nodeEnter.append("g").attr("class", "node")
						.attr("transform",
								function(d) {
									return "translate(" + source.y0 + ","
											+ source.x0 + ")";
								})
						.on("click",function(d){	

							item++;
							if(item==1) {
								//2016.11.04新增,点击图形上某节点,联动
								$('#msg').html(d.name);
								loadInfo(d.name);//相关信息联动
								
								var color = d3.select(this).attr("fill");
								if(color == null) {
									if(keyList.indexOf(d.name) <= -1) {
										keyList.push(d.name);
									}
									d3.select(this).attr("fill","blue");
								} else {
									if(keyList.indexOf(d.name) > -1) {
										keyList = removeFromList(keyList,d.name);
									}
									d3.select(this).attr("fill",null);
								}
								var text = list2Text(keyList);
								
								$("#keys").val(text);
								var t=setTimeout(function() {
									item--;
									toggle(d);
									redraw(d);
									return ;
								},500);
								
							}
							if(item>=2) {
								var oldKeyword = $("#keyinput").val();
								historyList.push(oldKeyword);
								keyList = new Array();
								$("#keys").val("");
								$("#keyinput").val(d.name);
								loadResult(basePath,rootPath);
							}
						});
				enterNodes.append("circle").attr("r", 0).style("fill",
						function(d) {
							return d._children ? "lightsteelblue" : "#fff";
						});
				enterNodes.append("text").attr("x", function(d) {
					return d.children || d._children ? -14 : 14;
				}).attr("dy", ".35em").attr("text-anchor", function(d) {
					return d.children || d._children ? "end" : "start";
				}).text(function(d) {
					return d.name;
				}).style("fill-opacity", 0);

				var updateNodes = nodeUpdate.transition().duration(500).attr(
						"transform", function(d) {
							return "translate(" + d.y + "," + d.x + ")";
						});

				updateNodes.select("circle").attr("r", 8).style("fill",
						function(d) {
							return d._children ? "lightsteelblue" : "#fff";
						});

				updateNodes.select("text").style("fill-opacity", 1);
				var exitNodes = nodeExit.transition().duration(500).attr(
						"transform",
						function(d) {
							return "translate(" + source.y + "," + source.x
									+ ")";
						}).remove();

				exitNodes.select("circle").attr("r", 0);

				exitNodes.select("text").style("fill-opacity", 0);
				var linkUpdate = svg.selectAll(".link").data(links,
						function(d) {
							return d.target.name;
						});
				var linkEnter = linkUpdate.enter();
				var linkExit = linkUpdate.exit();
				linkEnter.insert("path", ".node").attr("class", "link").attr(
						"d", function(d) {
							var o = {
								x : source.x0,
								y : source.y0
							};
							return diagonal({
								source : o,
								target : o
							});
						}).transition().duration(500).attr("d", diagonal);
				linkUpdate.transition().duration(500).attr("d", diagonal);
				linkExit.transition().duration(500).attr("d", function(d) {
					var o = {
						x : source.x,
						y : source.y
					};
					return diagonal({
						source : o,
						target : o
					});
				}).remove();

				nodes.forEach(function(d) {
					d.x0 = d.x;
					d.y0 = d.y;
				});

			}
			function toggle(d) {
				if (d.children) {
					d._children = d.children;
					d.children = null;
				} else {
					d.children = d._children;
					d._children = null;
				}
			}

		});
	}
	
	function showForceResult(basePath,jsonPath,rootPath) {
		var width = 600;
		var height = 600;
//		var padding = {
//				left : 80,
//				right : 50,
//				top : 20,
//				bottom : 20
//			};
		var padding = {
				left : 40,
				right : 50,
				top : 0,
				bottom : 20
			};		
		$("#contentid").html("");
		var svg = d3.select("div[id=contentid]").append("svg").attr("width",
				width + padding.left + padding.right).attr("height",
				height + padding.top + padding.bottom).append("g").attr(
				"transform",
				"translate(" + padding.left + "," + padding.top + ")");
		d3.json(jsonPath,function(error,root){
            if( error ) {
                return console.log(error);
            }
            console.log(root);
			var force = d3.layout.force().nodes(root.nodes).links(root.edges).size([width,height]).linkDistance(50).charge(-700);
			force.start();
			var circleR = root.circleR;
			var min = d3.min(circleR);
			var max = d3.max(circleR);
			var linear = d3.scale.linear().domain([min, max]).range([10, 20]);
			var svg_edges = svg.selectAll("line").data(root.edges).enter().append("line").style("stroke","#ccc").style("stroke-width",1);
			var color = d3.scale.category20();
			var drag = force.drag();
			var svg_nodes = svg.selectAll("circle").data(root.nodes).enter().append("circle")
				.attr("r",function(d,i) {
					return linear(circleR[i])}).style("fill",function(d,i){
						return color(i);
					}).call(drag).on("dblclick",function(d,i){
						var oldKeyword = $("#keyinput").val();
						historyList.push(oldKeyword);
						keyList = new Array();
						$("#keys").val("");
						$("#keyinput").val(d.name);
						loadResult(basePath,rootPath);
					}).on("click",function(d) {
						
						//2016.11.04新增,点击图形上某节点,联动
						loadInfo(d.name);
						
						var color = d3.select(this).attr("fill");
						if(color == null) {
							if(keyList.indexOf(d.name) <= -1) {
								keyList.push(d.name);
							}
							d3.select(this).attr("fill","blue");
						} else {
							if(keyList.indexOf(d.name) > -1) {
								keyList = removeFromList(keyList,d.name);
							}
							d3.select(this).attr("fill",null);
						}
						var text = list2Text(keyList);
						$("#keys").val(text);
					});
			var svg_texts = svg.selectAll("text").data(root.nodes).enter().append("text").style("fill", "black")
				.attr("dx", 20).attr("dy", 8).text(function(d){
									return d.name;
								});
			force.on("tick", function(){
				 svg_edges.attr("x1",function(d){ return d.source.x; })
					 .attr("y1",function(d){ return d.source.y; })
					 .attr("x2",function(d){ return d.target.x; })
					 .attr("y2",function(d){ return d.target.y; });
				 svg_nodes.attr("cx",function(d){ return d.x; })
				 	 .attr("cy",function(d){ return d.y; });
				 svg_texts.attr("x", function(d){ return d.x; })
				 	.attr("y", function(d){ return d.y; });
			});
					
		});
	}
	
	function showPackResult(basePath,jsonPath,rootPath) {
		var width  = 600;
		var height = 600;
		var item=0;
		var selectColor;
		var pack = d3.layout.pack().size([ width, height ]).radius(25);
		var padding = {
				left : 60,
				right : 20,
				top : 20,
				bottom : 20
		};		

		$("#contentid").html("");
		var svg = d3.select("div[id=contentid]").append("svg").attr("width",
				width + padding.left + padding.right).attr("height",
				height + padding.top + padding.bottom).append("g").attr(
				"transform",
				"translate(" + padding.left + "," + padding.top + ")");
		d3.json(jsonPath, function(error, root) {
			var nodes = pack.nodes(root);
			var links = pack.links(nodes);
			console.log(nodes);
			console.log(links);
			var color = d3.scale.category20();
			svg.selectAll("circle").data(nodes).enter().append("circle")
				.attr("fill",function(d,i){
					return color(i);
				}).attr("fill-opacity","0.4").attr("cx",function(d){
					return d.x;
				}).attr("cy",function(d){
					return d.y;
				}).attr("r",function(d){
					return d.r;
				}).on("mouseover",function(d,i){
					if(keyList.indexOf(d.name) <= -1) {
						d3.select(this).attr("fill","yellow");
					}
				}).on("mouseout",function(d,i){
					if(keyList.indexOf(d.name) <= -1) {
						d3.select(this).attr("fill",function(){
							return color(i);});
					}
				}).on("click",function(d,i){
					item++;
					if(item==1){
						var color = d3.select(this).attr("fill");
						
						if(color != "red") {
							if(keyList.indexOf(d.name) <= -1) {
								keyList.push(d.name);
							}
							d3.select(this).attr("fill","red");
							var text = list2Text(keyList);
							$("#keys").val(text);
							item--;
						} else {
							if(keyList.indexOf(d.name) > -1) {
								keyList = removeFromList(keyList,d.name);
							}
							d3.select(this).attr("fill",function(){
								var text = list2Text(keyList);
								$("#keys").val(text);
								item--;
								return color(i);});
						}
						
					}
					if(item>=2){
						var oldKeyword = $("#keyinput").val();
						historyList.push(oldKeyword);
						keyList = new Array();
						$("#keys").val("");
						$("#keyinput").val(d.name);
						loadResult(basePath,rootPath);
					}
			});
			svg.selectAll("text").data(nodes).enter().append("text").attr("font-size","15px").attr("fill","white")
				.attr("fill-opacity",function(d){
					if(d.depth == 2)
						return "0.9";
					else
						return "0.0";
				}).attr("x",function(d){ return d.x-6; })
					.attr("y",function(d){ return d.y; })
					.attr("dx",-12)
					.attr("dy",1)
					.text(function(d){ return d.name; });
		});
	}
	